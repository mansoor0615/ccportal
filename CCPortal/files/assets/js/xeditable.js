﻿/*!
angular-xeditable - 0.1.8
Edit-in-place for angular.js
Build date: 2014-01-10 
*/
/**
 * Angular-xeditable module 
 *
 */

//!function ($) {

//    // Picker object
//    var mclose = "";
//    var Datepicker = function (element, options) {
//        this.element = $(element);

//        this.mclose = options.myclose;
//        this.format = DPGlobal.parseFormat(options.format || this.element.data('date-format') || 'mm/dd/yyyy');
//        this.picker = $(DPGlobal.template)
//							.appendTo('body')
//							.on({
//							    click: $.proxy(this.click, this)//,
//							    //mousedown: $.proxy(this.mousedown, this)
//							});
//        this.isInput = this.element.is('input');
//        this.component = this.element.is('.date') ? this.element.find('.add-on') : false;

//        if (this.isInput) {
//            this.element.on({
//                focus: $.proxy(this.show, this),
//                //blur: $.proxy(this.hide, this),
//                keyup: $.proxy(this.update, this)
//            });
//        } else {
//            if (this.component) {
//                this.component.on('click', $.proxy(this.show, this));
//            } else {
//                this.element.on('click', $.proxy(this.show, this));
//            }
//        }

//        this.minViewMode = options.minViewMode || this.element.data('date-minviewmode') || 0;
//        if (typeof this.minViewMode === 'string') {
//            switch (this.minViewMode) {
//                case 'months':
//                    this.minViewMode = 1;
//                    break;
//                case 'years':
//                    this.minViewMode = 2;
//                    break;
//                default:
//                    this.minViewMode = 0;
//                    break;
//            }
//        }
//        this.viewMode = options.viewMode || this.element.data('date-viewmode') || 0;
//        if (typeof this.viewMode === 'string') {
//            switch (this.viewMode) {
//                case 'months':
//                    this.viewMode = 1;
//                    break;
//                case 'years':
//                    this.viewMode = 2;
//                    break;
//                default:
//                    this.viewMode = 0;
//                    break;
//            }
//        }
//        this.startViewMode = this.viewMode;
//        this.weekStart = options.weekStart || this.element.data('date-weekstart') || 0;
//        this.weekEnd = this.weekStart === 0 ? 6 : this.weekStart - 1;
//        this.onRender = options.onRender;
//        this.fillDow();
//        this.fillMonths();
//        this.update();
//        this.showMode();
//    };

//    Datepicker.prototype = {
//        constructor: Datepicker,

//        show: function (e) {
//            this.picker.show();
//            this.height = this.component ? this.component.outerHeight() : this.element.outerHeight();
//            this.place();
//            $(window).on('resize', $.proxy(this.place, this));
//            if (e) {
//                e.stopPropagation();
//                e.preventDefault();
//            }
//            if (!this.isInput) {
//            }
//            var that = this;
//            $(document).on('mousedown', function (ev) {
//                if ($(ev.target).closest('.datepicker').length == 0) {
//                    that.hide();
//                }
//            });
//            this.element.trigger({
//                type: 'show',
//                date: this.date
//            });
//        },

//        hide: function () {
//            this.picker.hide();
//            $(window).off('resize', this.place);
//            this.viewMode = this.startViewMode;
//            this.showMode();
//            if (!this.isInput) {
//                $(document).off('mousedown', this.hide);
//            }
//            //this.set();
//            this.element.trigger({
//                type: 'hide',
//                date: this.date
//            });
//        },

//        set: function () {
//            var formated = DPGlobal.formatDate(this.date, this.format);
//            if (!this.isInput) {
//                if (this.component) {
//                    this.element.find('input').prop('value', formated);
//                }
//                this.element.data('date', formated);
//            } else {
//                this.element.prop('value', formated);
//            }
//        },

//        setValue: function (newDate) {
//            if (typeof newDate === 'string') {
//                this.date = DPGlobal.parseDate(newDate, this.format);
//            } else {
//                this.date = new Date(newDate);
//            }
//            this.set();
//            this.viewDate = new Date(this.date.getFullYear(), this.date.getMonth(), 1, 0, 0, 0, 0);
//            this.fill();
//        },

//        place: function () {
//            var offset = this.component ? this.component.offset() : this.element.offset();

//            this.picker.css({
//                top: offset.top + 30,
//                left: offset.left
//            });
//        },

//        update: function (newDate) {
//            this.date = DPGlobal.parseDate(
//				typeof newDate === 'string' ? newDate : (this.isInput ? this.element.prop('value') : this.element.data('date')),
//				this.format
//			);
//            this.viewDate = new Date(this.date.getFullYear(), this.date.getMonth(), 1, 0, 0, 0, 0);
//            this.fill();
//        },

//        fillDow: function () {
//            var dowCnt = this.weekStart;
//            var html = '<tr>';
//            while (dowCnt < this.weekStart + 7) {
//                html += '<th class="dow">' + DPGlobal.dates.daysMin[(dowCnt++) % 7] + '</th>';
//            }
//            html += '</tr>';
//            this.picker.find('.datepicker-days thead').append(html);
//        },

//        fillMonths: function () {
//            var html = '';
//            var i = 0
//            while (i < 12) {
//                html += '<span class="month">' + DPGlobal.dates.monthsShort[i++] + '</span>';
//            }
//            this.picker.find('.datepicker-months td').append(html);
//        },

//        fill: function () {
//            var d = new Date(this.viewDate),
//				year = d.getFullYear(),
//				month = d.getMonth(),
//				currentDate = this.date.valueOf();
//            this.picker.find('.datepicker-days th:eq(1)')
//						.text(DPGlobal.dates.months[month] + ' ' + year);
//            var prevMonth = new Date(year, month - 1, 28, 0, 0, 0, 0),
//				day = DPGlobal.getDaysInMonth(prevMonth.getFullYear(), prevMonth.getMonth());
//            prevMonth.setDate(day);
//            prevMonth.setDate(day - (prevMonth.getDay() - this.weekStart + 7) % 7);
//            var nextMonth = new Date(prevMonth);
//            nextMonth.setDate(nextMonth.getDate() + 42);
//            nextMonth = nextMonth.valueOf();
//            var html = [];
//            var clsName,
//				prevY,
//				prevM;
//            while (prevMonth.valueOf() < nextMonth) {
//                if (prevMonth.getDay() === this.weekStart) {
//                    html.push('<tr>');
//                }
//                clsName = this.onRender(prevMonth);
//                prevY = prevMonth.getFullYear();
//                prevM = prevMonth.getMonth();
//                if ((prevM < month && prevY === year) || prevY < year) {
//                    clsName += ' old';
//                } else if ((prevM > month && prevY === year) || prevY > year) {
//                    clsName += ' new';
//                }
//                if (prevMonth.valueOf() === currentDate) {
//                    clsName += ' active';
//                }
//                html.push('<td class="day ' + clsName + '">' + prevMonth.getDate() + '</td>');
//                if (prevMonth.getDay() === this.weekEnd) {
//                    html.push('</tr>');
//                }
//                prevMonth.setDate(prevMonth.getDate() + 1);
//            }
//            this.picker.find('.datepicker-days tbody').empty().append(html.join(''));
//            var currentYear = this.date.getFullYear();

//            var months = this.picker.find('.datepicker-months')
//						.find('th:eq(1)')
//							.text(year)
//							.end()
//						.find('span').removeClass('active');
//            if (currentYear === year) {
//                months.eq(this.date.getMonth()).addClass('active');
//            }

//            html = '';
//            year = parseInt(year / 10, 10) * 10;
//            var yearCont = this.picker.find('.datepicker-years')
//								.find('th:eq(1)')
//									.text(year + '-' + (year + 9))
//									.end()
//								.find('td');
//            year -= 1;
//            for (var i = -1; i < 11; i++) {
//                html += '<span class="year' + (i === -1 || i === 10 ? ' old' : '') + (currentYear === year ? ' active' : '') + '">' + year + '</span>';
//                year += 1;
//            }
//            yearCont.html(html);
//        },

//        click: function (e) {

//            e.stopPropagation();
//            e.preventDefault();
//            var target = $(e.target).closest('span, td, th');
//            if (target.length === 1) {
//                switch (target[0].nodeName.toLowerCase()) {
//                    case 'th':
//                        switch (target[0].className) {
//                            case 'switch':
//                                this.showMode(1);
//                                break;
//                            case 'prev':
//                            case 'next':
//                                this.viewDate['set' + DPGlobal.modes[this.viewMode].navFnc].call(
//									this.viewDate,
//									this.viewDate['get' + DPGlobal.modes[this.viewMode].navFnc].call(this.viewDate) +
//									DPGlobal.modes[this.viewMode].navStep * (target[0].className === 'prev' ? -1 : 1)
//								);
//                                this.fill();
//                                this.set();
//                                break;
//                        }
//                        break;
//                    case 'span':
//                        if (target.is('.month')) {
//                            var month = target.parent().find('span').index(target);
//                            this.viewDate.setMonth(month);
//                        } else {
//                            var year = parseInt(target.text(), 10) || 0;
//                            this.viewDate.setFullYear(year);
//                        }
//                        if (this.viewMode !== 0) {
//                            this.date = new Date(this.viewDate);
//                            this.element.trigger({
//                                type: 'changeDate',
//                                date: this.date,
//                                viewMode: DPGlobal.modes[this.viewMode].clsName
//                            });
//                        }
//                        this.showMode(-1);
//                        this.fill();
//                        this.set();
//                        break;
//                    case 'td':
//                        if (target.is('.day') && !target.is('.disabled')) {
//                            var day = parseInt(target.text(), 10) || 1;
//                            var month = this.viewDate.getMonth();
//                            if (target.is('.old')) {
//                                month -= 1;
//                            } else if (target.is('.new')) {
//                                month += 1;
//                            }
//                            var year = this.viewDate.getFullYear();
//                            this.date = new Date(year, month, day, 0, 0, 0, 0);
//                            this.viewDate = new Date(year, month, Math.min(28, day), 0, 0, 0, 0);
//                            this.fill();
//                            this.set();
//                            //if (this.mclose == true)
//                            //{
//                            //    this.hide();

//                            //}
//                            this.hide();
//                            this.element.trigger({
//                                type: 'changeDate',
//                                date: this.date,
//                                viewMode: DPGlobal.modes[this.viewMode].clsName
//                            });
//                        }
//                        break;
//                }
//            }
//        },

//        mousedown: function (e) {
//            e.stopPropagation();
//            e.preventDefault();
//        },

//        showMode: function (dir) {
//            if (dir) {
//                this.viewMode = Math.max(this.minViewMode, Math.min(2, this.viewMode + dir));
//            }
//            this.picker.find('>div').hide().filter('.datepicker-' + DPGlobal.modes[this.viewMode].clsName).show();
//        }
//    };

//    $.fn.datepicker = function (option, val) {
//        return this.each(function () {
//            var $this = $(this),
//				data = $this.data('datepicker'),
//				options = typeof option === 'object' && option;
//            if (!data) {
//                $this.data('datepicker', (data = new Datepicker(this, $.extend({}, $.fn.datepicker.defaults, options))));
//            }
//            if (typeof option === 'string') data[option](val);
//        });
//    };

//    $.fn.datepicker.defaults = {
//        onRender: function (date) {
//            return '';
//        }
//    };
//    $.fn.datepicker.Constructor = Datepicker;

//    var DPGlobal = {
//        modes: [
//			{
//			    clsName: 'days',
//			    navFnc: 'Month',
//			    navStep: 1
//			},
//			{
//			    clsName: 'months',
//			    navFnc: 'FullYear',
//			    navStep: 1
//			},
//			{
//			    clsName: 'years',
//			    navFnc: 'FullYear',
//			    navStep: 10
//			}],
//        dates: {
//            days: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"],
//            daysShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"],
//            daysMin: ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa", "Su"],
//            months: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
//            monthsShort: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
//        },
//        isLeapYear: function (year) {
//            return (((year % 4 === 0) && (year % 100 !== 0)) || (year % 400 === 0))
//        },
//        getDaysInMonth: function (year, month) {
//            return [31, (DPGlobal.isLeapYear(year) ? 29 : 28), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][month]
//        },
//        parseFormat: function (format) {
//            var separator = format.match(/[.\/\-\s].*?/),
//				parts = format.split(/\W+/);
//            if (!separator || !parts || parts.length === 0) {
//                throw new Error("Invalid date format.");
//            }
//            return { separator: separator, parts: parts };
//        },
//        parseDate: function (date, format) {
//            var parts = date.split(format.separator),
//				date = new Date(),
//				val;
//            date.setHours(0);
//            date.setMinutes(0);
//            date.setSeconds(0);
//            date.setMilliseconds(0);
//            if (parts.length === format.parts.length) {
//                var year = date.getFullYear(), day = date.getDate(), month = date.getMonth();
//                for (var i = 0, cnt = format.parts.length; i < cnt; i++) {
//                    val = parseInt(parts[i], 10) || 1;
//                    switch (format.parts[i]) {
//                        case 'dd':
//                        case 'd':
//                            day = val;
//                            date.setDate(val);
//                            break;
//                        case 'mm':
//                        case 'm':
//                            month = val - 1;
//                            date.setMonth(val - 1);
//                            break;
//                        case 'yy':
//                            year = 2000 + val;
//                            date.setFullYear(2000 + val);
//                            break;
//                        case 'yyyy':
//                            year = val;
//                            date.setFullYear(val);
//                            break;
//                    }
//                }
//                date = new Date(year, month, day, 0, 0, 0);
//            }
//            return date;
//        },
//        formatDate: function (date, format) {
//            var val = {
//                d: date.getDate(),
//                m: date.getMonth() + 1,
//                yy: date.getFullYear().toString().substring(2),
//                yyyy: date.getFullYear()
//            };
//            val.dd = (val.d < 10 ? '0' : '') + val.d;
//            val.mm = (val.m < 10 ? '0' : '') + val.m;
//            var date = [];
//            for (var i = 0, cnt = format.parts.length; i < cnt; i++) {
//                date.push(val[format.parts[i]]);
//            }
//            return date.join(format.separator);
//        },
//        headTemplate: '<thead>' +
//							'<tr>' +
//								'<th class="prev"><i class="fa fa-chevron-left"></i></th>' +
//								'<th colspan="5" class="switch"></th>' +
//								'<th class="next"><i class="fa fa-chevron-right"></i></th>' +
//							'</tr>' +
//						'</thead>',
//        contTemplate: '<tbody><tr><td colspan="7"></td></tr></tbody>'
//    };
//    DPGlobal.template = '<div class="datepicker dropdown-menu">' +
//							'<div class="datepicker-days">' +
//								'<table class=" table-condensed">' +
//									DPGlobal.headTemplate +
//									'<tbody></tbody>' +
//								'</table>' +
//							'</div>' +
//							'<div class="datepicker-months">' +
//								'<table class="table-condensed">' +
//									DPGlobal.headTemplate +
//									DPGlobal.contTemplate +
//								'</table>' +
//							'</div>' +
//							'<div class="datepicker-years">' +
//								'<table class="table-condensed">' +
//									DPGlobal.headTemplate +
//									DPGlobal.contTemplate +
//								'</table>' +
//							'</div>' +
//						'</div>';

//}(window.jQuery);



angular.module('xeditable', [])


/**
 * Default options. 
 *
 * @namespace editable-options
 */
//todo: maybe better have editableDefaults, not options...
.value('editableOptions', {
    /**
     * Theme. Possible values `bs3`, `bs2`, `default`.
     * 
     * @var {string} theme
     * @memberOf editable-options
     */
    theme: 'default',
    /**
     * Whether to show buttons for single editalbe element.  
     * Possible values `right` (default), `no`.
     * 
     * @var {string} buttons
     * @memberOf editable-options
     */
    buttons: 'right',
    /**
     * Default value for `blur` attribute of single editable element.  
     * Can be `cancel|submit|ignore`.
     * 
     * @var {string} blurElem
     * @memberOf editable-options
     */
    blurElem: 'cancel',
    /**
     * Default value for `blur` attribute of editable form.  
     * Can be `cancel|submit|ignore`.
     * 
     * @var {string} blurForm
     * @memberOf editable-options
     */
    blurForm: 'ignore',
    /**
     * How input elements get activated. Possible values: `focus|select|none`.
     *
     * @var {string} activate
     * @memberOf editable-options
     */
    activate: 'focus'

});
/*
Angular-ui bootstrap datepicker
http://angular-ui.github.io/bootstrap/#/datepicker
*/
angular.module('xeditable').directive('editableBsdate', ['editableDirectiveFactory',
  function (editableDirectiveFactory) {
      // debugger;
      // alert("1");
      return editableDirectiveFactory({
          directiveName: 'editableBsdate',
          //  inputTpl: '<input  type="text" date-time  view="dates" min-view="date" max-view="date" format="DD-MM-YYYY"  >',

          //  inputTpl: '<input  type="text"  class="MyDa" >',



          //render: function () {

          //    $(".MyDa").datepicker();
          //},

          inputTpl: '<span></span>',
          //render: function () {
          //    this.parent.render.call(this);
          //    if (this.attrs.eTitle) {
          //        this.inputEl.wrap('<label></label>');
          //        this.inputEl.after(angular.element('<span></span>').text(this.attrs.eTitle));
          //    }
          //},
          render: function () {

              this.parent.render.call(this);



              var html =
                '<div class="position-relative"><input type="text" style="cursor:pointer;" date-time view="dates" min-view="date" max-view="date" readonly class="form-control" format="MM-DD-YYYY" auto-close="true" ng-model="$parent.$data"><span class="fa fa-calendar bsdatecalendar"></span></div>'
              ;


              this.inputEl.html(html);
          },

          //autosubmit: function () {
          //    // debugger;
          //    //   alert("20");
          //    var self = this;
          //    self.inputEl.bind('change', function () {
          //        alert("tooAlSun");
          //        self.scope.$apply(function () {
          //            self.scope.$form.$submit();
          //        });
          //    });
          //}
          // inputTpl: '<input  type="text" class="MyDa"  >'
      });
  }]);
/*
Angular-ui bootstrap editable timepicker
http://angular-ui.github.io/bootstrap/#/timepicker
*/
angular.module('xeditable').directive('editableBstime', ['editableDirectiveFactory',
  function (editableDirectiveFactory) {
      // debugger;
      // alert("2");
      return editableDirectiveFactory({
          directiveName: 'editableBstime',
          inputTpl: '<timepicker></timepicker>',
          render: function () {
              // // debugger;
              //alert("3");
              this.parent.render.call(this);

              // timepicker can't update model when ng-model set directly to it
              // see: https://github.com/angular-ui/bootstrap/issues/1141
              // so we wrap it into DIV
              // debugger;
              // alert("4");
              var div = angular.element('<div class="well well-small" style="display:inline-block;"></div>');

              // move ng-model to wrapping div
              // debugger;
              // alert("5");
              div.attr('ng-model', this.inputEl.attr('ng-model'));
              this.inputEl.removeAttr('ng-model');

              // move ng-change to wrapping div
              // debugger;
              //  alert("6");
              if (this.attrs.eNgChange) {
                  div.attr('ng-change', this.inputEl.attr('ng-change'));
                  this.inputEl.removeAttr('ng-change');
              }
              // debugger;
              // alert("7");
              // wrap
              this.inputEl.wrap(div);
          }
      });
  }]);
//checkbox
angular.module('xeditable').directive('editableCheckbox', ['editableDirectiveFactory',
  function (editableDirectiveFactory) {
      // debugger;
      //  alert("8");
      return editableDirectiveFactory({
          directiveName: 'editableCheckbox',
          inputTpl: '<input type="checkbox">',
          render: function () {
              this.parent.render.call(this);
              if (this.attrs.eTitle) {
                  this.inputEl.wrap('<label></label>');
                  this.inputEl.after(angular.element('<span></span>').text(this.attrs.eTitle));
              }
          },
          autosubmit: function () {
              // debugger;
              //   alert("9");
              var self = this;
              self.inputEl.bind('change', function () {
                  setTimeout(function () {
                      self.scope.$apply(function () {
                          self.scope.$form.$submit();
                      });
                  }, 500);
              });
          }
      });
  }]);
// checklist
angular.module('xeditable').directive('editableChecklist', [
  'editableDirectiveFactory',
  'editableNgOptionsParser',
  function (editableDirectiveFactory, editableNgOptionsParser) {
      // debugger;
      // alert("9");
      return editableDirectiveFactory({
          directiveName: 'editableChecklist',
          inputTpl: '<span></span>',
          useCopy: true,
          render: function () {
              // debugger;
              //  alert("10");
              this.parent.render.call(this);
              var parsed = editableNgOptionsParser(this.attrs.eNgOptions);
              var html = '<label ng-repeat="' + parsed.ngRepeat + '">' +
                '<input type="checkbox" checklist-model="$parent.$data" checklist-value="' + parsed.locals.valueFn + '">' +
                '<span ng-bind="' + parsed.locals.displayFn + '"></span></label>';
              // debugger;
              //  alert("11");
              this.inputEl.removeAttr('ng-model');
              this.inputEl.removeAttr('ng-options');
              this.inputEl.html(html);
          }
      });
  }]);
/*
Input types: text|email|tel|number|url|search|color|date|datetime|time|month|week
*/

(function () {
    // debugger;
    // alert("12");
    var types = 'text|email|tel|number|url|search|color|date|datetime|time|month|week'.split('|');

    //todo: datalist

    // generate directives
    angular.forEach(types, function (type) {
        // debugger;
        // alert("13");
        var directiveName = 'editable' + type.charAt(0).toUpperCase() + type.slice(1);
        angular.module('xeditable').directive(directiveName, ['editableDirectiveFactory',
          function (editableDirectiveFactory) {
              return editableDirectiveFactory({
                  directiveName: directiveName,
                  inputTpl: '<input type="' + type + '">'
              });
          }]);
    });



    //`range` is bit specific
    angular.module('xeditable').directive('editableRange', ['editableDirectiveFactory',
      function (editableDirectiveFactory) {
          // debugger;
          //   alert("14");
          return editableDirectiveFactory({
              directiveName: 'editableRange',
              inputTpl: '<input type="range" id="range" name="range">',
              render: function () {
                  this.parent.render.call(this);
                  this.inputEl.after('<output>{{$data}}</output>');
              }
          });
      }]);

}());


// radiolist
angular.module('xeditable').directive('editableRadiolist', [
  'editableDirectiveFactory',
  'editableNgOptionsParser',
  function (editableDirectiveFactory, editableNgOptionsParser) {
      // debugger;
      // alert("15");
      return editableDirectiveFactory({
          directiveName: 'editableRadiolist',
          inputTpl: '<span></span>',
          render: function () {
              // debugger;
              //   alert("16");
              this.parent.render.call(this);
              var parsed = editableNgOptionsParser(this.attrs.eNgOptions);
              var html = '<label ng-repeat="' + parsed.ngRepeat + '">' +
                  '<input type="radio" ng-model="$parent.$data" name="' + this.attrs.eName + '_{{$index}}" ng-value="{{' + parsed.locals.valueFn + '}}">' +
                '<span ng-bind="' + parsed.locals.displayFn + '"></span></label>';

              this.inputEl.removeAttr('ng-model');
              this.inputEl.removeAttr('ng-options');
              this.inputEl.html(html);
          },
          autosubmit: function () {
              // debugger;
              //   alert("17");
              var self = this;
              self.inputEl.bind('change', function () {
                  setTimeout(function () {
                      self.scope.$apply(function () {
                          self.scope.$form.$submit();
                      });
                  }, 500);
              });
          }
      });
  }]);
//select
angular.module('xeditable').directive('editableSelect', ['editableDirectiveFactory',
  function (editableDirectiveFactory) {
      // debugger;
      // alert("18");
      return editableDirectiveFactory({
          directiveName: 'editableSelect',
          inputTpl: '<select></select>',
          autosubmit: function () {
              // debugger;

              var self = this;
              self.inputEl.bind('change', function () {

                  self.scope.$apply(function () {
                      self.scope.$form.$submit();
                  });
              });
          }
      });
  }]);
//textarea
angular.module('xeditable').directive('editableTextarea', ['editableDirectiveFactory',
  function (editableDirectiveFactory) {
      // debugger;
      //  alert("19");
      return editableDirectiveFactory({
          directiveName: 'editableTextarea',
          inputTpl: '<textarea  rows="4" cols="30"></textarea>',
          addListeners: function () {
              // debugger;
              //    alert("21");
              var self = this;
              self.parent.addListeners.call(self);
              // submit textarea by ctrl+enter even with buttons
              if (self.single && self.buttons !== 'no') {
                  self.autosubmit();
              }
          },
          autosubmit: function () {
              // debugger;
              //   alert("22");
              var self = this;
              self.inputEl.bind('keydown', function (e) {
                  if ((e.ctrlKey || e.metaKey) && (e.keyCode === 13)) {
                      self.scope.$apply(function () {
                          self.scope.$form.$submit();
                      });
                  }
              });
          }
      });
  }]);

/**
 * EditableController class. 
 * Attached to element with `editable-xxx` directive.
 *
 * @namespace editable-element
 */
/*
TODO: this file should be refactored to work more clear without closures!
*/
angular.module('xeditable').factory('editableController',
  ['$q', 'editableUtils',
  function ($q, editableUtils) {

      //EditableController function
      EditableController.$inject = ['$scope', '$attrs', '$element', '$parse', 'editableThemes', 'editableOptions', '$rootScope', '$compile', '$q', '$filter'];
      function EditableController($scope, $attrs, $element, $parse, editableThemes, editableOptions, $rootScope, $compile, $q, $filter) {
          var valueGetter;
          // debugger;
          //  alert("23");
          //if control is disabled - it does not participate in waiting process
          var inWaiting;

          var self = this;

          self.scope = $scope;
          self.elem = $element;
          self.attrs = $attrs;
          self.inputEl = null;
          self.editorEl = null;
          self.single = true;
          self.error = '';
          self.theme = editableThemes[editableOptions.theme] || editableThemes['default'];
          self.parent = {};

          //to be overwritten by directive
          self.inputTpl = '';
          self.directiveName = '';

          // with majority of controls copy is not needed, but..
          // copy MUST NOT be used for `select-multiple` with objects as items
          // copy MUST be used for `checklist`
          self.useCopy = false;

          //runtime (defaults)
          self.single = null;

          /**
           * Attributes defined with `e-*` prefix automatically transfered from original element to
           * control.  
           * For example, if you set `<span editable-text="user.name" e-style="width: 100px"`>
           * then input will appear as `<input style="width: 100px">`.  
           * See [demo](#text-customize).
           * 
           * @var {any|attribute} e-*
           * @memberOf editable-element
           */

          /**
           * Whether to show ok/cancel buttons. Values: `right|no`.
           * If set to `no` control automatically submitted when value changed.  
           * If control is part of form buttons will never be shown. 
           * 
           * @var {string|attribute} buttons
           * @memberOf editable-element
           */
          self.buttons = 'right';
          /**
           * Action when control losses focus. Values: `cancel|submit|ignore`.
           * Has sense only for single editable element.
           * Otherwise, if control is part of form - you should set `blur` of form, not of individual element.
           * 
           * @var {string|attribute} blur
           * @memberOf editable-element
           */
          // no real `blur` property as it is transfered to editable form

          //init
          self.init = function (single) {
              self.single = single;
              // debugger;
              // alert("24");
              self.name = $attrs.eName || $attrs[self.directiveName];
              /*
              if(!$attrs[directiveName] && !$attrs.eNgModel && ($attrs.eValue === undefined)) {
                throw 'You should provide value for `'+directiveName+'` or `e-value` in editable element!';
              }
              */
              if ($attrs[self.directiveName]) {
                  valueGetter = $parse($attrs[self.directiveName]);
              } else {
                  throw 'You should provide value for `' + self.directiveName + '` in editable element!';
              }

              // settings for single and non-single
              if (!self.single) {
                  // hide buttons for non-single
                  self.buttons = 'no';
              } else {
                  self.buttons = self.attrs.buttons || editableOptions.buttons;
              }

              //if name defined --> watch changes and update $data in form
              if ($attrs.eName) {
                  self.scope.$watch('$data', function (newVal) {
                      //debugger;
                      //if ($attrs.editableBsdate) {

                      //        if (newVal._i) {
                      //            var v = $filter('date')(newVal._i, "MM/dd/yyyy");

                      //            self.scope.$form.$data[$attrs.eName] = v;
                      //            $data = v;
                      //        }
                      //        else {
                      //            self.scope.$form.$data[$attrs.eName] = newVal;
                      //        }




                      //}
                      //else {

                      self.scope.$form.$data[$attrs.eName] = newVal;
                      // }
                  });
              }

              /**
               * Called when control is shown.  
               * See [demo](#select-remote).
               * 
               * @var {method|attribute} onshow
               * @memberOf editable-element
               */
              if ($attrs.onshow) {

                  self.onshow = function () {
                      //alert("sh");
                      //$(".MyDa").datepicker();
                      return self.catchError($parse($attrs.onshow)($scope));
                  };
              }

              /**
               * Called when control is hidden after both save or cancel.  
               * 
               * @var {method|attribute} onhide
               * @memberOf editable-element
               */
              if ($attrs.onhide) {
                  self.onhide = function () {
                      return $parse($attrs.onhide)($scope);
                  };
              }

              /**
               * Called when control is cancelled.  
               * 
               * @var {method|attribute} oncancel
               * @memberOf editable-element
               */
              if ($attrs.oncancel) {
                  self.oncancel = function () {
                      return $parse($attrs.oncancel)($scope);
                  };
              }

              /**
               * Called during submit before value is saved to model.  
               * See [demo](#onbeforesave).
               * 
               * @var {method|attribute} onbeforesave
               * @memberOf editable-element
               */
              if ($attrs.onbeforesave) {
                  self.onbeforesave = function () {
                      return self.catchError($parse($attrs.onbeforesave)($scope));
                  };
              }

              /**
               * Called during submit after value is saved to model.  
               * See [demo](#onaftersave).
               * 
               * @var {method|attribute} onaftersave
               * @memberOf editable-element
               */
              if ($attrs.onaftersave) {
                  self.onaftersave = function () {
                      return self.catchError($parse($attrs.onaftersave)($scope));
                  };
              }

              // watch change of model to update editable element
              // now only add/remove `editable-empty` class.
              // Initially this method called with newVal = undefined, oldVal = undefined
              // so no need initially call handleEmpty() explicitly
              $scope.$parent.$watch($attrs[self.directiveName], function (newVal, oldVal) {
                  self.handleEmpty();
              });
          };

          self.render = function () {
              // debugger;
              // alert("25");
              var theme = self.theme;

              //build input
              self.inputEl = angular.element(self.inputTpl);

              //build controls
              self.controlsEl = angular.element(theme.controlsTpl);
              self.controlsEl.append(self.inputEl);

              //build buttons
              if (self.buttons !== 'no') {
                  // debugger;
                  //  alert("26");
                  self.buttonsEl = angular.element(theme.buttonsTpl);
                  self.submitEl = angular.element(theme.submitTpl);
                  self.cancelEl = angular.element(theme.cancelTpl);
                  self.buttonsEl.append(self.submitEl).append(self.cancelEl);
                  self.controlsEl.append(self.buttonsEl);

                  self.inputEl.addClass('editable-has-buttons');
              }

              //build error
              // debugger;
              // alert("28");
              self.errorEl = angular.element(theme.errorTpl);
              self.controlsEl.append(self.errorEl);

              //build editor
              self.editorEl = angular.element(self.single ? theme.formTpl : theme.noformTpl);
              self.editorEl.append(self.controlsEl);

              // transfer `e-*|data-e-*|x-e-*` attributes
              for (var k in $attrs.$attr) {
                  // debugger;
                  // alert("27");
                  if (k.length <= 1) {
                      continue;
                  }
                  var transferAttr = false;
                  var nextLetter = k.substring(1, 2);

                  // if starts with `e` + uppercase letter
                  // debugger;
                  //alert("30");
                  if (k.substring(0, 1) === 'e' && nextLetter === nextLetter.toUpperCase()) {
                      // debugger;
                      // alert("31");
                      transferAttr = k.substring(1); // cut `e`
                  } else {
                      continue;
                  }

                  // exclude `form` and `ng-submit`, 
                  if (transferAttr === 'Form' || transferAttr === 'NgSubmit') {
                      // debugger;
                      // alert("29");
                      continue;
                  }

                  // convert back to lowercase style

                  // debugger;
                  //alert("33");
                  transferAttr = transferAttr.substring(0, 1).toLowerCase() + editableUtils.camelToDash(transferAttr.substring(1));

                  // workaround for attributes without value (e.g. `multiple = "multiple"`)
                  var attrValue = ($attrs[k] === '') ? transferAttr : $attrs[k];

                  // set attributes to input
                  self.inputEl.attr(transferAttr, attrValue);
              }
              // debugger;
              // alert("34");
              self.inputEl.addClass('editable-input');
              self.inputEl.attr('ng-model', '$data');

              // add directiveName class to editor, e.g. `editable-text`
              self.editorEl.addClass(editableUtils.camelToDash(self.directiveName));

              if (self.single) {
                  // debugger;
                  //  alert("35");
                  self.editorEl.attr('editable-form', '$form');
                  // transfer `blur` to form
                  self.editorEl.attr('blur', self.attrs.blur || (self.buttons === 'no' ? 'cancel' : editableOptions.blurElem));
              }

              //apply `postrender` method of theme
              if (angular.isFunction(theme.postrender)) {
                  // debugger;
                  // alert("37");
                  theme.postrender.call(self);
              }

          };

          // with majority of controls copy is not needed, but..
          // copy MUST NOT be used for `select-multiple` with objects as items
          // copy MUST be used for `checklist`
          self.setLocalValue = function () {
              // debugger;
              // alert("38");
              self.scope.$data = self.useCopy ?
                angular.copy(valueGetter($scope.$parent)) :
                valueGetter($scope.$parent);
          };

          //show
          self.show = function () {
              // debugger;
              //  alert("39");
              // set value of scope.$data
              self.setLocalValue();

              /*
              Originally render() was inside init() method, but some directives polluting editorEl,
              so it is broken on second openning.
              Cloning is not a solution as jqLite can not clone with event handler's.
              */
              self.render();

              // insert into DOM
              $element.after(self.editorEl);

              // compile (needed to attach ng-* events from markup)
              $compile(self.editorEl)($scope);

              // attach listeners (`escape`, autosubmit, etc)
              self.addListeners();

              // hide element
              $element.addClass('editable-hide');

              // onshow
              return self.onshow();
          };

          //hide
          self.hide = function () {
              // debugger;
              //  alert("40");
              self.editorEl.remove();
              $element.removeClass('editable-hide');

              // onhide
              return self.onhide();
          };

          // cancel
          self.cancel = function () {
              // debugger;
              //  alert("41");
              // oncancel
              self.oncancel();
              // don't call hide() here as it called in form's code
          };

          /*
          Called after show to attach listeners
          */
          self.addListeners = function () {
              // debugger;
              //    alert("42");
              // bind keyup for `escape`
              self.inputEl.bind('keyup', function (e) {
                  // debugger;
                  //  alert("43");
                  if (!self.single) {
                      return;
                  }
                  // debugger;
                  // alert("44");
                  // todo: move this to editable-form!
                  switch (e.keyCode) {

                      // hide on `escape` press
                      case 27:
                          self.scope.$apply(function () {
                              self.scope.$form.$cancel();
                          });
                          break;
                  }
              });

              // autosubmit when `no buttons`
              // debugger;
              //  alert("45");
              if (self.single && self.buttons === 'no') {
                  // debugger;
                  //   alert("46");
                  self.autosubmit();
              }

              // click - mark element as clicked to exclude in document click handler
              self.editorEl.bind('click', function (e) {
                  // debugger;
                  //alert("47");
                  // ignore right/middle button click
                  if (e.which !== 1) {
                      return;
                  }

                  if (self.scope.$form.$visible) {
                      // debugger;
                      //  alert("48");
                      self.scope.$form._clicked = true;
                  }
              });
          };

          // setWaiting
          self.setWaiting = function (value) {
              // debugger;
              //  alert("49");
              if (value) {
                  // debugger;
                  //   alert("50");
                  // participate in waiting only if not disabled
                  inWaiting = !self.inputEl.attr('disabled') &&
                              !self.inputEl.attr('ng-disabled') &&
                              !self.inputEl.attr('ng-enabled');
                  if (inWaiting) {
                      // debugger;
                      //  alert("51");
                      self.inputEl.attr('disabled', 'disabled');
                      if (self.buttonsEl) {
                          self.buttonsEl.find('button').attr('disabled', 'disabled');
                      }
                  }
              } else {
                  if (inWaiting) {
                      // debugger;
                      //   alert("52");
                      self.inputEl.removeAttr('disabled');
                      if (self.buttonsEl) {
                          self.buttonsEl.find('button').removeAttr('disabled');
                      }
                  }
              }
          };

          self.activate = function () {
              // debugger;
              // alert("53");
              setTimeout(function () {
                  // debugger;
                  //  alert("54");
                  var el = self.inputEl[0];
                  if (editableOptions.activate === 'focus' && el.focus) {
                      el.focus();
                  }
                  if (editableOptions.activate === 'select' && el.select) {
                      el.select();
                  }
              }, 0);
          };

          self.setError = function (msg) {
              // debugger;
              // alert("55");
              if (!angular.isObject(msg)) {
                  $scope.$error = msg;
                  self.error = msg;
              }
          };

          /*
          Checks that result is string or promise returned string and shows it as error message
          Applied to onshow, onbeforesave, onaftersave
          */
          self.catchError = function (result, noPromise) {
              // debugger;
              //  alert("56");
              if (angular.isObject(result) && noPromise !== true) {
                  // debugger;
                  //  alert("57");
                  $q.when(result).then(
                    //success and fail handlers are equal
                    angular.bind(this, function (r) {
                        this.catchError(r, true);
                    }),
                    angular.bind(this, function (r) {
                        this.catchError(r, true);
                    })
                  );
                  //check $http error
              } else if (noPromise && angular.isObject(result) && result.status &&
                (result.status !== 200) && result.data && angular.isString(result.data)) {
                  // debugger;
                  //   alert("58");
                  this.setError(result.data);
                  //set result to string: to let form know that there was error
                  result = result.data;
              } else if (angular.isString(result)) {
                  this.setError(result);
              }
              return result;
          };

          self.save = function () {
              // debugger;
              //  alert("59");
              valueGetter.assign($scope.$parent, angular.copy(self.scope.$data));

              // no need to call handleEmpty here as we are watching change of model value
              // self.handleEmpty();
          };

          /*
          attach/detach `editable-empty` class to element
          */
          self.handleEmpty = function () {
              // debugger;
              // alert("60");
              var val = valueGetter($scope.$parent);
              var isEmpty = val === null || val === undefined || val === "" || (angular.isArray(val) && val.length === 0);
              $element.toggleClass('editable-empty', isEmpty);
          };

          /*
          Called when `buttons = "no"` to submit automatically
          */
          self.autosubmit = angular.noop;

          self.onshow = angular.noop;
          self.onhide = angular.noop;
          self.oncancel = angular.noop;
          self.onbeforesave = angular.noop;
          self.onaftersave = angular.noop;
      }

      return EditableController;
  }]);

/*
editableFactory is used to generate editable directives (see `/directives` folder)
Inside it does several things:
- detect form for editable element. Form may be one of three types:
  1. autogenerated form (for single editable elements)
  2. wrapper form (element wrapped by <form> tag)
  3. linked form (element has `e-form` attribute pointing to existing form)

- attach editableController to element

Depends on: editableController, editableFormFactory
*/
angular.module('xeditable').factory('editableDirectiveFactory',
['$parse', '$compile', 'editableThemes', '$rootScope', '$document', 'editableController', 'editableFormController',
function ($parse, $compile, editableThemes, $rootScope, $document, editableController, editableFormController) {

    //directive object // debugger;
    // alert("61");
    return function (overwrites) {
        // debugger;
        //  alert("62");
        return {
            restrict: 'A',
            scope: true,
            require: [overwrites.directiveName, '?^form'],
            controller: editableController,
            link: function (scope, elem, attrs, ctrl) {
                // editable controller
                var eCtrl = ctrl[0];

                // form controller
                var eFormCtrl;

                // this variable indicates is element is bound to some existing form, 
                // or it's single element who's form will be generated automatically
                // By default consider single element without any linked form.ß
                var hasForm = false;

                // element wrapped by form
                if (ctrl[1]) {
                    eFormCtrl = ctrl[1];
                    hasForm = true;
                } else if (attrs.eForm) { // element not wrapped by <form>, but we hane `e-form` attr
                    var getter = $parse(attrs.eForm)(scope);
                    if (getter) { // form exists in scope (above), e.g. editable column
                        eFormCtrl = getter;
                        hasForm = true;
                    } else { // form exists below or not exist at all: check document.forms
                        for (var i = 0; i < $document[0].forms.length; i++) {
                            if ($document[0].forms[i].name === attrs.eForm) {
                                // form is below and not processed yet
                                eFormCtrl = null;
                                hasForm = true;
                                break;
                            }
                        }
                    }
                }

                /*
                if(hasForm && !attrs.eName) {
                  throw 'You should provide `e-name` for editable element inside form!';
                }
                */

                //check for `editable-form` attr in form
                /*
                if(eFormCtrl && ) {
                  throw 'You should provide `e-name` for editable element inside form!';
                }
                */

                // store original props to `parent` before merge
                angular.forEach(overwrites, function (v, k) {
                    // debugger;
                    //    alert("63");
                    if (eCtrl[k] !== undefined) {
                        eCtrl.parent[k] = eCtrl[k];
                    }
                });

                // merge overwrites to base editable controller
                angular.extend(eCtrl, overwrites);

                // init editable ctrl
                eCtrl.init(!hasForm);

                // publich editable controller as `$editable` to be referenced in html
                scope.$editable = eCtrl;

                // add `editable` class to element
                elem.addClass('editable');

                // hasForm
                if (hasForm) {
                    if (eFormCtrl) {
                        // debugger;
                        //    alert("64");
                        scope.$form = eFormCtrl;
                        if (!scope.$form.$addEditable) {
                            throw 'Form with editable elements should have `editable-form` attribute.';
                        }
                        scope.$form.$addEditable(eCtrl);
                    } else {
                        // debugger;
                        //  alert("65");
                        // future form (below): add editable controller to buffer and add to form later
                        $rootScope.$$editableBuffer = $rootScope.$$editableBuffer || {};
                        $rootScope.$$editableBuffer[attrs.eForm] = $rootScope.$$editableBuffer[attrs.eForm] || [];
                        $rootScope.$$editableBuffer[attrs.eForm].push(eCtrl);
                        scope.$form = null; //will be re-assigned later
                    }
                    // !hasForm
                } else {
                    // create editableform controller
                    scope.$form = editableFormController();
                    // add self to editable controller
                    scope.$form.$addEditable(eCtrl);

                    // if `e-form` provided, publish local $form in scope
                    if (attrs.eForm) {
                        scope.$parent[attrs.eForm] = scope.$form;
                    }

                    // bind click - if no external form defined
                    if (!attrs.eForm) {
                        elem.addClass('editable-click');
                        elem.bind('click', function (e) {
                            e.preventDefault();
                            e.editable = eCtrl;
                            scope.$apply(function () {
                                scope.$form.$show();
                            });
                        });
                    }
                }

            }
        };
    };
}]);

/*
Returns editableForm controller
*/
angular.module('xeditable').factory('editableFormController',
  ['$parse', '$document', '$rootScope', 'editablePromiseCollection', 'editableUtils',
  function ($parse, $document, $rootScope, editablePromiseCollection, editableUtils) {
      // debugger;
      // alert("66");
      // array of opened editable forms
      var shown = [];

      // bind click to body: cancel|submit|ignore forms
      $document.bind('click', function (e) {
          // ignore right/middle button click
          if (e.which !== 1) {
              return;
          }

          var toCancel = [];
          var toSubmit = [];
          for (var i = 0; i < shown.length; i++) {
              // debugger;
              //alert("67");
              // exclude clicked
              if (shown[i]._clicked) {
                  shown[i]._clicked = false;
                  continue;
              }

              // exclude waiting
              if (shown[i].$waiting) {
                  continue;
              }

              if (shown[i]._blur === 'cancel') {
                  toCancel.push(shown[i]);
              }

              if (shown[i]._blur === 'submit') {
                  // debugger;
                  // alert("68");
                  toSubmit.push(shown[i]);
              }
          }

          if (toCancel.length || toSubmit.length) {
              $rootScope.$apply(function () {
                  // debugger;
                  alert("69");
                  angular.forEach(toCancel, function (v) { v.$cancel(); });
                  angular.forEach(toSubmit, function (v) { v.$submit(); });
              });
          }
      });


      var base = {
          $addEditable: function (editable) {
              // debugger;
              //alert("70");
              //console.log('add editable', editable.elem, editable.elem.bind);
              this.$editables.push(editable);

              //'on' is not supported in angular 1.0.8
              editable.elem.bind('$destroy', angular.bind(this, this.$removeEditable, editable));

              //bind editable's local $form to self (if not bound yet, below form) 
              if (!editable.scope.$form) {
                  // debugger;
                  //  alert("71");
                  editable.scope.$form = this;
              }

              //if form already shown - call show() of new editable
              if (this.$visible) {
                  // debugger;
                  //alert("73");
                  editable.catchError(editable.show());
              }
          },

          $removeEditable: function (editable) {
              //arrayRemove
              for (var i = 0; i < this.$editables.length; i++) {
                  // debugger;
                  //  alert("72");
                  if (this.$editables[i] === editable) {
                      this.$editables.splice(i, 1);
                      return;
                  }
              }
          },

          /**
           * Shows form with editable controls.
           * 
           * @method $show()
           * @memberOf editable-form
           */
          $show: function () {
              // debugger;
              //alert("74");
              if (this.$visible) {
                  return;
              }

              this.$visible = true;

              var pc = editablePromiseCollection();

              //own show
              pc.when(this.$onshow());

              //clear errors
              this.$setError(null, '');

              //children show
              angular.forEach(this.$editables, function (editable) {
                  // debugger;
                  //  alert("75");
                  pc.when(editable.show());
              });

              //wait promises and activate
              pc.then({

                  onWait: angular.bind(this, this.$setWaiting),
                  onTrue: angular.bind(this, this.$activate),
                  onFalse: angular.bind(this, this.$activate),
                  onString: angular.bind(this, this.$activate)
              });

              // add to internal list of shown forms
              // setTimeout needed to prevent closing right after opening (e.g. when trigger by button)
              setTimeout(angular.bind(this, function () {
                  // clear `clicked` to get ready for clicks on visible form
                  // debugger;
                  // alert("76");
                  this._clicked = false;
                  if (editableUtils.indexOf(shown, this) === -1) {
                      shown.push(this);
                  }
              }), 0);

              $(".MyDa").datepicker();
          },

          /**
           * Sets focus on form field specified by `name`.
           * 
           * @method $activate(name)
           * @param {string} name name of field
           * @memberOf editable-form
           */
          $activate: function (name) {
              // debugger;
              // alert("77");
              var i;
              if (this.$editables.length) {
                  // debugger;
                  // alert("78");
                  //activate by name
                  if (angular.isString(name)) {
                      for (i = 0; i < this.$editables.length; i++) {
                          if (this.$editables[i].name === name) {
                              this.$editables[i].activate();
                              return;
                          }
                      }
                  }

                  //try activate error field
                  for (i = 0; i < this.$editables.length; i++) {
                      // debugger;
                      // alert("79");
                      if (this.$editables[i].error) {
                          this.$editables[i].activate();
                          return;
                      }
                  }

                  //by default activate first field
                  this.$editables[0].activate();
              }
          },

          /**
           * Hides form with editable controls without saving.
           * 
           * @method $hide()
           * @memberOf editable-form
           */
          $hide: function () {
              // debugger;
              // alert("80");
              if (!this.$visible) {
                  return;
              }
              this.$visible = false;
              // self hide
              this.$onhide();
              // children's hide
              angular.forEach(this.$editables, function (editable) {
                  editable.hide();
              });

              // remove from internal list of shown forms
              editableUtils.arrayRemove(shown, this);
          },

          /**
           * Triggers `oncancel` event and calls `$hide()`.
           * 
           * @method $cancel()
           * @memberOf editable-form
           */
          $cancel: function () {
              // debugger;
              //  alert("81");
              if (!this.$visible) {
                  return;
              }
              // self cancel
              this.$oncancel();
              // children's cancel      
              angular.forEach(this.$editables, function (editable) {
                  // debugger;
                  //  alert("82");
                  editable.cancel();
              });
              // self hide
              this.$hide();
          },

          $setWaiting: function (value) {
              // debugger;
              // alert("83");
              this.$waiting = !!value;
              // we can't just set $waiting variable and use it via ng-disabled in children
              // because in editable-row form is not accessible
              angular.forEach(this.$editables, function (editable) {
                  // debugger;
                  // alert("84");
                  editable.setWaiting(!!value);
              });
          },

          /**
           * Shows error message for particular field.
           * 
           * @method $setError(name, msg)
           * @param {string} name name of field
           * @param {string} msg error message
           * @memberOf editable-form
           */
          $setError: function (name, msg) {
              // debugger;
              //   alert("85");
              angular.forEach(this.$editables, function (editable) {
                  if (!name || editable.name === name) {
                      editable.setError(msg);
                  }
              });
          },

          $submit: function () {
              // debugger;
              //  alert("86");
              if (this.$waiting) {
                  return;
              }

              //clear errors
              this.$setError(null, '');

              //children onbeforesave
              var pc = editablePromiseCollection();
              angular.forEach(this.$editables, function (editable) {

                  //   alert("87");
                  pc.when(editable.onbeforesave());
              });

              /*
              onbeforesave result:
              - true/undefined: save data and close form
              - false: close form without saving
              - string: keep form open and show error
              */
              pc.then({
                  onWait: angular.bind(this, this.$setWaiting),
                  onTrue: angular.bind(this, checkSelf, true),
                  onFalse: angular.bind(this, checkSelf, false),
                  onString: angular.bind(this, this.$activate)
              });

              //save
              function checkSelf(childrenTrue) {
                  // debugger;
                  //  alert("88");
                  var pc = editablePromiseCollection();
                  pc.when(this.$onbeforesave());
                  pc.then({
                      onWait: angular.bind(this, this.$setWaiting),
                      onTrue: childrenTrue ? angular.bind(this, this.$save) : angular.bind(this, this.$hide),
                      onFalse: angular.bind(this, this.$hide),
                      onString: angular.bind(this, this.$activate)
                  });
              }
          },

          $save: function () {
              // debugger;
              //  alert("89");
              // write model for each editable
              angular.forEach(this.$editables, function (editable) {
                  // debugger;
                  //   alert("90");
                  editable.save();
              });

              //call onaftersave of self and children
              var pc = editablePromiseCollection();
              pc.when(this.$onaftersave());
              angular.forEach(this.$editables, function (editable) {
                  pc.when(editable.onaftersave());
              });

              /*
              onaftersave result:
              - true/undefined/false: just close form
              - string: keep form open and show error
              */
              pc.then({
                  onWait: angular.bind(this, this.$setWaiting),
                  onTrue: angular.bind(this, this.$hide),
                  onFalse: angular.bind(this, this.$hide),
                  onString: angular.bind(this, this.$activate)
              });
          },

          $onshow: angular.noop,
          $oncancel: angular.noop,
          $onhide: angular.noop,
          $onbeforesave: angular.noop,
          $onaftersave: angular.noop
      };

      return function () {
          // debugger;
          // alert("91");
          return angular.extend({
              $editables: [],
              /**
               * Form visibility flag.
               * 
               * @var {bool} $visible
               * @memberOf editable-form
               */
              $visible: false,
              /**
               * Form waiting flag. It becomes `true` when form is loading or saving data.
               * 
               * @var {bool} $waiting
               * @memberOf editable-form
               */
              $waiting: false,
              $data: {},
              _clicked: false,
              _blur: null
          }, base);
      };
  }]);

/**
 * EditableForm directive. Should be defined in <form> containing editable controls.  
 * It add some usefull methods to form variable exposed to scope by `name="myform"` attribute.
 *
 * @namespace editable-form
 */
angular.module('xeditable').directive('editableForm',
  ['$rootScope', '$parse', 'editableFormController', 'editableOptions',
  function ($rootScope, $parse, editableFormController, editableOptions) {
      return {
          restrict: 'A',
          require: ['form'],
          //require: ['form', 'editableForm'],
          //controller: EditableFormController,
          compile: function () {
              // debugger;
              //  alert("92");
              return {
                  pre: function (scope, elem, attrs, ctrl) {
                      var form = ctrl[0];
                      var eForm;

                      //if `editableForm` has value - publish smartly under this value
                      //this is required only for single editor form that is created and removed
                      if (attrs.editableForm) {
                          // debugger;
                          //  alert("96");
                          if (scope[attrs.editableForm] && scope[attrs.editableForm].$show) {
                              // debugger;
                              //    alert("93");
                              eForm = scope[attrs.editableForm];
                              angular.extend(form, eForm);
                          } else {
                              // debugger;
                              //   alert("94");
                              eForm = editableFormController();
                              scope[attrs.editableForm] = eForm;
                              angular.extend(eForm, form);
                          }
                      } else { //just merge to form and publish if form has name
                          // debugger;
                          //   alert("95");
                          eForm = editableFormController();
                          angular.extend(form, eForm);
                      }

                      //read editables from buffer (that appeared before FORM tag)
                      var buf = $rootScope.$$editableBuffer;
                      var name = form.$name;
                      // debugger;
                      //  alert("97");
                      if (name && buf && buf[name]) {
                          // debugger;
                          //   alert("98");
                          angular.forEach(buf[name], function (editable) {
                              // debugger;
                              //   alert("99");
                              eForm.$addEditable(editable);
                          });
                          delete buf[name];
                      }
                  },
                  post: function (scope, elem, attrs, ctrl) {
                      var eForm;
                      // debugger;
                      //  alert("100");
                      if (attrs.editableForm && scope[attrs.editableForm] && scope[attrs.editableForm].$show) {
                          // debugger;
                          //  alert("101");
                          eForm = scope[attrs.editableForm];
                      } else {
                          // debugger;
                          //   alert("103");
                          eForm = ctrl[0];
                      }

                      /**
                       * Called when form is shown.
                       * 
                       * @var {method|attribute} onshow 
                       * @memberOf editable-form
                       */
                      if (attrs.onshow) {
                          // debugger;
                          //  alert("102");
                          eForm.$onshow = angular.bind(eForm, $parse(attrs.onshow), scope);

                      }


                      /**
                       * Called when form hides after both save or cancel.
                       * 
                       * @var {method|attribute} onhide 
                       * @memberOf editable-form
                       */
                      if (attrs.onhide) {
                          // debugger;
                          // alert("104");
                          eForm.$onhide = angular.bind(eForm, $parse(attrs.onhide), scope);
                      }

                      /**
                       * Called when form is cancelled.
                       * 
                       * @var {method|attribute} oncancel
                       * @memberOf editable-form
                       */
                      if (attrs.oncancel) {
                          // debugger;
                          //  alert("105");
                          eForm.$oncancel = angular.bind(eForm, $parse(attrs.oncancel), scope);
                      }

                      /**
                       * Whether form initially rendered in shown state.
                       *
                       * @var {bool|attribute} shown
                       * @memberOf editable-form
                       */
                      if (attrs.shown && $parse(attrs.shown)(scope)) {
                          // debugger;
                          //  alert("106");
                          eForm.$show();
                      }

                      /**
                       * Action when form losses focus. Values: `cancel|submit|ignore`.
                       * Default is `ignore`.
                       * 
                       * @var {string|attribute} blur
                       * @memberOf editable-form
                       */
                      eForm._blur = attrs.blur || editableOptions.blurForm;

                      // onbeforesave, onaftersave
                      if (!attrs.ngSubmit && !attrs.submit) {
                          // debugger;
                          //   alert("107");
                          /**
                           * Called after all children `onbeforesave` callbacks but before saving form values
                           * to model.  
                           * If at least one children callback returns `non-string` - it will not not be called.  
                           * See [editable-form demo](#editable-form) for details.
                           * 
                           * @var {method|attribute} onbeforesave
                           * @memberOf editable-form
                           * 
                           */
                          if (attrs.onbeforesave) {
                              eForm.$onbeforesave = function () {
                                  // debugger;
                                  //    alert("108");
                                  return $parse(attrs.onbeforesave)(scope, { $data: eForm.$data });
                              };
                          }

                          /**
                           * Called when form values are saved to model.  
                           * See [editable-form demo](#editable-form) for details.
                           * 
                           * @var {method|attribute} onaftersave 
                           * @memberOf editable-form
                           * 
                           */
                          if (attrs.onaftersave) {
                              eForm.$onaftersave = function () {
                                  // debugger;
                                  //  alert("109");
                                  return $parse(attrs.onaftersave)(scope, { $data: eForm.$data });
                              };
                          }

                          elem.bind('submit', function (event) {
                              // debugger;
                              // alert("110");
                              event.preventDefault();
                              scope.$apply(function () {
                                  eForm.$submit();
                              });
                          });
                      }


                      // click - mark form as clicked to exclude in document click handler
                      elem.bind('click', function (e) {
                          // debugger;
                          //  alert("111");
                          // ignore right/middle button click
                          if (e.which !== 1) {
                              // debugger;
                              //    alert("112");
                              return;
                          }

                          if (eForm.$visible) {
                              // debugger;
                              //  alert("113");
                              eForm._clicked = true;
                          }
                      });

                  }
              };
          }
      };
  }]);
/**
 * editablePromiseCollection
 *  
 * Collect results of function calls. Shows waiting if there are promises. 
 * Finally, applies callbacks if:
 * - onTrue(): all results are true and all promises resolved to true
 * - onFalse(): at least one result is false or promise resolved to false
 * - onString(): at least one result is string or promise rejected or promise resolved to string
 */
angular.module('xeditable').factory('editablePromiseCollection', ['$q', function ($q) {

    function promiseCollection() {
        // debugger;
        //  alert("114");
        return {
            promises: [],
            hasFalse: false,
            hasString: false,
            when: function (result, noPromise) {
                // debugger;
                // alert("115");
                if (result === false) {
                    // debugger;
                    //   alert("116");
                    this.hasFalse = true;
                } else if (!noPromise && angular.isObject(result)) {
                    // debugger;
                    //   alert("117");
                    this.promises.push($q.when(result));
                } else if (angular.isString(result)) {
                    // debugger;
                    //  alert("118");
                    this.hasString = true;
                } else { //result === true || result === undefined || result === null
                    return;
                }
            },
            //callbacks: onTrue, onFalse, onString
            then: function (callbacks) {
                // debugger;
                // alert("119");
                callbacks = callbacks || {};
                var onTrue = callbacks.onTrue || angular.noop;
                var onFalse = callbacks.onFalse || angular.noop;
                var onString = callbacks.onString || angular.noop;
                var onWait = callbacks.onWait || angular.noop;

                var self = this;

                if (this.promises.length) {
                    // debugger;
                    // alert("120");
                    onWait(true);
                    $q.all(this.promises).then(
                      //all resolved       
                      function (results) {
                          // debugger;
                          // alert("121");
                          onWait(false);
                          //check all results via same `when` method (without checking promises)
                          angular.forEach(results, function (result) {
                              self.when(result, true);
                          });
                          applyCallback();
                      },
                      //some rejected
                      function (error) {
                          onWait(false);
                          onString();
                      }
                    );
                } else {
                    applyCallback();
                }

                function applyCallback() {
                    // debugger;
                    // alert("122");
                    if (!self.hasString && !self.hasFalse) {
                        onTrue();
                    } else if (!self.hasString && self.hasFalse) {
                        onFalse();
                    } else {
                        onString();
                    }
                }

            }
        };
    }

    return promiseCollection;

}]);

/**
 * editableUtils
 */
angular.module('xeditable').factory('editableUtils', [function () {
    return {

        indexOf: function (array, obj) {
            // debugger;
            // alert("127");
            if (array.indexOf) return array.indexOf(obj);

            for (var i = 0; i < array.length; i++) {
                if (obj === array[i]) return i;
            }
            return -1;
        },

        arrayRemove: function (array, value) {
            // debugger;
            //  alert("128");
            var index = this.indexOf(array, value);
            if (index >= 0) {
                array.splice(index, 1);
            }
            return value;
        },

        // copy from https://github.com/angular/angular.js/blob/master/src/Angular.js
        camelToDash: function (str) {
            // debugger;
            //  alert("124");
            var SNAKE_CASE_REGEXP = /[A-Z]/g;
            return str.replace(SNAKE_CASE_REGEXP, function (letter, pos) {
                return (pos ? '-' : '') + letter.toLowerCase();
            });
        },

        dashToCamel: function (str) {
            // debugger;
            // alert("125");
            var SPECIAL_CHARS_REGEXP = /([\:\-\_]+(.))/g;
            var MOZ_HACK_REGEXP = /^moz([A-Z])/;
            return str.
              replace(SPECIAL_CHARS_REGEXP, function (_, separator, letter, offset) {
                  return offset ? letter.toUpperCase() : letter;
              }).
              replace(MOZ_HACK_REGEXP, 'Moz$1');
        }
    };
}]);

/**
 * editableNgOptionsParser
 *
 * see: https://github.com/angular/angular.js/blob/master/src/ng/directive/select.js#L131
 */
angular.module('xeditable').factory('editableNgOptionsParser', [
  function () {
      //0000111110000000000022220000000000000000000000333300000000000000444444444444444000000000555555555555555000000066666666666666600000000000000007777000000000000000000088888
      var NG_OPTIONS_REGEXP = /^\s*(.*?)(?:\s+as\s+(.*?))?(?:\s+group\s+by\s+(.*))?\s+for\s+(?:([\$\w][\$\w]*)|(?:\(\s*([\$\w][\$\w]*)\s*,\s*([\$\w][\$\w]*)\s*\)))\s+in\s+(.*?)(?:\s+track\s+by\s+(.*?))?$/;

      function parser(optionsExp) {
          var match;
          // debugger;
          // alert("130");
          if (!(match = optionsExp.match(NG_OPTIONS_REGEXP))) {
              throw 'ng-options parse error';
          }

          var
              displayFn = match[2] || match[1],
              valueName = match[4] || match[6],
              keyName = match[5],
              groupByFn = match[3] || '',
              valueFn = match[2] ? match[1] : valueName,
              valuesFn = match[7],
              track = match[8],
              trackFn = track ? match[8] : null;

          var ngRepeat;
          if (keyName === undefined) {
              // debugger;
              // alert("131");// array
              ngRepeat = valueName + ' in ' + valuesFn;
              if (track !== undefined) {
                  // debugger;
                  // alert("132");
                  ngRepeat += ' track by ' + trackFn;
              }
          } else { // object
              // debugger;
              // alert("134");
              ngRepeat = '(' + keyName + ', ' + valueName + ') in ' + valuesFn;
          }

          // group not supported yet
          return {

              ngRepeat: ngRepeat,
              locals: {
                  valueName: valueName,
                  keyName: keyName,
                  valueFn: valueFn,
                  displayFn: displayFn
              }
          };
      }
      // debugger;
      // alert("133");
      return parser;
  }]);

/*
Editable themes:
- default
- bootstrap 2
- bootstrap 3

Note: in postrender() `this` is instance of editableController
*/
angular.module('xeditable').factory('editableThemes', function () {
    // debugger;
    // alert("141");
    var themes = {
        //default
        'default': {
            formTpl: '<form class="editable-wrap"></form>',
            noformTpl: '<span class="editable-wrap"></span>',
            controlsTpl: '<span class="editable-controls"></span>',
            inputTpl: '',
            errorTpl: '<div class="editable-error" ng-show="$error" ng-bind="$error"></div>',
            buttonsTpl: '<span class="editable-buttons"></span>',
            submitTpl: '<button type="submit">save</button>',
            cancelTpl: '<button type="button" ng-click="$form.$cancel()">cancel</button>'
        },

        //bs2
        'bs2': {
            formTpl: '<form class="form-inline editable-wrap" role="form"></form>',
            noformTpl: '<span class="editable-wrap"></span>',
            controlsTpl: '<div class="editable-controls controls control-group" ng-class="{\'error\': $error}"></div>',
            inputTpl: '',
            errorTpl: '<div class="editable-error help-block" ng-show="$error" ng-bind="$error"></div>',
            buttonsTpl: '<span class="editable-buttons"></span>',
            submitTpl: '<button type="submit" class="btn btn-primary"><span class="icon-ok icon-white"></span></button>',
            cancelTpl: '<button type="button" class="btn" ng-click="$form.$cancel()">' +
                            '<span class="icon-remove"></span>' +
                         '</button>'

        },

        //bs3
        'bs3': {
            formTpl: '<form class="form-inline editable-wrap" role="form"></form>',
            noformTpl: '<span class="editable-wrap"></span>',
            controlsTpl: '<div class="editable-controls form-group" ng-class="{\'has-error\': $error}"></div>',
            inputTpl: '',
            errorTpl: '<div class="editable-error help-block" ng-show="$error" ng-bind="$error"></div>',
            buttonsTpl: '<span class="editable-buttons"></span>',
            submitTpl: '<button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-ok"></span></button>',
            cancelTpl: '<button type="button" class="btn btn-default" ng-click="$form.$cancel()">' +
                           '<span class="glyphicon glyphicon-remove"></span>' +
                         '</button>',

            //bs3 specific prop to change buttons class: btn-sm, btn-lg
            buttonsClass: '',
            //bs3 specific prop to change standard inputs class: input-sm, input-lg
            inputClass: '',
            postrender: function () {
                //apply `form-control` class to std inputs
                switch (this.directiveName) {
                    case 'editableText':
                    case 'editableSelect':
                    case 'editableTextarea':
                    case 'editableEmail':
                    case 'editableTel':
                    case 'editableNumber':
                    case 'editableUrl':
                    case 'editableSearch':
                    case 'editableDate':
                    case 'editableDatetime':
                    case 'editableTime':
                    case 'editableMonth':
                    case 'editableWeek':
                        this.inputEl.addClass('form-control');
                        if (this.theme.inputClass) {
                            // don`t apply `input-sm` and `input-lg` to select multiple
                            // should be fixed in bs itself!
                            if (this.inputEl.attr('multiple') &&
                              (this.theme.inputClass === 'input-sm' || this.theme.inputClass === 'input-lg')) {
                                break;
                            }
                            this.inputEl.addClass(this.theme.inputClass);
                        }
                        break;
                }

                //apply buttonsClass (bs3 specific!)
                if (this.buttonsEl && this.theme.buttonsClass) {
                    this.buttonsEl.find('button').addClass(this.theme.buttonsClass);
                }
            }
        }
    };

    return themes;
});
