﻿app.controller('TicketCtrl', ['$scope', '$rootScope', '$http', '$location', '$routeParams',
    function ($scope, $rootScope, $http, $location, $routeParams) {

        debugger;
        $scope.pagination = {
            TicketnumPerPage: "5"
        }
        var UserName = window.localStorage.getItem("UserName");
        if (UserName) {
            $rootScope.UserName = UserName;
        }
        else {
            window.localStorage.clear();
            $rootScope.UserName = null;
            $location.path("/signin");
        }

        $rootScope.SerachCustomerID = $routeParams.CustomerId;
        $scope.filterstatus = parseInt($routeParams.status);


        function GetCCustomerDetailsByCustomerId() {
            var CustomerId = $rootScope.SerachCustomerID;
            if (CustomerId.length > 0) {

                $http.get($rootScope.APiUrl + "/api/GetDetailsByIndiviualCustomerPortal?CustomerID=" + CustomerId + "&PortalType=1")
                    .success(function (emp) {
                        debugger;
                        $scope.user = emp;
                        date = new Date($scope.user.DOBGeorgian);
                        $scope.userAge = _calculateAge(date);

                    });

            }
        }
        function _calculateAge(birthday) { // birthday is a date
            var ageDifMs = Date.now() - birthday.getTime();
            var ageDate = new Date(ageDifMs); // miliseconds from epoch
            return Math.abs(ageDate.getUTCFullYear() - 1970);
        }

        GetCCustomerDetailsByCustomerId();
        GetCustomerTicketDetails();

        function GetCustomerTicketDetails() {

            var CustomerId = $rootScope.SerachCustomerID;
            if (CustomerId.length > 0) {

                $http.get($rootScope.APiUrl + "/api/GetEnquiryStatusCount?CustomerId=" + CustomerId)
                    .success(function (emp) {
                        debugger;
                        $scope.TicketDatails = emp;

                        $scope.AllTicket = $scope.TicketDatails.filter(x => x.StatusName == "All")[0];
                        $scope.OpenTicket = $scope.TicketDatails.filter(x => x.StatusName == "Open")[0];
                        $scope.CloseTicket = $scope.TicketDatails.filter(x => x.StatusName == "Closed")[0];
                        $scope.OnHoldTicket = $scope.TicketDatails.filter(x => x.StatusName == "On Hold")[0];
                        $scope.ResolvedTicket = $scope.TicketDatails.filter(x => x.StatusName == "Resolved")[0];
                        $scope.ReopenTicket = $scope.TicketDatails.filter(x => x.StatusName == "Reopen")[0];
                        $scope.CanceledTicket = $scope.TicketDatails.filter(x => x.StatusName == "Canceled")[0];

                        var total = $scope.AllTicket.Count;
                        TicketStatusChart();
                        if ($scope.OpenTicket.Count > 0)
                            $scope.OpenTicketPer = (($scope.OpenTicket.Count / total) * 100).toFixed(2);

                        if ($scope.CloseTicket.Count > 0)
                            $scope.CloseTicketPer = (($scope.CloseTicket.Count / total) * 100).toFixed(2);

                        if ($scope.OnHoldTicket.Count > 0)
                            $scope.OnHoldTicketPer = (($scope.OnHoldTicket.Count / total) * 100).toFixed(2);

                        if ($scope.ResolvedTicket.Count > 0)
                            $scope.ResolvedTicketPer = (($scope.ResolvedTicket.Count / total) * 100).toFixed(2);

                        if ($scope.ReopenTicket.Count > 0)
                            $scope.ReopenTicketPer = (($scope.ReopenTicket.Count / total) * 100).toFixed(2);

                        if ($scope.CanceledTicket.Count > 0)
                            $scope.CanceledTicketPer = (($scope.CanceledTicket.Count / total) * 100).toFixed(2);
                    });
            }
        }


        GetCustomerTicketList();
      

        $scope.GetCustomerTicketList = function () {
            GetCustomerTicketList();
        };

        function GetCustomerTicketList() {
            debugger;
            var rootapi = "";
            if ($scope.filterstatus && $scope.filterstatus != -2) {
                rootapi = $rootScope.APiUrl + "/api/GetMyTicket?CustomerId=" + $rootScope.SerachCustomerID + "&status=" + $scope.filterstatus;
            }
            else {
                rootapi = $rootScope.APiUrl + "/api/GetMyTicket?CustomerId=" + $rootScope.SerachCustomerID;
            }
            $http.get(rootapi)
               .success(function (data) {
                   debugger;
                   $scope.CustomerTicketList = data.Data;
                   $scope.TicketcurrentPage = 1;
                   $scope.pagination.TicketnumPerPage = "5";
                   $scope.Tickettotallist = Math.ceil($scope.CustomerTicketList.length / parseInt($scope.pagination.TicketnumPerPage));

                   $scope.LowTicket = $scope.CustomerTicketList.filter(x => x.PriorityId == 1);
                   $scope.MediumTicket = $scope.CustomerTicketList.filter(x => x.PriorityId == 2);
                   $scope.HighTicket = $scope.CustomerTicketList.filter(x => x.PriorityId == 3);
                   TicketPriorityChart();
               });

        }

        $scope.getTicketDetails = function (item) {
            $scope.TicketNumber = item.EnquiryId;

        };

        $scope.CreateTicket = function () {
            debugger;
            $scope.ddlGroup = null;
            $scope.ddlTicketType = null;
            $scope.ddlAssignedGroup = null;
            $scope.ddlPriority = null;
            $scope.ddlAssignedTo = null;
        //    $scope.loadGroupDrop();
            $scope.ddlSubGroup = null;
            $scope.TicketContract = null;
            $scope.TicketLabour = null;
            $("#file-6").val("");
            $("#custname").val("");
            $("#ddlGroup").val("");
            $("#ddlSubGroup").val("");
            $("#txtSubject").val("");
            $("#txtAccuracy").val("");
            $("#txtCustomerSatisfied").val("");
            $("#textArea").val("");
            $("#createpopup").removeClass("hide");
            $("#createpopup").show();

            $scope.GetCustomerContract();

        }
        $scope.hideTicketpop = function () {
            $("#createpopup").hide();
        }
        $scope.Ticketupdate = function (enquiry) {
            $scope.Status = enquiry.StatusName;
            $scope.Subject = enquiry.Subject;
            $scope.Email = enquiry.Email;
            $scope.CustomerName = enquiry.CustomerName;
            $scope.Contact = enquiry.ContactNo;
            $scope.Desp = enquiry.Description;
            $scope.EnqId = enquiry.EnquiryId;
            $("#StatusSelect").val(enquiry.StatusId);
            $scope.StatusSelect = enquiry.StatusId;
            $("#Ticketupdate").show();

        }
        $scope.closeTicketupdate = function () {
            $("#Ticketupdate").hide();
        }

        $('.panel .nav-tabs').on('click', 'a', function (e) {
            var tab = $(this).parent(),
                tabIndex = tab.index(),
                tabPanel = $(this).closest('.panel'),
                tabPane = tabPanel.find('.tab-pane').eq(tabIndex);
            tabPanel.find('.active').removeClass('active');
            tab.addClass('active');
            tabPane.addClass('active');
        });

        $scope.ddlGroupDrop = [];
        $scope.loadGroupDrop = function (AssignGroupId) {
            // $http.get($rootScope.APiUrl + "/api/GroupDrop")
            //     .success(function (data) {
            //         $scope.ddlGroupDrop = data;
            //     });

            $http.get("/api/GetTicketGroupByDepatmentId?TicketAssignGroupId=" + AssignGroupId)
            .success(function (emp) {
         
                $scope.ddlGroupDrop = emp;
            });

        };

        $scope.ClearFilter = function () {
            debugger;
            $("#createpopup").addClass("hide");
            $scope.ddlGroup = null;
            $scope.ddlSubGroup = null;
            $scope.TicketContract = null;
            $scope.TicketLabour = null;

        }
        $scope.ChangeTicketType = function (e) {
            var TicketType = $scope.ddlTicketType == null ? e : $scope.ddlTicketType;
            
            if (TicketType) {

                $scope.GetAssigntogroup(TicketType);
            
              //  $http.get("/api/GetTicketAssignedToGroupByTicketTypeId?TicketTypeID="+$scope.TicketTypeId)
            
                // $http.get("/EnquiryAngular/GroupList?TicketTypeId=" + TicketType)
                //     .success(function (data) {
                //         $scope.ddlGroupDrop = [{ ID: "", Name: "" }];
                //         for (i = 0; i < data.length; i++) {
                //             $scope.ddlGroupDrop.push(data[i]);
                //         }
                //         $scope.ChangeGroup();
                //     });
            }

        };

        $scope.ChangeAssigntogroup = function (e) {
            var AssignedGroup = $scope.ddlAssignedGroup == null ? e : $scope.ddlAssignedGroup;

            if (AssignedGroup) {

                $scope.GetAssignedTo(AssignedGroup);
                $scope.loadGroupDrop(AssignedGroup);
            }
        }


        $scope.ChangeGroup = function (e) {
debugger;
            var group = $scope.ddlGroup == null ? e : $scope.ddlGroup;
            var group1 = $("#ddlGroup").val();
            var group = group1.split(":")[1];
            if (group) {
                $http.get($rootScope.APiUrl + "/api/SubGroupByGroup?id=" + group)
                    .success(function (data) {
                        $scope.ddlSubGroupDrop = data;
                    });
            }
        };

        $scope.changeSubGroup = function (e) {
            debugger;
            if (e && e.Description)
                $("#txtSubject").val(e.Description);
        };

        $scope.fileNameChanged = function () {
            debugger;
            var fileChange = document.getElementById('file-6');
            $scope.FileName = fileChange.files.item(0).name;
            $scope.$apply(function () {
                if ($scope.FileName != null) {
                    $scope.ChoosenFileName = $scope.FileName;
                }
            });
        }

        $scope.CustomerContract = [];
        $scope.GetCustomerContract = function () {
            $http.get($rootScope.APiUrl + "/api/GetTicketCustomerDetails?CustomerId=" + $routeParams.CustomerId)
                .success(function (data) {
                    $scope.CustomerContract = data.Result2;
                });
        }

        $scope.GetContractEmployee = function (ContractId) {
            if (ContractId) {
                $http.get($rootScope.APiUrl + "/api/GetTicketIndContractAllEmployee?CustomerId=" + $routeParams.CustomerId + "&ContractId=" + ContractId)
                    .success(function (data) {
                        $scope.CustomerLabour = data;
                    });
            }
            else {
                $scope.CustomerLabour = null;
            }
        }

        $scope.Assigntogroup = [];
        $scope.GetAssigntogroup = function (TicketTypeId) {
            // $http.get($rootScope.APiUrl + "/api/AssigntogroupList")
            //     .success(function (data) {
            //         $scope.Assigntogroup = data.Data;
            //     });

                $http.get("/api/GetTicketAssignedToGroupByTicketTypeId?TicketTypeID="+TicketTypeId)
                .success(function (data) {
                    $scope.Assigntogroup = data;
                });
        }



       // $scope.GetAssigntogroup();

        $scope.TicketType = [];
        $scope.GetTicketType = function () {
            $http.get($rootScope.APiUrl + "/api/TickettypeList")
                .success(function (data) {
                    $scope.myrequestlist = data.Data;
                    $scope.TicketType = [];
                    var Transcation = $scope.myrequestlist.filter(function (d) {
                        if (d.ID != "1") {
                            return (d);
                        }
                    });
                    $scope.TicketType = Transcation;
                });
        }
        $scope.GetTicketType();

        $scope.Priority = [];
        $scope.GetPriority = function () {
            $http.get($rootScope.APiUrl + "/api/PriorityList")
                .success(function (data) {
                    $scope.Priority = data.Data;
                });
        }
        $scope.GetPriority();


        $scope.AssignedTo = [];
        $scope.GetAssignedTo = function (AssignGroupId) {
            // $http.get($rootScope.APiUrl + "/api/assigntoList")
            //     .success(function (data) {
            //         $scope.AssignedTo = data.Data;
            //     });
            $http.get("/EnquiryAngular/GetAssignToByGroup?AssignGroupId=" + AssignGroupId)
                .success(function (emp) {

                    $scope.AssignedTo = emp;
                });

        }
     //   $scope.GetAssignedTo();

        $scope.statuslist = [];
        $scope.GetStatus = function () {
            $http.get($rootScope.APiUrl + "/api/GetStatusList")
      .success(function (emp) {
          $scope.statuslist = emp.Data;
      });
        }
        $scope.GetStatus();

        $scope.filterstatuslist = [];
        $scope.GetfilterStatus = function () {
            $http.get($rootScope.APiUrl + "/api/GetStatusList")
      .success(function (emp) {
          $scope.filterstatuslist = emp.Data;
          $scope.filterstatuslist = $scope.filterstatuslist.filter(x => x.Name != "Canceled");
          $scope.filterstatuslist.push({ ID: 6, Name: "Cancelled", NameAr: " الملغاة" });
          $scope.filterstatuslist.push({ ID: -2, Name: "Show All", NameAr: "" });

      });
        }
        $scope.GetfilterStatus();


        $("#crtbtn").click(function () {
            debugger;
            if (($("#ddlGroup").val()).length && ($("#ddlSubGroup").val()).length && ($("#textArea").val()).length && ($("#txtSubject").val()).length != 0) {
                debugger;
                //$('#crtbtn').attr('disabled', 'disabled');
                var data = new FormData();

                var ContractNumber = $scope.TicketContract;
                var LabourNumber = $scope.TicketLabour;
                var cusnam = $("#custname").val();
                var custid = $rootScope.SerachCustomerID;
                var contno = $("#contno").val();
                var emil = $("#emailids").val();
                var descp = $("#textArea").val();
                var Priority = $scope.ddlPriority; //$('input[name=Priority]:checked').val() == null ? 1 : $('input[name=Priority]:checked').val();
                var group1 = $("#ddlGroup").val();
                var Group = group1.split(":")[1];
                var SubGroup = $scope.ddlSubGroup;
                var SubGroup1 = $("#ddlSubGroup").val();
                var SubGroup = SubGroup1.split(":")[1];
                var Subject = $("#txtSubject").val();
                var Accuracy = $("#txtAccuracy").val();
                var CustomerSatisfied = $("#txtCustomerSatisfied").val();
                var AssignedTo = $scope.ddlAssignedTo;
                var TicketAssignGroup = $scope.ddlAssignedGroup;
                var TicketType = $scope.ddlTicketType;
                var TicketChannel = 2;

                var fileInput = document.getElementById('file-6');
                for (i = 0; i < fileInput.files.length; i++) {
                    data.append(fileInput.files[i].name, fileInput.files[i]);
                }
                var setparams = {
                    cusnam: cusnam,
                    custid: custid,
                    emil: emil,
                    descp: descp,
                    contno: contno,
                    Page: "",
                    PageSize: "",
                    Priority: Priority,
                    Group: Group,
                    SubGroup: SubGroup,
                    Subject: Subject,
                    AssignedTo: AssignedTo,
                    TicketType: TicketType,
                    TicketAssignGroup: TicketAssignGroup,
                    ContractNumber: ContractNumber,
                    LabourNumber: LabourNumber,
                    TicketChannel: TicketChannel,
                    UserId: $rootScope.UserName,
                    accuracy: Accuracy,
                   
                };
                $.ajax({

                    type: "POST",
                    url: $rootScope.APiUrl + '/api/CreateTicketNew?UpdateTicketFields=' + JSON.stringify(setparams),
                    traditional: true,
                    data: data,
                    contentType: false,
                    processData: false,
                    success: successFunc,
                    error: errorFunc
                });
            }
            else {
                swal("fil mandatory fields !");

                if (($("#ddlGroup").val()).length == 0) {
                    $("#ddlGroup").next().show();
                }
                if (($("#ddlSubGroup").val()).length == 0) {
                    $("#ddlSubGroup").next().show();
                }
                if (($("#txtContractNo").val()).length == 0) {
                    $("#txtContractNo").next().show();
                }
               
                if (($("#textArea").val()).length == 0) {
                    $("#textArea").next().show();
                }
                if (($("#txtSubject").val()).length == 0) {
                    $("#txtSubject").next().show();
                }

                //if ($("#ddlAssignedTo").val() == '') {
                //    $("#ddlAssignedTo").next().show();
                //}
            }
            function successFunc(data, status) {
                swal(data);
                $("#createpopup").addClass("hide");
                GetCustomerTicketList();
            }
            function errorFunc() {
                swal('error');
                $("#createpopup").addClass("hide");
                GetCustomerTicketList();
            }
        });


        $scope.saveupdate = function (EnqId) {
            debugger;
            var f = new FormData();
            var data = new FormData();
            EnquiryId = parseInt(EnqId);
            comments = $scope.comments;
            status = $scope.StatusSelect;
            CustomerSatisfied = $("#ddlCustomerSatisfied").val()
            var checkIds = "";
            //var checkIds = $(".replyTy").filter(".active").data("stat");
            var ReplyType = "2";

            if (checkIds == "Internal") {
                var ReplyType = "1";
            }
            else {
                var ReplyType = "2";
            }

            var fileInput = document.getElementById('fileUploadQ');
            for (i = 0; i < fileInput.files.length; i++) {
                data.append(fileInput.files[i].name, fileInput.files[i]);
            }
            $.ajax({

                type: "POST",
                url: $rootScope.APiUrl + "/api/EnqList1?EnquiryId=" + JSON.stringify(EnquiryId) + "&status=" + JSON.stringify(status) + "&comments=" + JSON.stringify(comments) + "&ReplyType=" + ReplyType + "&UserId=" + $rootScope.UserName + "&CustomerSatisfied=" + CustomerSatisfied,
                traditional: true,

                //enctype: 'multipart/form-data',
                data: data,
                contentType: false,
                processData: false,
                success: successFunc,
                error: errorFunc
            });
        }
        function successFunc(data, status) {
            alert(data);
            $("#Ticketupdate").hide();
            $scope.comments = "";
            $scope.StatusSelecta = { "selected": null };
            $("#fileUploadQ").val("");


        }
        function errorFunc() {
            alert('error');
        }

        $scope.changestatus = function (status) {
            debugger;
            GetCustomerTicketList();
        }

        $scope.showCustomerSatisfied = false;
        $scope.changeStatusSelected = function () {
            debugger;
            var status = $scope.StatusSelect;
            if (status == 2)
                $scope.showCustomerSatisfied = true;
            else
                $scope.showCustomerSatisfied = false;
        };

        //************* Pagination *****************

        $scope.paginationPageSizes = [5, 10, 15, 25, 50],
        $scope.Ticketpaginate = function (value) {
            var begin, end, index;
            begin = ($scope.TicketcurrentPage - 1) * parseInt($scope.pagination.TicketnumPerPage);
            end = begin + parseInt($scope.pagination.TicketnumPerPage);
            index = $scope.CustomerTicketList.indexOf(value);
            return (begin <= index && index < end);
        };
        $scope.selectTicketpagination = function (value) {
            debugger;
            $scope.TicketcurrentPage = value;
        };
        $scope.changeTicketpagesize = function (value) {
            debugger;
            $scope.pagination.TicketnumPerPage = value;
            $scope.TicketcurrentPage = 1;
            $scope.Tickettotallist = Math.ceil($scope.CustomerTicketList.length / parseInt($scope.pagination.TicketnumPerPage));

        };
        //************** /Pagination ***********

        // ****************** Charts***********************
        colorsList = ["#F08080", "#20B2AA", "#222b6f", "#ff0000", "#e808cd", "#53a93f", "#8064a1", "#ff0734", "#db3b3b", "#0e33ea", "#c0504e", "#fb8334",]

        function TicketStatusChart() {
            debugger;
            
            var dataPoint = [
         { label: "Open Ticket", y: $scope.OpenTicket.Count, legendText: "Open Ticket", color: this.colorsList[0] },
         { label: "Close Ticket", y: $scope.CloseTicket.Count, legendText: "Close Ticket", color: this.colorsList[1] },
         { label: "OnHold Ticket", y: $scope.OnHoldTicket.Count, legendText: "OnHold Ticket", color: this.colorsList[2] },
         { label: "Resolved Ticket", y: $scope.ResolvedTicket.Count, legendText: "Resolved Ticket", color: this.colorsList[3] },
         { label: "Reopen Ticket", y: $scope.ReopenTicket.Count, legendText: "Reopen Ticket", color: this.colorsList[3] },
         { label: "Canceled Ticket", y: $scope.CanceledTicket.Count, legendText: "Canceled Ticket", color: this.colorsList[3] },
            ];

           this.OnboardChart = new CanvasJS.Chart("TicketStatusChart", {
                animationEnabled: true,
                exportEnabled: true,
                colorSet: ["#fb8334", "#53a93f", "#8064a1", "#ff0734", "#db3b3b"],
                title: {
                    text: ""
                },
                startAngle: 90,
                data: [{
                    type: "pie",
                    color: "LightSeaGreen",
                    indexLabelPlacement: "outside",
                    showInLegend: false,
                    legendText: "Ticket Status",
                    indexLabel: "{label} - {y}",
                    dataPoints: dataPoint
                }]
            });

            this.OnboardChart.render();

        }

        function TicketPriorityChart() {
            debugger;

            var dataPoint = [
         { label: "Low", y: $scope.LowTicket.length, legendText: "Low", color: this.colorsList[0] },
         { label: "Medium", y: $scope.MediumTicket.length, legendText: "Medium Ticket", color: this.colorsList[1] },
         { label: "High", y: $scope.HighTicket.length, legendText: "High", color: this.colorsList[2] },
            ];

            this.OnboardChart = new CanvasJS.Chart("TicketPriorityChart", {
                animationEnabled: true,
                exportEnabled: true,
                colorSet: ["#fb8334", "#53a93f", "#8064a1", "#ff0734", "#db3b3b"],
                title: {
                    text: ""
                },
                startAngle: 90,
                data: [{
                    type: "pie",
                    color: "LightSeaGreen",
                    indexLabelPlacement: "outside",
                    showInLegend: false,
                    legendText: "Ticket Status",
                    indexLabel: "{label} - {y}",
                    dataPoints: dataPoint
                }]
            });

            this.OnboardChart.render();

        }

      

        // ****************** /Charts***********************


    }]);