﻿
var app = angular.module('CallCenterPortalApp', ['ngRoute', 'xeditable', 'pascalprecht.translate', 'ngCookies', 'angular.css.injector', 'timer', 'ngSanitize',
    'ui.grid', 'ui.grid.pagination', 'ui.grid.cellNav', 'ui.grid.edit', 'ui.grid.resizeColumns', 'ui.grid.pinning', 'ui.grid.selection', 'ui.grid.moveColumns',
    'ui.grid.exporter', 'ui.grid.importer', 'ui.grid.grouping']);


app.directive('datedir', function ($timeout) {
    return {
        restrict: 'A',
        require: 'ngModel',
        //$(function(){
        //    element.datepicker({
        //        dateFormat:'dd/mm/yy',
        //        onSelect:function (date) {
        //            scope.$apply(function () {
        //                ngModelCtrl.$setViewValue(date);
        //            });
        //        }
        //    });
        //});
        link: function (scope, element, attrs, ngModelCtrl) {
            element.attr("readonly", true);
            element.datepicker({
                dateFormat: 'yy-mm-dd',
                set: function (date) {

                    scope.$apply(function () {
                        ngModelCtrl.$setViewValue(date);
                    });
                }
            }).on("changeDate", function (e) {
                ngModelCtrl.$setViewValue($(this).val());
            });

            if (attrs.format) {
                element.datepicker({
                    format: 'dd/MM/yyyy',

                });
            }

            //element[0].addEventListener("focusout", function (e) {
            //    alert("out");
            //    ngModelCtrl.$setViewValue($(this).val());
            //    $(".datepicker dropdown-menu").css("display","none")
            //}, true);


            //element.datepicker("option", 'onSelect', function () {
            //    alert($(this).val());
            //    ngModelCtrl.$setViewValue($(this).val());
            //});

            //element.bind('onSelect', function () {
            //    alert($(this).val());
            //    ngModelCtrl.$setViewValue( $(this).val());
            //});

        }


    };



});

app.config(function ($routeProvider) {
    $routeProvider

        .when('/', {
            templateUrl: 'CCPortal/templates/Home.html',
            controller: 'HomeCtrl'
        })
        .when('/home', {
            templateUrl: 'CCPortal/templates/Home.html',
            controller: 'HomeCtrl'
        })
        .when('/404', {
            templateUrl: 'CCPortal/templates/404.html',
            controller: 'NotFoundCtrl'
        })
        .when('/signin', {
            templateUrl: 'CCPortal/templates/SignIn.html',
            controller: 'SigninCtrl'
        })
        .when('/dashboard/:CustomerId', {
            templateUrl: 'CCPortal/templates/Dashbord.html',
            controller: 'DashbordCtrl'
        })
        .when('/customer/:CustomerId', {
            templateUrl: 'CCPortal/templates/Customer.html',
            controller: 'CustomerCtrl'
        })
        .when('/contract/:CustomerId', {
            templateUrl: 'CCPortal/templates/Contract.html',
            controller: 'ContractCtrl'
        })
        .when('/contract/:CustomerId/:ContractNumber', {
            templateUrl: 'CCPortal/templates/Contract.html',
            controller: 'ContractCtrl'
        })

        .when('/payment/:CustomerId', {
            templateUrl: 'CCPortal/templates/payment.html',
            controller: 'PaymentCtrl'
        })
        .when('/payment/:CustomerId/:PaymentNumber', {
            templateUrl: 'CCPortal/templates/payment.html',
            controller: 'PaymentCtrl'
        })

        .when('/ticket/:CustomerId/:status', {
            templateUrl: 'CCPortal/templates/Ticket.html',
            controller: 'TicketCtrl'
        })
        .when('/ticket/:CustomerId', {
            templateUrl: 'CCPortal/templates/Ticket.html',
            controller: 'TicketCtrl'
        })
         .when('/custservice/:CustomerId', {
             templateUrl: 'CCPortal/templates/CustomerServiceEnquiryList.html',
             controller: 'CustomerServiceEnquiryListCtrl'
         })
        .when('/ticketdetails/:CustomerId/:TicketId', {
            templateUrl: 'CCPortal/templates/TicketDetails.html',
            controller: 'TicketDetailsCtrl'
        })
    .when('/task/:CustomerId', {
        templateUrl: 'CCPortal/templates/Task.html',
        controller: 'TaskCtrl'
    })
                .when('/taskdetails/:CustomerId/:TaskId', {
                    templateUrl: 'CCPortal/templates/TaskDetails.html',
                    controller: 'TaskDetailsCtrl'
                })

        ;

});


app.filter("thousandSepfilter", function () {
    return function (x) {
        if (x > 0) {
            var val = parseFloat(x).toLocaleString(undefined, {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2
            });
            return val;

        }
        else {
            return x;
        }
    };
});

app.run(function ($rootScope, $filter, $timeout, $location) {

   //$rootScope.APiUrl = "http://localhost:44977";
    $rootScope.APiUrl = "https://erp.arco.sa:65/";

    var UserName = window.localStorage.getItem("UserName");
    if (UserName) {
        $rootScope.UserName = UserName;

        var CCPUser = window.localStorage.getItem("CCPUser");
        $rootScope.CCPUser = JSON.parse(CCPUser);
    }
    else {
        window.localStorage.clear();
        $rootScope.UserName = null;
        $location.path("/signin");
    }


    $rootScope.langfilter = function (object, field, prefixEn, suffixAr) {

        if (!object) {
            return "";
        }
        var lang = $rootScope.currentLang;

        if (lang == "Ar") {
            if (prefixEn == "2") {
                field = field.slice(0, -2);
            }
            if (suffixAr == "1") {
                if (object[field + 'AR']) {
                    return object[field + 'AR'];
                }
                else {
                    return object[field];
                }
            }
            else {
                if (object[field + 'Ar']) {
                    return object[field + 'Ar'];
                }
                else {
                    return object[field];
                }
            }
        }
        else {
            return object[field];
        }
    };


    $rootScope.initsqllitedb = function (source) {
        if (window.cordova) {
            try {
                db = $cordovaSQLite.openDB({ name: "my.db", iosDatabaseLocation: 'default' }); //device
            }
            catch (e) {
                alert(e.message);
            }
        }
        else {

            try {
                db = window.openDatabase("my.db", '1', 'my', 1024 * 1024 * 100); // browser
            }
            catch (e) {
                alert("Db - error " + e.message);
            }
        }
    };


    $rootScope.SwalMsg = function (Msg) {
        var msg = $filter('translate')(Msg);
        var btntext = $filter('translate')("Ok");
        swal({
            title: "",
            text: msg,
            confirmButtonText: btntext
        });

    }

    $rootScope.SwalMsgWithType = function (title, Msg, type, timer) {
        var msg = $filter('translate')(Msg);
        var textTitle = $filter('translate')(title);
        var btntext = $filter('translate')("Ok");
        swal({
            title: textTitle,
            type: type,
            text: msg,
            timer: timer,
            confirmButtonText: btntext

        });
        //swal(textTitle, text, type);         

    };


    $rootScope.SwalWarning = function (obj) {
        try {
            obj.title = $filter('translate')(obj.template);
            obj.type = obj.title;
            obj.showCancelButton = obj.cancelText;
            obj.confirmButtonColor = obj.confirmButtonColor;
            obj.confirmButtonText = $filter('translate')(obj.confirmButtonText);
            obj.closeOnConfirm = obj.closeOnConfirm;

        }
        catch (e) {
            alert(e);
        }
        return swal(obj);
    };

    $rootScope.WordTranslate = function (obj) {
        var text = $filter('translate')(obj);
        return text;
    };

    //$scope.getClass = function (path) {
    //    var cssclass = "";
    //    if (path)
    //        cssclass = $location.path().substr(0, path.length) === path ? 'active' : '';

    //    return cssclass;
    //};
});

app.directive('datedirtodayfuture', function ($timeout) {
    return {
        restrict: 'A',
        require: 'ngModel',
        //$(function(){
        //    element.datepicker({
        //        dateFormat:'dd/mm/yy',
        //        onSelect:function (date) {
        //            scope.$apply(function () {
        //                ngModelCtrl.$setViewValue(date);
        //            });
        //        }
        //    });
        //});
        link: function (scope, element, attrs, ngModelCtrl) {
            element.attr("readonly", true);
            element.datepicker({
                format: 'dd/mm/yyyy',
                onRender: function (date) {
                    return date.valueOf() < new Date().valueOf() ? 'disabled' : '';
                },
                set: function (date) {

                    scope.$apply(function () {
                        ngModelCtrl.$setViewValue(date);
                    });
                }
            }).on("changeDate", function (e) {
                ngModelCtrl.$setViewValue($(this).val());
            });

            if (attrs.format) {
                element.datepicker({
                    format: 'dd/mm/yyyy',

                });
            }

            //element[0].addEventListener("focusout", function (e) {
            //    alert("out");
            //    ngModelCtrl.$setViewValue($(this).val());
            //    $(".datepicker dropdown-menu").css("display","none")
            //}, true);


            //element.datepicker("option", 'onSelect', function () {
            //    alert($(this).val());
            //    ngModelCtrl.$setViewValue($(this).val());
            //});

            //element.bind('onSelect', function () {
            //    alert($(this).val());
            //    ngModelCtrl.$setViewValue( $(this).val());
            //});

        }


    };



});


app.config(function ($translateProvider) {


    var cookie = readCookie('MyLanguage');
    if (cookie) {

        $translateProvider.preferredLanguage(cookie == '%22Eng%22' ? 'Eng' : 'Ar');
    } else {
        $translateProvider.preferredLanguage('Eng');
    }

    //$translateProvider.preferredLanguage('Eng');
    $translateProvider.useLoader('asyncLoader');
});


function readCookie(name) {

    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}



app.factory('asyncLoader', function ($rootScope, $q, $timeout, $http, $cookies, $cookieStore) {

    return function (options) {
        var deferred = $q.defer(),
            translations;
        $cookieStore.remove('MyLanguage');
        $cookieStore.put('MyLanguage', options.key);
        //  $cookies['MyLanguage'] = options.key;

        //$rootScope.APiUrl = "http://localhost:44977";
        $rootScope.APiUrl = "https://erp.arco.sa:65/";

        if (options.key === 'Eng') {
            
            $http.get($rootScope.APiUrl + "/api/GetLanguageValues")
                .success(function (data) {
                    deferred.resolve(data[0].source);
                });

        } else {
            $http.get($rootScope.APiUrl + "/api/GetLanguageValues")
                .success(function (data) {
                    deferred.resolve(data[1].source);
                });
        }

        //$timeout(function () {
        //    deferred.resolve(translations);
        //}, 2000);

        return deferred.promise;
    };
});

app.controller('CallCenterPortalMainCtrl', ['$scope', '$rootScope', '$http', '$translate', '$route', '$location',
    function ($scope, $rootScope, $http, $translate, $route, $location) {

        $scope.getClass = function (path) {
            return ($location.path().substr(0, path.length) === path) ? 'active' : '';
        };

        $scope.getSearchCustomer = function (event) {
            debugger;
            var CustomerFilter = $scope.CustomerFilter;
            $http.get($rootScope.APiUrl + "/api/GetCallCenterbyCustomerAcutoComplete?term=" + CustomerFilter)
                .success(function (datam) {
                    $scope.customerlist = datam;
                });
        };

        $scope.selectCustomer = function (list) {
            $rootScope.SerachCustomerID = list.CustomerID;
            var url = "#/dashboard/" + list.CustomerID;
            window.open(url);
        };



    }]);




app.controller('ChangeLang', ['$scope', '$rootScope', '$http', '$timeout', '$translate', 'cssInjector',
    function ($scope, $rootScope, $http, $timeout, $translate, cssInjector) {


        function checkquerystring(field) {
            var url = window.location.href;
            if (url.indexOf('?' + field + '=') != -1)
                return true;
            else if (url.indexOf('&' + field + '=') != -1)
                return true;
            return false
        }

        if (checkquerystring("Lang")) {
            getparameterlang();
        }

        function getparameterlang() {
            debugger;
            var Lang = GetParameterValues('Lang');
            //  alert(Lang);
            if (Lang == "Ar") {
                $scope.currentLanguage = "Ar";
                $translate.use($scope.currentLanguage);
                $translate.preferredLanguage($scope.currentLanguage);
                $rootScope.currentLang = $scope.currentLanguage;
            }
            else if (Lang == "Eng") {
                $scope.currentLanguage = "Eng";
                $translate.use($scope.currentLanguage);
                $translate.preferredLanguage($scope.currentLanguage);
                $rootScope.currentLang = $scope.currentLanguage;
            }

            var url = window.location.href;
            window.location.href = removeURLParameter(url, 'Lang');
            //window.location.reload();
        }

        function removeURLParameter(url, parameter) {
            //prefer to use l.search if you have a location/link object
            var urlparts = url.split('?');
            if (urlparts.length >= 2) {

                var prefix = encodeURIComponent(parameter) + '=';
                var pars = urlparts[1].split(/[&;]/g);

                //reverse iteration as may be destructive
                for (var i = pars.length; i-- > 0;) {
                    //idiom for string.startsWith
                    if (pars[i].lastIndexOf(prefix, 0) !== -1) {
                        pars.splice(i, 1);
                    }
                }

                url = urlparts[0] + '?' + pars.join('&');
                return url;
            } else {
                return url;
            }
        }
        function GetParameterValues(param) {
            var url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
            for (var i = 0; i < url.length; i++) {
                var urlparam = url[i].split('=');
                if (urlparam[0] == param) {
                    return urlparam[1];
                }
            }
        }

        $scope.currentLanguage = $translate.preferredLanguage();
        $rootScope.currentLang = $translate.preferredLanguage();


        $scope.changeLanguage = function () {
            //$scope.currentLanguage = key;
            $translate.use($scope.currentLanguage);
            $translate.preferredLanguage($scope.currentLanguage);
            $rootScope.currentLang = $scope.currentLanguage;
            //window.location.reload();
            changelanguage();
        };

        $scope.changeLang = function (lang) {
            $scope.currentLanguage = lang;
            $translate.use($scope.currentLanguage);
            $translate.preferredLanguage($scope.currentLanguage);
            //window.location.reload();

            $rootScope.currentLang = $scope.currentLanguage;
            changelanguage();

        };

        function changelanguage() {

            if ($scope.currentLanguage == "Ar") {
                cssInjector.removeAll();
                cssInjector.add("/IndividualCustomerPortalNew/css/bootstrap-rtl.min.css");
                cssInjector.add("/IndividualCustomerPortalNew/css/style-rtl.css");
            }
            else {
                cssInjector.removeAll();
                cssInjector.add("/IndividualCustomerPortalNew/css/bootstrap.min.css");
                cssInjector.add("/IndividualCustomerPortalNew/css/style.css");
            }

            setTimeout(function () {
                $("#pagestyle1").remove();
                $("#pagestyle2").remove();
            });


        }

        changelanguage();

    }]);

app.service('ApiCall', ['$http', function ($http) {
    var result;

    // This is used for calling get methods from web api
    this.get = function (url, obj) {

        var response = $http({
            method: "get",
            url: url,
            params: obj
            //data: data,
        });

        //result = $http.get(url, obj).success(function (data, status) {
        //    result = (data);
        //}).error(function () {
        //    alert("Something went wrong");
        //});
        return response;
    };

    // This is used for calling post methods from web api with passing some data to the web api controller
    this.post = function (url, obj) {
        //  $http.defaults.headers.common['Authorization'] = "Basic sk_test_GNhhspRZe8jQxZkn9bGCTUnHo5QTvxJJimnvk4k6:123";
        var response = $http({
            method: "post",
            url: url,
            //   headers: { 'WWW-Authenticate': 'Basic sk_test_GNhhspRZe8jQxZkn9bGCTUnHo5QTvxJJimnvk4k6:123' },
            params: obj
            //data: data,
        });
        //result = $http.post(url, obj).success(function (data, status) {
        //    result = (data);
        //}).error(function () {
        //    alert("Something went wrong");
        //});
        return response;
    };

}]);

app.directive('loading', ['$http', function ($http) {
    return {
        restrict: 'A',
        link: function (scope, elm, attrs) {
            scope.isLoading = function () {
                return $http.pendingRequests.length > 0;
            };
            scope.$watch(scope.isLoading, function (v) {
                if (v) {
                    elm.show();
                } else {
                    elm.hide();
                }
            });
        }
    };
}]);

app.filter("jsToPlainFormatDate", function ($filter) {
    return function (x) {
        if (x) {
            return new Date(parseInt(x.substr(6)));
        }
        else {
            return x;
        }
    };
});

app.filter("thousandSepfilter", function () {
    return function (x) {
        if (x > 0) {
            var val = parseFloat(x).toLocaleString(undefined, {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2
            });
            return val;

        }
        else {
            return x;
        }
    };
});
app.filter("BoolToString", function () {

    return function (x) {

        var s = x == true ? "Yes" : "No";
        return s;

    };
});

app.filter("jsToFormateDate", function ($filter) {
    return function (x) {
        if (x) {
            var c = new Date(parseInt(x.substr(6)));

            return $filter('date')(c, 'dd/MM/yyyy');

        }
        else {
            return x;
        }
    };
});
app.filter("jsToFormateDateMM", function ($filter) {
    return function (x) {
        if (x) {
            var c = new Date(parseInt(x.substr(6)));

            return $filter('date')(c, 'MM/dd/yyyy');

        }
        else {
            return x;
        }
    };


    app.filter("jsToFormateDateString", function ($filter) {
        return function (x) {
            if (x) {
                var c = new Date(x.substr(6));

                return $filter('date')(c, 'dd/MM/yyyy');

            }
            else {
                return x;
            }
        };
    });
});
