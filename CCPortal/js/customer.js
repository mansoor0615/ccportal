﻿app.controller('CustomerCtrl', ['$scope', '$rootScope', '$http', '$routeParams', '$filter',
    function ($scope, $rootScope, $http, $routeParams, $filter) {

        debugger;
        $scope.pagination = {
            CustomerDelegationnumPerPage: "5",
            ContractListnumPerPage: "5",
            LocationListnumPerPage: "5",
            LocationContactListnumPerPage: "5",
            BContractListnumPerPage: "5",
            CustomerMessageSendnumPerPage: "5",

        }


        $rootScope.SerachCustomerID = $routeParams.CustomerId;

        GetCCustomerDetailsByCustomerId();

        function GetCCustomerDetailsByCustomerId() {
            var CustomerId = $rootScope.SerachCustomerID;
            if (CustomerId.length > 0) {

                $http.get($rootScope.APiUrl + "/api/GetDetailsByIndiviualCustomerPortal?CustomerID=" + CustomerId + "&PortalType=1")
                    .success(function (emp) {
                        debugger;
                        $scope.user = emp;
                        if (emp.DOBGeorgian != null) {
                            hijidate1($scope.user.DOBGeorgian);
                        }
                        $scope.user.OldIDNumber = $scope.user.IDNumber;
                        //GetCustomerContractList();
                        //LoadDelegationGrid();
                        LoadMessageSendListGrid();
                        //$scope.loadLocationList();
                        //$scope.loadLocationContactList();
                        $scope.getbanklist();
                    });

            }
        }

        function hijidate1(date) {

            $http.get($rootScope.APiUrl + "/api/ConvertHijiriDate?date=" + date)
                .success(function (data) {
                    debugger;
                    $scope.user.DOBHijri = data;
                    $("#uesrHijriDOB").text(data);
                });
        }

        $scope.customermobileupdatefn = {};

        $scope.customermobileupdatefn.refreshfn = GetCCustomerDetailsByCustomerId;


        //if (ParaContractNo && ParaContractNo.length > 0) {
        //    $scope.CustomerId = ParaContractNo;
        //    GetContractDetailsByContractNumber();
        //}
        $scope.showcontractlist = false;
        $scope.clickbody = function ($event) {
            if (!$($event.target).is('#txtContractNumber')) {
                $scope.showcontractlist = false;
            }
        };
        $scope.NationalityDrop = [];
        $scope.loadNationalityDrop = function () {
            debugger;
            $http.get($rootScope.APiUrl + "/api/Nationality")
       .success(function (emp) {
           debugger;
           $scope.NationalityDrop = emp;
       });
        };
        $scope.loadNationalityDrop();


        //$scope.getcontract = function (e) {
        //    debugger;
        //    var CustomerId = $routeParams.CustomerId;
        //    if (e.keyCode == 13) {
        //        GetCCustomerDetailsByCustomerId();
        //    }
        //    else {
        //        if (contractnumber.length > 0) {
        //            $http.post($rootScope.APiUrl + "/IndividualSector/GetCustomerAcutoComplete?term=" + CustomerId)
        //                .success(function (datam) {
        //                    debugger;
        //                    $scope.contractlist = datam;
        //                    $scope.showcontractlist = true;

        //                });
        //        }
        //        else {
        //            $scope.showcontractlist = false;
        //        }
        //    }
        //};


        $scope.selectcontract = function (list) {
            $scope.ContractNumber = list.ContractNumber;
            $scope.showcontractlist = false;
            GetCCustomerDetailsByCustomerId();
        };
        $scope.getdatails = function () {
            GetCCustomerDetailsByCustomerId();
        };

        $scope.GetCustomerContractList = function () {
            GetCustomerContractList();
        }
        function GetCustomerContractList() {
            debugger;

            $http.get($rootScope.APiUrl + "/api/GetCustomerContractLists?CustomerId=" + $rootScope.SerachCustomerID)
                .success(function (data) {
                    debugger;
                    data.forEach(function addDates(row, index) {
                        debugger;
                        var currentTime = new Date();
                        currentTime.setDate(currentTime.getDate() + row.TotalRemainingDays);
                        row.ExpectedEndDate = currentTime;
                    });
                    $scope.CustomerContractList = data;
                    $scope.ContractListcurrentPage = 1;
                    $scope.pagination.ContractListnumPerPage = "5";
                    $scope.ContractListtotallist = Math.ceil($scope.CustomerContractList.length / parseInt($scope.pagination.ContractListnumPerPage));

                });
        }


        $scope.getContractPaymentList = function () {
            getContractPaymentList();
        };

        function getContractPaymentList() {
            debugger;
            $http.get($rootScope.APiUrl + "/api/GetCustomerPaymentList?CustomerId=" + $routeParams.CustomerId)
                .success(function (datam) {
                    debugger;
                    $scope.ContractPaymentList = datam;

                });
        }

        $scope.GetSalaryProcess = function (list) {
            debugger;
            var SalaryRecId = list.PaymentNumber;
            $scope.showSalaryrecid = list.PaymentNumber;
            $(".closesalary").hide();
            $(".showsalary").show();
            $("#showsalary_" + list.PaymentNumber).hide();
            $("#closesalary_" + list.PaymentNumber).show();
            if ($scope.tempSalaryRecId != SalaryRecId) {
                $http.get($rootScope.APiUrl + "/api/GetSettledPaymentList?PaymentNumber=" + SalaryRecId)
                    .success(function (datam) {
                        debugger;
                        $scope.SettledPaymentList = datam;
                        $scope.tempSalaryRecId = SalaryRecId;
                        $scope.showsalaryTransaction = true;


                    });
            }
            else {
                $scope.showsalaryTransaction = true;
            }
        };

        $scope.CloseGetSalaryProcess = function (list) {

            $(".closesalary").hide();
            $(".showsalary").show();
            $scope.showsalaryTransaction = false;
        };

        $scope.getContractAdvancePaymentList = function () {
            getContractAdvancePaymentList();
        };
        function getContractAdvancePaymentList() {

            debugger;
            $http.get($rootScope.APiUrl + "/api/GetAdvanceCustomerPaymentList?CustomerId=" + $routeParams.CustomerId)
                .success(function (datam) {
                    debugger;
                    $scope.ContractAdvancePaymentList = datam;
                });
        }


        $scope.GetAdvanceReturnProcess = function (list) {
            debugger;
            var AdvanceRecId = list.AdvanceNumber;
            $scope.showAdvanceReturnrecid = list.AdvanceNumber;
            $(".closeAdvance").hide();
            $(".showAdvance").show();
            $("#showAdvance_" + list.AdvanceNumber).hide();
            $("#closeAdvance_" + list.AdvanceNumber).show();
            if ($scope.tempAdvanceRecId != AdvanceRecId) {
                $http.get($rootScope.APiUrl + "/api/GetAdvanceSettlement?AdvanceNumber=" + AdvanceRecId)
                    .success(function (datam) {
                        debugger;

                        $scope.SettledPaymentList1 = datam;
                        $scope.tempAdvanceRecId = AdvanceRecId;
                        $scope.showAdvanceReturn = true;

                    });

                $http.get($rootScope.APiUrl + "/api/GetAdvanceInvoiceSettlement?AdvanceNumber=" + AdvanceRecId)
                    .success(function (datam) {
                        debugger;

                        $scope.AdvanceInvoiceSettleList = datam;
                        $scope.tempAdvanceRecId = AdvanceRecId;
                        $scope.showAdvanceReturn = true;

                    });
            }
            else {
                $scope.showAdvanceReturn = true;
            }
        };

        $scope.CloseGetAdvanceReturnProcess = function (list) {

            $(".closeAdvance").hide();
            $(".showAdvance").show();
            $scope.showAdvanceReturn = false;
        };

        $scope.GetCustomerInvoiceByCustomerNumber = function () {
            GetCustomerInvoiceByCustomerNumber();
        }
        function GetCustomerInvoiceByCustomerNumber() {

            $http.get($rootScope.APiUrl + "/api/GetCustomerInvoiceByCustomerNumber?CustomerId=" + $routeParams.CustomerId)
                .success(function (data) {
                    debugger;
                    $scope.CustInvoice = data;

                });
        }

        $scope.GetContractTransactionByContractNumber = function () {
            GetContractTransactionByContractNumber();
        };

        function GetContractTransactionByContractNumber() {

            $http.get($rootScope.APiUrl + "/api/GetContractTransactionByCustomerId?CustomerId=" + $routeParams.CustomerId)
                .success(function (data) {
                    debugger;
                    $scope.ContractTransaction = data;

                });
        }
        $scope.getCustomerRefundList = function () {
            getCustomerRefundList();
        };

        function getCustomerRefundList() {

            debugger;
            $http.get($rootScope.APiUrl + "/api/GetCustomerRefundList?CustomerId=" + $routeParams.CustomerId)
                .success(function (datam) {
                    debugger;
                    $scope.CustomerRefundList = datam;
                });
        }

        $scope.GetRefundReturnProcess = function (list) {
            debugger;
            var RefundRecId = list.RefundNumber;
            $scope.showRefundReturnrecid = list.RefundNumber;
            $(".closeRefund").hide();
            $(".showRefund").show();
            $("#showRefund_" + list.RefundNumber).hide();
            $("#closeRefund_" + list.RefundNumber).show();
            if ($scope.tempRefundRecId != RefundRecId) {
                $http.get($rootScope.APiUrl + "/api/GetRefundSettlement?RefundNumber=" + RefundRecId)
                    .success(function (datam) {
                        debugger;

                        $scope.RefundSettledList = datam;
                        $scope.tempRefundRecId = RefundRecId;
                        $scope.showRefundReturn = true;

                    });
            }
            else {
                $scope.showRefundReturn = true;
            }
        };

        $scope.showbankpopup = function (id) {
            $("#showbankpopup").show();
            $scope.CreateMob = {};

        };
        $scope.closebankpopup = function (id) {
            $("#showbankpopup").hide();

        };

        $scope.CloseGetRefundReturnProcess = function (list) {

            $(".closeRefund").hide();
            $(".showRefund").show();
            $scope.showRefundReturn = false;
        };

        $scope.GetDiscountByCustomerId = function () {
            GetDiscountByCustomerId();
        };
        function GetDiscountByCustomerId() {

            $http.get($rootScope.APiUrl + "/api/GetDiscountByCustomerId?CustomerID=" + $routeParams.CustomerId)
                .success(function (data) {
                    debugger;
                    $scope.Discount = data;
                });
        }



        $scope.genderchange = function (id) {
            if (id == 1) {
                $scope.IsMale = true;
            }
            else {
                $scope.IsMale = false;
            }

        };

        $scope.Gender = [
     { value: 1, text: 'MALE' },
    { value: 2, text: 'FEMALE' },
        ];

        $scope.Nationality = [];
        $scope.loadNationality = function () {
            debugger;
            return $scope.Nationality.length ? null : $http.get($rootScope.APiUrl + "/api/GetNationality")
       .success(function (emp) {
           debugger;
           $scope.Nationality = emp;
       });
        };
        $scope.loadNationality();

        $scope.showNationality = function () {
            if ($scope.user && $scope.user.Nationality) {
                if ($scope.Nationality && $scope.Nationality.length) {
                    var selected = $filter('filter')($scope.Nationality, { Id: $scope.user.Nationality });
                    return selected.length ? selected[0].Desc : 'Not set';
                } else {
                    return $scope.user.Nationality;
                }
            }
            else {
                return 'Not set';
            }
        };

        $scope.Gethiridate = function (date) {
            debugger;
            hijidate1(date);
        };




        $scope.WorkingSectorDrop = [
            { value: "0", text: "-- Select --" },
       { value: "1", text: "Government" },
      { value: "2", text: "Semi Government" },
      { value: "3", text: "Private" },


        ];


        $scope.KnowViaDrop = [
           { value: 0, text: 'Website', textar: 'الموقع الالكتروني' },
       { value: 1, text: 'Search Engine', textar: 'محرك بحث' },
      { value: 2, text: 'Marketing Exhibition', textar: 'معرض تسويقي' },
       { value: 3, text: 'Friend Referral', textar: 'صديق' },
       { value: 4, text: 'Twitter', textar: 'تويتر' },
      { value: 5, text: 'Instagram', textar: 'انستقرام' },
       { value: 6, text: 'Snapchat', textar: 'سناب شات ' },
       { value: 7, text: 'Facebook', textar: 'الفيس بوك' },
      { value: 8, text: 'SMS', textar: 'رسائل نصية' },
       { value: 9, text: 'Call from Fts', textar: 'اتصال من الشركة' },
       { value: 10, text: 'LinkedIn', textar: 'لينكد إن' },
      { value: 11, text: 'Advertisement boards', textar: 'لوحات إعلانية' },
        ];




        $scope.SectorDrop = [
            { value: 0, text: "-- Select --", textAr: "-- اختار --" },
            { value: 1, text: "Government", textAr: "حكومي" },
            { value: 2, text: "Semi Government", textAr: "شبه حكومي" },
            { value: 3, text: "Private", textAr: "خاص" },

        ];

        $scope.NatureofBusinessDrop = [
            { value: "1", text: 'Arts and entertainment', textAr: 'فنون و ثقافة' },
            { value: "2", text: 'Automotive', textAr: 'السيارات' },
            { value: "3", text: 'Business', textAr: 'أعمال' },
            { value: "4", text: 'Computers', textAr: 'الحاسب الآلي' },
            { value: "5", text: 'Games', textAr: 'ألعاب' },
            { value: "6", text: 'Health', textAr: 'صحة' },
            { value: "7", text: 'Internet', textAr: 'إنترنت' },
            { value: "8", text: 'News and Media', textAr: 'أخبار و إعلام' },
            { value: "9", text: 'Recreation', textAr: 'ترفيه' },
            { value: "10", text: 'Reference', textAr: 'مرجع' },
            { value: "11", text: 'Shopping', textAr: 'تسوق' },
            { value: "12", text: 'Sports', textAr: 'رياضة' },
            { value: "13", text: 'Others', textAr: 'أخرى' },

        ];

        $scope.SocialMediaDrop = [
{ value: "1", text: 'Twitter' },
{ value: "2", text: 'Instagram' },
{ value: "3", text: 'Snapchat' },
{ value: "4", text: 'Facebook' },
{ value: "5", text: 'LinkedIn' },

        ];

        var IdentityValidator = {
            ValidateIdentity: function (idNumber, IdType) {
                if (idNumber.substr(0, 1) != IdType) {
                    return 12;
                }
                else {
                    if (CheckIdentity(idNumber) != 0) {
                        return 12;
                    }
                    else
                        return 0;
                }
            }
        }

        function CheckIdentity(IDNO) {
            debugger;
            var Result; // int
            var Type; //varchar(1);
            var i; //int;
            var ZFOdd; //varchar(max);
            var SubString; // varchar(1);
            var InTVALUE1; //int;
            var InTVALUE2; //int;
            var sum; //int;
            Result = 1;
            sum = 0;

            Type = IDNO.substr(0, 1);

            if (IDNO.length != 10) {
                return Result = 11;
            }
            else {

                if (Type != '1' && Type != '2') {
                    return Result = 12;
                }
                else {
                    var i = 0;
                    while (i < 10) {
                        if (i % 2 == 0) {
                            SubString = IDNO.substr(i, 1);
                            InTVALUE1 = parseInt(SubString) * 2;
                            if (InTVALUE1.toString().length == 1) {
                                ZFOdd = '0' + InTVALUE1.toString();
                            }
                            else if (InTVALUE1.toString().length == 0) {
                                ZFOdd = '00';
                            }
                            else {
                                ZFOdd = InTVALUE1.toString();
                            }
                            InTVALUE1 = parseInt(ZFOdd.substr(0, 1));
                            InTVALUE2 = parseInt(ZFOdd.substr(1, 1));
                            sum = sum + InTVALUE1 + InTVALUE2;
                        }
                        else {
                            sum = sum + parseInt(IDNO.substr(i, 1));
                        }
                        i = i + 1;
                    } // while loop
                }
                Result = sum % 10;

            }
            return Result;
        }

        function uploadCustomerProfile(id, datass) {
            debugger;
            var fileinput = document.getElementById('uploadCustomerProfile');

            if (fileinput != null && fileinput.files.length > 0) {

                var fileInput1 = fileinput.files[0];
                var name = fileinput.files[0].name;
                var ext = name.match(/\.(.+)$/)[1];
                if (fileInput1 && (ext == "jpg" || ext == "jpeg" || ext == "png")) {

                    debugger;
                    var data = new FormData();

                    data.append("CustomerId", id);

                    for (i = 0; i < fileinput.files.length; i++) {
                        data.append(fileinput.files[i].name, fileinput.files[i]);
                    }


                    $.ajax({
                        type: "POST",
                        url: $rootScope.APiUrl + "/UserRoleMapping/setprofilepicForCustomer",
                        traditional: true,
                        contentType: "multipart/mixed",
                        contentType: false,
                        processData: false,
                        data: data,
                        success: successFunc,
                        error: errorFunc
                    });
                    function successFunc(data, Status) {
                        if (data != "") {
                            swal(datass);
                            $(".header-avatar").attr("src", data);
                        }
                        GetData();
                        $("#uploadCustomerProfile").val("");
                    }
                    function errorFunc(data, Status) {
                        alert('error');
                        $("#uploadCustomerProfile").val("");
                    }
                }
            }
        }


        $scope.haveIdEditable = true;
        $scope.saveCustomer = function (data, id) {
            debugger;
            var profilepic = $("#uploadCustomerProfile").val();
            //$scope.editableForm.$cancel();
            angular.extend(data, { id: id });
            var myobj = {

                Location: $scope.user.LocationInfo,
                LocationContact: $scope.user.LocationContactInfo,
            };
            if ($scope.user && !$scope.user.OldIDNumber) {
                if (!data.IDNumber) {
                    swal("IDNumber Not Found");
                    return;
                }

                var idnumberentitytype = data.IDNumber.substring(0, 1);
                var ValidateResult = IdentityValidator.ValidateIdentity(data.IDNumber, idnumberentitytype);
                if (ValidateResult != 0) {
                    swal("InCorrect IDNumber");
                    return;
                }
            }
            var setparams = {
                CustomerID: JSON.stringify($routeParams.CustomerId),
                FirstName: JSON.stringify(data.FirstName),
                MiddleName: JSON.stringify(data.MiddleName),
                LastName: JSON.stringify(data.LastName),
                FamilyName: JSON.stringify(data.FamilyName),
                Nationality: JSON.stringify(data.Nationality),
                IDNumber: JSON.stringify(data.IDNumber),
                DOBGeorgian: JSON.stringify(data.DOBGeorgian),
                MobileNumber: JSON.stringify(data.MobileNumber),
                PhoneNumber: JSON.stringify(data.PhoneNumber),
                Email: JSON.stringify(data.Email),
                FaxNumber: JSON.stringify(data.FaxNumber),
                POBOXNumber: JSON.stringify(data.POBOXNumber),
                City: JSON.stringify(data.City),
                CountryRegion: JSON.stringify(data.CountryRegion),
                Street: JSON.stringify(data.Street),
                PostalCode: JSON.stringify(data.PostalCode),
                WorkingSector: JSON.stringify(data.WorkingSector),
                OccupationDesc: JSON.stringify(data.OccupationDesc),
                WorkPlace: JSON.stringify(data.WorkPlace),
                MonthlySalary: JSON.stringify(data.MonthlySalary),
                OtherIncome: JSON.stringify(data.OtherIncome),
                WorkTel: JSON.stringify(data.WorkTel),
                WorkEmail: JSON.stringify(data.WorkEmail),
                WorkFax: JSON.stringify(data.WorkFax),
                WorkMailBox: JSON.stringify(data.WorkMailBox),
                NoofFamilyMembers: JSON.stringify(data.NoofFamilyMembers),
                NoofWifes: JSON.stringify(data.NoofWifes),
                OldAgePerson: JSON.stringify(data.OldAgePerson),
                WorkAddress: JSON.stringify(data.WorkAddress),
                SpecialDisabled: JSON.stringify(data.SpecialDisabled),
                OldAgePersonNotes: JSON.stringify(data.OldAgePersonNotes),
                SpecialDisabledNotes: JSON.stringify(data.SpecialDisabledNotes),
                Gender: JSON.stringify(data.Gender),
                KnowVia: JSON.stringify(data.KnowVia),
                Notes: JSON.stringify(data.Notes),
                LocationFields: JSON.stringify(myobj),
                FemaleContact: data.FemaleContact,
                AuthorizedPerson: data.AuthorizedPerson,
                AuthorizedNo: data.AuthorizedNo,
                SocialMedia: data.SocialMedia,
            };
            var response = $http({
                method: "post",
                url: $rootScope.APiUrl + "/api/UpdateCustomerApiInObj?UpdateCustomerFields=" + JSON.stringify(setparams),
                params: {
                    //UpdateCustomerFields: JSON.stringify(setparams),
                }

            });
            response.then(function (emp) {
                debugger;
                if (emp.data.msg == "Customer Registered Edit Successfully") {
                    if (profilepic) {
                        uploadCustomerProfile($routeParams.CustomerId, emp.data);

                    }
                    else {
                        // alert(emp.data);
                        swal("", emp.data.msg);
                        GetCCustomerDetailsByCustomerId();
                    }
                }
                else {
                    //  alert(emp.data);
                    swal("", emp.data.msg);
                    GetCCustomerDetailsByCustomerId();
                }

            });
        };

        $scope.getaffiliatecode = function () {
            debugger;
            $http.get($rootScope.APiUrl + "/api/GetAffiliateCodebyGenarate?CustomerId=" + $routeParams.CustomerId) //GetAffiliateCodebyPromotion //GetAffiliateCode
           .success(function (data) {
               debugger;
               swal(data);
               GetCCustomerDetailsByCustomerId();
           });
        }

        $scope.LoadDelegationGrid = LoadDelegationGrid();

        function LoadDelegationGrid() {
            debugger;
            $http.get($rootScope.APiUrl + "/api/GetCustomerDeligationList?Customer=" + $routeParams.CustomerId)
      .success(function (data) {
          $scope.CustomerDelegationList = data.Data;
          $scope.CustomerDelegationcurrentPage = 1;
          $scope.pagination.CustomerDelegationnumPerPage = "5";
          $scope.CustomerDelegationtotallist = Math.ceil($scope.CustomerDelegationList.length / parseInt($scope.pagination.CustomerDelegationnumPerPage));
      });
        };

        $scope.CreateDeligationClick = function () {
            $scope.IsEdit = false;
            $scope.IsCreate = true;
            $scope.custdelegation = {
            };
            $scope.editableForm5.$cancel();
            $("#CreateCustomerDeligationPopup").show();
            $scope.editableForm5.$show();
        };

        $scope.CreateDeligationhide = function () {
            $scope.editableForm5.$cancel();
            $("#CreateCustomerDeligationPopup").hide();
            $scope.editableForm5.$show();
        };



        $scope.CurrentCustomerDeligation = {};
        $scope.saveCustomerDeligation = function (data, id) {
            debugger;
            $scope.editableForm5.$show();
            if ($scope.IsCreate == true) {
                debugger;
                var response = $http({
                    method: "post",
                    url: $rootScope.APiUrl + "/api/CreateCustomerDeligation",
                    params: {
                        CustDeligation: JSON.stringify($scope.custdelegation),
                        CustomerId: $routeParams.CustomerId,
                    }
                });
                response.then(function (emp) {
                    debugger;
                    //swal("", emp.data, "success");
                    if (emp.data.status == true) {
                        swal("", emp.data.msg, "success");
                        $scope.CurrentCustomerDeligation = emp.data.data;
                        $("#CreateCustomerDeligationPopup").hide();
                        $scope.custdelegateverificationcode = "";
                        $("#CustDeligateVerifypopup").show();
                        LoadDelegationGrid();
                    }
                    else {
                        swal(emp.data.msg);
                        $scope.editableForm5.$show();
                    }
                });
            }
            else {
                angular.extend(data, { id: id });
                var response = $http({
                    method: "post",
                    url: $rootScope.APiUrl + "/IndividualSector/UpdateLocationContact",
                    params: {
                        setup:
                            JSON.stringify($scope.userlocationcontact)
                    }
                });
                response.then(function (emp) {
                    debugger;
                    swal("", emp.data, "success");
                    //griddata();
                    $("#CreateCustomerDeligationPopup").hide();
                });
            }
        };

        $scope.VerifyCustDeligateClick = function (e) {
            debugger;
            $scope.CurrentCustomerDeligation = e;
            $scope.custdelegateverificationcode = "";
            $("#CustDeligateVerifypopup").show();
        };

        $scope.verifyCustDeligation = function () {
            var AccessCode = $scope.custdelegateverificationcode;
            var setup = JSON.stringify($scope.CurrentCustomerDeligation);
            $http.post($rootScope.APiUrl + "/api/VerifyCustDelegation?CurrentData=" + setup + "&AccessCode=" + AccessCode)
            .success(function (emp) {
                debugger;
                if (emp.status == true) {
                    swal("", emp.msg, "success");
                    $("#CustDeligateVerifypopup").hide();
                    LoadDelegationGrid();
                }
                else {
                    swal(emp.msg);
                    $scope.custdelegateverificationcode = "";
                }
            });
        }

        $scope.resendverifyCustDeligationcode = function () {
            var setup = JSON.stringify($scope.CurrentCustomerDeligation);
            $http.post($rootScope.APiUrl + "/api/ResendCustDelegation?RecId=" + setup)
            .success(function (emp) {
                debugger;
                if (emp.status == true) {
                    swal(emp.msg);
                }
                else {
                    swal(emp.msg);
                }
            });
        }

        $scope.ActiveCustDeligateClick = function (Custdelegation) {
            $scope.CurrentCustomerDeligation = Custdelegation;
            var setup = JSON.stringify($scope.CurrentCustomerDeligation);
            $http.post($rootScope.APiUrl + "/api/ActiveCustDeligate?Setup=" + setup)
            .success(function (emp) {
                debugger;
                if (emp.status == true) {
                    swal("", emp.msg, "success");
                    $("#CustDeligateVerifypopup").hide();
                    LoadDelegationGrid();
                }
                else {
                    swal(emp.msg);
                }
            });
        }

        $scope.DeActiveCustDeligateClick = function (Custdelegation) {
            $scope.CurrentCustomerDeligation = Custdelegation;
            var setup = JSON.stringify($scope.CurrentCustomerDeligation);
            $http.post($rootScope.APiUrl + "/api/DeActiveCustDeligate?Setup=" + setup)
            .success(function (emp) {
                debugger;
                if (emp.status == true) {
                    swal("", emp.msg, "success");
                    $("#CustDeligateVerifypopup").hide();
                    LoadDelegationGrid();
                }
                else {
                    swal(emp.msg);
                }
            });
        }

        $scope.LocationList = [];
        $scope.loadLocationList = function (id) {
            debugger;
            $http.get($rootScope.APiUrl + "/api/GetLocationListByCustomerId?CustomerId=" + $routeParams.CustomerId)
          .success(function (data) {
              $scope.LocationList = data;

              $scope.LocationListcurrentPage = 1;
              $scope.pagination.LocationListnumPerPage = "5";
              $scope.LocationListtotallist = Math.ceil($scope.LocationList.length / parseInt($scope.pagination.LocationListnumPerPage));

              var LocationFilter = $scope.LocationList.filter(function (d) {
                  return (d.IsPrimary == true)
              })[0];
              $scope.user.LocationInfo = LocationFilter.LocationId;
          });
        }


        $scope.LocationContactList = [];
        $scope.loadLocationContactList = function (id) {
            debugger;
            $http.get($rootScope.APiUrl + "/api/GetLocationContactListByCustomerId?CustomerId=" + $routeParams.CustomerId)
          .success(function (data) {
              $scope.LocationContactList = data;
              $scope.LocationContactListcurrentPage = 1;
              $scope.pagination.LocationContactListnumPerPage = "5";
              $scope.LocationContactListtotallist = Math.ceil($scope.LocationContactList.length / parseInt($scope.pagination.LocationContactListnumPerPage));

              var LocationContactFilter = $scope.LocationContactList.filter(function (d) {
                  return (d.IsPrimary == true)
              })[0];
              $scope.user.LocationContactInfo = LocationContactFilter;
          });
        }


        $scope.editlocation = function (id) {
            debugger;
            $scope.editableForm1.$cancel();
            $http.get($rootScope.APiUrl + "/api/GetLocationById?Id=" + id)
       .success(function (datam) {
           $scope.userlocation = datam;
           $scope.IsEdit = true;
           $scope.IsCreate = false;
           $scope.editableForm1.$cancel();
           $("#CreateLocationPopup").show();
           $scope.editableForm1.$show();
       });
        };

        $scope.editLocationContact = function (id) {
            debugger;
            $scope.editableForm3.$cancel();
            $http.get($rootScope.APiUrl + "/api/GetLocationContactById?Id=" + id)
           .success(function (datam) {
               $scope.userlocationcontact = datam;
               $scope.IsEdit = true;
               $scope.IsCreate = false;
               $scope.editableForm3.$cancel();
               $("#CreateLocationContactPopup").show();
               $scope.editableForm3.$show();
           });
        };

        $scope.deletelocation = function (ids) {
            debugger;
            checkIds = ids;
            swal({
                title: "Are You Sure to Delete?", text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Delete",
                closeOnConfirm: true
            }, function () {
                $http.get($rootScope.APiUrl + "/api/DeleteLocation?id=" + checkIds)
          .success(function (data) {
              swal("", data, "success");
              $scope.loadLocationList();
          });
            });
        };

        $scope.deleteLocationContact = function (ids) {
            debugger;
            checkIds = ids;
            swal({
                title: "Are You Sure to Delete?", text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Delete",
                closeOnConfirm: true
            }, function () {
                $http.get($rootScope.APiUrl + "/api/DeleteLocationContact?id=" + checkIds)
          .success(function (data) {
              swal("", data, "success");
              $scope.loadLocationContactList();
          });
            });
        };

        function googlesearch() {
            debugger;
            var loc = {};
            loc.lat = "24.695701500000002";
            loc.lng = "46.7334075";
            var marker;
            var infoWindow;
            var options = {
                enableHighAccuracy: false,
                timeout: 5000,
                maximumAge: 0
            };

            function addInfoWindow(marker, message) {
                infoWindow = new google.maps.InfoWindow({
                    content: message
                });
                infoWindow.open(map, marker);
            }

            var geocoder = new google.maps.Geocoder();
            if (navigator.geolocation) {
                // timeout at 60000 milliseconds (60 seconds)
                var options = { timeout: 60000 };
                navigator.geolocation.getCurrentPosition(showLocation, errorHandler, options);
            }

            function showLocation(position) {
                loc.lat = position.coords.latitude;
                loc.lng = position.coords.longitude;
                myLatlng = new google.maps.LatLng(loc.lat, loc.lng);
                marker = new google.maps.Marker({ position: myLatlng, map: map, draggable: true });
                geocoder.geocode({
                    'latLng': myLatlng
                }, function (results, status) {
                    debugger;
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (results[0]) {
                            if (infoWindow != null) {
                                infoWindow.close();
                            }
                            addInfoWindow(marker, results[0].formatted_address);
                            $scope.$apply(function () {
                                $scope.userlocation.AddressFromMap = results[0].formatted_address;
                            });

                            $scope.googleSelect = true;
                            $scope.userlocation.Latitude = position.coords.latitude;
                            $scope.userlocation.Longitude = position.coords.longitude;
                        }
                    }
                });
            }

            function errorHandler(err) {
                if (err.code == 1) {
                    $rootScope.ionicalert({
                        title: 'Error',
                        template: "Access is denied",
                    });
                } else if (err.code == 2) {
                    $rootScope.ionicalert({
                        title: 'Error',
                        template: " Position is unavailable",
                    });
                }
            }

            var myLatlng = new google.maps.LatLng(loc.lat, loc.lng);
            var myOptions = {
                zoom: 17,
                center: myLatlng
            }
            var map = new google.maps.Map(document.getElementById("map"), myOptions);
            marker = new google.maps.Marker({ position: myLatlng, map: map, draggable: true });
            google.maps.event.addListener(marker, 'dragend', markerDrag);
            if (marker != null) {
                geocoder.geocode({
                    'latLng': myLatlng
                }, function (results, status) {
                    debugger;
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (results[0]) {
                            if (infoWindow != null) {
                                infoWindow.close();
                            }
                            addInfoWindow(marker, results[0].formatted_address);
                            $scope.$apply(function () {
                                $scope.userlocation.AddressFromMap = results[0].formatted_address;
                            });
                            $scope.googleSelect = true;
                            $scope.userlocation.Latitude = loc.lat;
                            $scope.userlocation.Longitude = loc.lng;
                        }
                    }
                });
            }

            google.maps.event.addListener(map, 'click', function (event) {
                placeMarker(event.latLng);
                google.maps.event.addListener(marker, 'dragend', markerDrag);
            });

            function markerDrag(event) {
                geocoder.geocode({
                    'latLng': event.latLng
                }, function (results, status) {
                    debugger;
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (results[0]) {
                            if (infoWindow != null) {
                                infoWindow.close();
                            }
                            addInfoWindow(marker, results[0].formatted_address);
                            $scope.$apply(function () {
                                $scope.userlocation.AddressFromMap = results[0].formatted_address;
                            });
                            $scope.googleSelect = true;
                            $scope.userlocation.Latitude = event.latLng.lat();
                            $scope.userlocation.Longitude = event.latLng.lng();
                        }
                    }
                });
            }

            var marker;
            function placeMarker(location) {
                if (marker) {
                    marker.setPosition(location);
                } else {
                    marker = new google.maps.Marker({
                        position: location,
                        map: map,
                        draggable: true,
                    });
                }
            }

            google.maps.event.addListener(map, 'click', function (event) {
                geocoder.geocode({
                    'latLng': event.latLng
                }, function (results, status) {
                    debugger;
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (results[0]) {
                            if (infoWindow != null) {
                                infoWindow.close();
                            }
                            addInfoWindow(marker, results[0].formatted_address);
                            $scope.$apply(function () {
                                $scope.userlocation.AddressFromMap = results[0].formatted_address;
                            });
                            $scope.googleSelect = true;
                            $scope.userlocation.Latitude = event.latLng.lat();
                            $scope.userlocation.Longitude = event.latLng.lng();
                        }
                    }
                });
            });
        }

        $scope.CreateClickLocation = function () {
            $scope.IsEdit = false;
            $scope.IsCreate = true;
            $scope.userlocation = {
                LocationType: null,
                City: null
            };
            $scope.editableForm1.$cancel();
            $("#CreateLocationPopup").show();
            $scope.editableForm1.$show();
            googlesearch();
        };
        $scope.deleteClickLocation = function () {
            $("#CreateLocationPopup").hide();
        };

        $scope.saveUserLocation = function (data, id) {
            debugger;
            $scope.editableForm1.$show();
            if ($scope.IsCreate == true) {
                debugger;
                var response = $http({
                    method: "get",
                    url: $rootScope.APiUrl + "/api/CreateLocation",
                    params: {
                        setup:
                            JSON.stringify($scope.userlocation),
                        RecId: $scope.user.RecId,
                        CustomerID: $routeParams.CustomerId,
                    }
                });
                response.then(function (emp) {
                    debugger;
                    swal("", emp.data.msg, "success");
                    $("#CreateLocationPopup").hide();
                    $scope.loadLocationList();
                });
            }
            else {
                angular.extend(data, { id: id });
                var response = $http({
                    method: "get",
                    url: $rootScope.APiUrl + "/api/UpdateLocation",
                    params: {
                        setup:
                            JSON.stringify($scope.userlocation)
                    }
                });
                response.then(function (emp) {
                    debugger;
                    swal("", emp.data.msg, "success");
                    $("#CreateLocationPopup").hide();
                    $scope.loadLocationList();
                });
            }
        };

        $scope.CreateClickContract = function () {
            $scope.IsEdit = false;
            $scope.IsCreate = true;
            $scope.userlocationcontact = {
                ContactType: null
            };
            $scope.loadContactType();
            $scope.editableForm3.$cancel();
            $("#CreateLocationContactPopup").show();
            $scope.editableForm3.$show();
        };
        $scope.deleteClickContract = function () {
            $("#CreateLocationContactPopup").hide();
        };

        $('.panel .nav-tabs').on('click', 'a', function (e) {
            var tab = $(this).parent(),
                tabIndex = tab.index(),
                tabPanel = $(this).closest('.panel'),
                tabPane = tabPanel.find('.tab-pane').eq(tabIndex);
            tabPanel.find('.active').removeClass('active');
            tab.addClass('active');
            tabPane.addClass('active');
        });

        $scope.savelocationcontact = function (data, id) {
            debugger;
            $scope.editableForm3.$show();
            if ($scope.IsCreate == true) {
                var response = $http({
                    method: "post",
                    url: $rootScope.APiUrl + "/api/CreateLocationContact",
                    params: {
                        setup:
                            JSON.stringify($scope.userlocationcontact),
                        RecId: $scope.user.RecId,
                    }
                });
                response.then(function (emp) {
                    debugger;
                    swal("", emp.data.Data, "success");
                    $("#CreateLocationContactPopup").hide();
                    $scope.loadLocationContactList();
                });
            }
            else {
                angular.extend(data, { id: id });
                var response = $http({
                    method: "post",
                    url: $rootScope.APiUrl + "/api/UpdateLocationContact",
                    params: {
                        setup:
                            JSON.stringify($scope.userlocationcontact)
                    }
                });
                response.then(function (emp) {
                    debugger;
                    swal("", emp.data.Data, "success");
                    $("#CreateLocationContactPopup").hide();
                    $scope.loadLocationContactList();
                });
            }
        };

        $scope.CityDrop = [];
        $scope.loadCity = function () {
            debugger;
            $http.get($rootScope.APiUrl + "/api/GetCity")
          .success(function (data) {
              $scope.CityDrop = data;
          });
        }
        $scope.loadCity();
        $scope.showCity = function () {
            if ($scope.user && $scope.user.City) {
                if ($scope.CityDrop && $scope.CityDrop.length) {
                    var selected = $filter('filter')($scope.CityDrop, { Id: $scope.user.City });
                    return selected.length ? selected[0].Desc : 'Not set';
                } else {
                    return $scope.user.City;
                }
            }
            else {
                return 'Not set';
            }
        };

     
        $scope.loadSocialMedia = function () {
            debugger;

            $http.get($rootScope.APiUrl + "/api/GetLookUpTypeId?Id=" + 25)
                .success(function (data) {
                    debugger;
                    $scope.SocialMediaDrop = data;
                });
        }
        //$scope.loadSocialMedia();
        $scope.showSocialMedia = function () {
            if ($scope.user && $scope.user.SocialMedia) {
                if ($scope.SocialMediaDrop && $scope.SocialMediaDrop.length) {
                    var selected = $filter('filter')($scope.SocialMediaDrop, { Id: $scope.user.SocialMedia });
                    return selected.length ? selected[0].Desc : 'Not set';
                } else {
                    return $scope.user.SocialMedia;
                }
            }
            else {
                return 'Not set';
            }
        };

        $scope.ContactTypeDrop = [];
        $scope.loadContactType = function () {
            $http.post($rootScope.APiUrl + "/api/GetEnumDrop?Name=" + "LocationType")
       .success(function (data) {
           $scope.ContactTypeDrop = data.Data;
       });
        };

        $scope.Bank = [];
        $scope.loadBank = function () {
            debugger;
            return $scope.Bank.length ? null : $http.get($rootScope.APiUrl + "/api/GetAllBank")
       .success(function (emp) {
           debugger;
           $scope.Bank = emp;
       });
        };
        $scope.loadBank();

        $scope.updateuserbank = function (data, id) {
            debugger;
            //$scope.editableForm2.$show();
            if (!$scope.userbank.BankName) {
                swal("Bank Not Found");
                return;
            }
            $scope.userbank.BankID = $scope.userbank.BankName;
            $scope.userbank.BankName = null;
            $scope.userbank.IBANNumber = $scope.userbank.IBANNumber1 + $scope.userbank.IBANNumber2 + $scope.userbank.IBANNumber3 + $scope.userbank.IBANNumber4 + $scope.userbank.IBANNumber5 + $scope.userbank.IBANNumber6;

            if ($scope.userbank.IBANNumber.length == 24) {
                var response = $http({
                    method: "GET",
                    url: $rootScope.APiUrl + "/api/CreateUserBank",
                    params: {
                        userbank: JSON.stringify($scope.userbank),
                        CustomerID: $routeParams.CustomerId,
                    }
                });
                response.then(function (emp) {
                    debugger;
                    swal("", emp.data.msg, "success");
                    $("#showbankpopup").hide();
                    $scope.getbanklist();
                });
            }
            else {
                swal("Please check your IBAN number");
                //$scope.editableForm2.$show();
                $("#showbankpopup").hide();
            }
        }

        $scope.showbankpopup = function (id) {
            $scope.userbank = {
                IBANNumber1: "SA",
                IBANNumber2: "",
                IBANNumber3: "",
                IBANNumber4: "",
                IBANNumber5: "",
                IBANNumber6: "",
                IBANNumber: "",
                BranchName: "",
            };
            $("#showbankpopup").show();
            $scope.CreateMob = {};
        };

        $scope.getbanklist = function () {
            $http.get($rootScope.APiUrl + "/api/GetCustomerBankList?CustomerId=" + $routeParams.CustomerId)
            .success(function (data) {
                $scope.banklist = data;
            });
        };

        $scope.sendverifyCode = function (id, type) {
            $scope.tmpbankrecid = id;
            $scope.tmpbankstatus = type;
            $http.get($rootScope.APiUrl + "/api/SendMobileVerifyCodeByBankAction?Id=" + id)
           .success(function (emp) {
               debugger;
               if (emp.status) {
                   $scope.BankActionVerifyCode = "";
                   if (emp.msg.length > 1) {
                       swal(emp.msg);
                   }
                   $("#bankverifypopup").show();
               }
               else {
                   swal(emp.msg)
               }
           });
        };

        $scope.CheckBankVerifyCode = function () {
            debugger;
            if (!$scope.BankActionVerifyCode) {
                swal("Verify code Not Found");
                return;
            }
            $http.get($rootScope.APiUrl + "/api/CustomerBankActiveWithVerification?Id=" + $scope.tmpbankrecid + "&AccessCode=" + $scope.BankActionVerifyCode + "&CustomerId=" + $routeParams.CustomerId + "&Status=" + $scope.tmpbankstatus)
           .success(function (emp) {
               debugger;
               if (emp.status) {
                   swal(emp.msg);
                   $("#bankverifypopup").hide();
                   GetCCustomerDetailsByCustomerId();
               }
               else {
                   swal(emp.msg)
               }
           });
        };

        $scope.closebankverifypopup = function () {
            $("#bankverifypopup").hide();
        }

        $scope.closeCustDeligateVerifypopup = function () {
            $("#CustDeligateVerifypopup").hide();
        }

        $scope.ResendBankVerifyCode = function () {
            $http.get($rootScope.APiUrl + "/api/ReSendMobileVerifyCodeByBankAction?Id=" + $scope.tmpbankrecid)
            .success(function (emp) {
                debugger;
                if (emp.status) {
                    swal(emp.msg);
                }
                else {
                    swal(emp.msg)
                }
            });
        };



        $scope.Verifimobile = function () {

            $scope.mobverificationcode = "";
            $("#MobileVerifypopup").show();
        };


        $scope.Verifiemail = function () {
            $scope.emailverificationcode = "";
            $("#EmailVerifypopup").show();
        };

        $scope.verifyemail = function () {
            var AccessCode = $scope.emailverificationcode;
            $http.get($rootScope.APiUrl + "/api/VerifyCustomerbyEmail?CustomerId=" + $routeParams.CustomerId + "&Email=" + $scope.user.Email + "&AccessCode=" + AccessCode)
            .success(function (data) {
                debugger;
                if (data.status == true) {
                    swal("Your Email id is verified");
                    $("#EmailVerifypopup").hide();
                    GetCCustomerDetailsByCustomerId();
                }
                else {
                    swal(data.msg);
                }

            });
        }
        $scope.closeemailverifypopup = function () {
            $("#EmailVerifypopup").hide();
        }
        $scope.closemobileverifypopup = function () {
            $("#MobileVerifypopup").hide();
        }

        $scope.resendverifycodebyemail = function () {
            $http.get($rootScope.APiUrl + "/api/SendVerificationCodebyEmail?CustomerId=" + $routeParams.CustomerId + "&Email=" + $scope.user.Email)
            .success(function (data) {
                debugger;
                if (data.status == true) {
                    swal(data.msg);
                }
                else {
                    swal(data.msg);
                }
            });
        }

        $scope.verifymobileno = function () {
            var AccessCode = $scope.mobverificationcode;
            $http.get($rootScope.APiUrl + "/api/VerifyCustomerMobileNumber?CustomerId=" + $routeParams.CustomerId + "&MobileNumber=" + $scope.user.MobileNumber + "&AccessCode=" + AccessCode)
            .success(function (data) {
                debugger;
                if (data.status == true) {
                    swal("Your mobile number is verified");
                    $("#MobileVerifypopup").hide();
                    GetCCustomerDetailsByCustomerId();
                }
                else {
                    swal(data.msg);
                }
            });
        }

        $scope.resendverifycode = function () {
            $http.get($rootScope.APiUrl + "/api/SendVerificationCodebyMobileNumber?CustomerId=" + $routeParams.CustomerId + "&MobileNumber=" + $scope.user.MobileNumber)
            .success(function (data) {
                debugger;
                if (data.status == true) {
                    swal(data.msg);
                }
                else {
                    swal(data.msg);
                }
            });
        }



        $scope.CountryRegionDrop = [];
        $scope.loadRegion = function () {
            debugger;
            return $scope.CountryRegionDrop.length ? null : $http.get($rootScope.APiUrl + "/api/GetCountryRegionDrop")
                .success(function (emp) {
                    debugger;
                    $scope.CountryRegionDrop = emp;
                });
        };
        $scope.loadRegion();
        $scope.showRegion = function () {
            if ($scope.user && $scope.user.CountryRegion) {
                if ($scope.CountryRegionDrop && $scope.CountryRegionDrop.length) {
                    var selected = $filter('filter')($scope.CountryRegionDrop, { Id: $scope.user.CountryRegion });
                    return selected.length ? selected[0].Desc : 'Not set';
                } else {
                    return $scope.user.CountryRegion;
                }
            }
            else {
                return 'Not set';
            }
        };



        $scope.saveBusinessCustomer = function (data, id) {
            debugger;
            var response = $http({
                method: "post",
                url: $rootScope.APiUrl + "/api/Business/UpdateBusinessCustomer",
                params: {
                    customerObj: JSON.stringify(data)
                }
            });
            response.then(function (res) {
                debugger;
                if (res.data.status) {
                    swal("", res.data.Message, "success");
                    GetCCustomerDetailsByCustomerId();
                }
                else {
                    swal("", res.data.Message, "error");
                    $scope.editableForm12.$show();

                }

            });

        }


        GetBCustomerContractList();
        $scope.GetBCustomerContractList = function () {
            GetBCustomerContractList();
        };
        function GetBCustomerContractList() {
            debugger;

            $http.get($rootScope.APiUrl + "/api/Business/GetContractListbyCustomer?CustomerId=" + $rootScope.SerachCustomerID)
                .success(function (data) {
                    debugger;
                    $scope.BCustomerContractList = data;
                    $scope.BContractListcurrentPage = 1;
                    $scope.pagination.BContractListnumPerPage = "5";
                    $scope.BContractListtotallist = Math.ceil($scope.BCustomerContractList.length / parseInt($scope.pagination.BContractListnumPerPage));
                });
        }



        $scope.LoadMessageSendListGrid = function () {
            LoadMessageSendListGrid();
        };

        function LoadMessageSendListGrid() {
            debugger;
            $http.get($rootScope.APiUrl + "IndividualSector/GetNoticationSentListByMobileNumber?Mobile=" + $scope.user.MobileNumber)
      .success(function (data) {
          data.forEach(function addDates(row, index) {
              if (row.CreatedDatetime != null) {
                  var dateString = row.CreatedDatetime.substr(6);
                  var currentTime = new Date(parseInt(dateString));
                  var month = currentTime.getMonth() + 1;
                  var day = currentTime.getDate();
                  var year = currentTime.getFullYear();
                  row.CreatedDatetime = day + "/" + month + "/" + year;
              }
              if (row.SendDate != null) {

                  var dateString1 = row.SendDate.substr(6);
                  var currentTime1 = new Date(parseInt(dateString1));
                  var month1 = currentTime1.getMonth() + 1;
                  var day1 = currentTime1.getDate();
                  var year1 = currentTime1.getFullYear();
                  var hr = currentTime1.getHours();
                  var min = currentTime1.getMinutes();
                  row.SendDate = day1 + "/" + month1 + "/" + year1 + " " + hr + ":" + min;

                  // row.SendDate = $filter('date')(new Date(parseInt(row.SendDate.substr(6))), "dd/MM/yyyy hh:MM");
              }
          });

          $scope.CustomerMessageSendList = data;
          $scope.CustomerMessageSendcurrentPage = 1;
          $scope.pagination.CustomerMessageSendnumPerPage = "5";
          $scope.CustomerMessageSendtotallist = Math.ceil($scope.CustomerMessageSendList.length / parseInt($scope.pagination.CustomerMessageSendnumPerPage));
      });
        };




        //************* Pagination CustomerDeligation *****************

        $scope.paginationPageSizes = [5, 10, 15, 25, 50],
        $scope.CustomerDelegationpaginate = function (value) {
            var begin, end, index;
            begin = ($scope.CustomerDelegationcurrentPage - 1) * parseInt($scope.pagination.CustomerDelegationnumPerPage);
            end = begin + parseInt($scope.pagination.CustomerDelegationnumPerPage);
            index = $scope.CustomerDelegationList.indexOf(value);
            return (begin <= index && index < end);
        };
        $scope.selectCustomerDelegationpagination = function (value) {
            debugger;
            $scope.CustomerDelegationcurrentPage = value;
        };
        $scope.changeCustomerDelegationpagesize = function (value) {
            debugger;
            $scope.pagination.CustomerDelegationnumPerPage = value;
            $scope.CustomerDelegationcurrentPage = 1;
            $scope.CustomerDelegationtotallist = Math.ceil($scope.CustomerDelegationList.length / parseInt($scope.pagination.CustomerDelegationnumPerPage));

        };
        //************** Pagination ***********


        //************* Pagination ContractList *****************

        $scope.paginationPageSizes = [5, 10, 15, 25, 50],
        $scope.ContractListpaginate = function (value) {
            var begin, end, index;
            begin = ($scope.ContractListcurrentPage - 1) * parseInt($scope.pagination.ContractListnumPerPage);
            end = begin + parseInt($scope.pagination.ContractListnumPerPage);
            index = $scope.CustomerContractList.indexOf(value);
            return (begin <= index && index < end);
        };
        $scope.selectContractListpagination = function (value) {
            debugger;
            $scope.ContractListcurrentPage = value;
        };
        $scope.changeContractListpagesize = function (value) {
            debugger;
            $scope.pagination.ContractListnumPerPage = value;
            $scope.ContractListcurrentPage = 1;
            $scope.ContractListtotallist = Math.ceil($scope.CustomerContractList.length / parseInt($scope.pagination.ContractListnumPerPage));

        };
        //************** Pagination ***********


        //************* Pagination BContractList *****************

        $scope.paginationPageSizes = [5, 10, 15, 25, 50],
            $scope.BContractListpaginate = function (value) {
                var begin, end, index;
                begin = ($scope.BContractListcurrentPage - 1) * parseInt($scope.pagination.BContractListnumPerPage);
                end = begin + parseInt($scope.pagination.BContractListnumPerPage);
                index = $scope.BCustomerContractList.indexOf(value);
                return (begin <= index && index < end);
            };
        $scope.selectBContractListpagination = function (value) {
            debugger;
            $scope.ContractListcurrentPage = value;
        };
        $scope.changeBContractListpagesize = function (value) {
            debugger;
            $scope.pagination.BContractListnumPerPage = value;
            $scope.BContractListcurrentPage = 1;
            $scope.BContractListtotallist = Math.ceil($scope.BCustomerContractList.length / parseInt($scope.pagination.BContractListnumPerPage));

        };
        //************** Pagination ***********



        //************* Pagination Location *****************

        $scope.paginationPageSizes = [5, 10, 15, 25, 50],
        $scope.LocationListpaginate = function (value) {
            var begin, end, index;
            begin = ($scope.LocationListcurrentPage - 1) * parseInt($scope.pagination.LocationListnumPerPage);
            end = begin + parseInt($scope.pagination.LocationListnumPerPage);
            index = $scope.LocationList.indexOf(value);
            return (begin <= index && index < end);
        };
        $scope.selectLocationListpagination = function (value) {
            debugger;
            $scope.LocationListcurrentPage = value;
        };
        $scope.changeLocationListpagesize = function (value) {
            debugger;
            $scope.pagination.LocationListnumPerPage = value;
            $scope.LocationListcurrentPage = 1;
            $scope.LocationListtotallist = Math.ceil($scope.LocationList.length / parseInt($scope.pagination.LocationListnumPerPage));

        };
        //************** Pagination ***********



        //************* Pagination LocationContact*****************

        $scope.paginationPageSizes = [5, 10, 15, 25, 50],
        $scope.LocationContactListpaginate = function (value) {
            var begin, end, index;
            begin = ($scope.LocationContactListcurrentPage - 1) * parseInt($scope.pagination.LocationContactListnumPerPage);
            end = begin + parseInt($scope.pagination.LocationContactListnumPerPage);
            index = $scope.LocationContactList.indexOf(value);
            return (begin <= index && index < end);
        };
        $scope.selectLocationContactListpagination = function (value) {
            debugger;
            $scope.LocationContactListcurrentPage = value;
        };
        $scope.changeLocationContactListpagesize = function (value) {
            debugger;
            $scope.pagination.LocationContactListnumPerPage = value;
            $scope.LocationContactListcurrentPage = 1;
            $scope.LocationContactListtotallist = Math.ceil($scope.LocationContactList.length / parseInt($scope.pagination.LocationContactListnumPerPage));

        };
        //************** Pagination ***********



        //************* Pagination MessageSend *****************

        $scope.paginationPageSizes = [5, 10, 15, 25, 50],
        $scope.CustomerMessageSendpaginate = function (value) {
            var begin, end, index;
            begin = ($scope.CustomerMessageSendcurrentPage - 1) * parseInt($scope.pagination.CustomerMessageSendnumPerPage);
            end = begin + parseInt($scope.pagination.CustomerMessageSendnumPerPage);
            index = $scope.CustomerMessageSendList.indexOf(value);
            return (begin <= index && index < end);
        };
        $scope.selectCustomerMessageSendpagination = function (value) {
            debugger;
            $scope.CustomerMessageSendcurrentPage = value;
        };
        $scope.changeCustomerMessageSendpagesize = function (value) {
            debugger;
            $scope.pagination.CustomerMessageSendnumPerPage = value;
            $scope.CustomerMessageSendcurrentPage = 1;
            $scope.CustomerMessageSendtotallist = Math.ceil($scope.CustomerMessageSendList.length / parseInt($scope.pagination.CustomerMessageSendnumPerPage));

        };
        //************** Pagination ***********


    }]);