﻿app.controller('PaymentCtrl', ['$scope', '$rootScope', '$http','$routeParams',
    function ($scope, $rootScope, $http, $routeParams) {

        $rootScope.SerachCustomerID = $routeParams.CustomerId;
        $scope.PaymentNumber = $routeParams.PaymentNumber;

        getCustomerPaymentList();
        $scope.getCustomerPaymentList = function () {
            getCustomerPaymentList();
        };

        function getCustomerPaymentList() {
            debugger;
            $http.get($rootScope.APiUrl + "/api/GetCustomerPaymentList?CustomerId=" + $rootScope.SerachCustomerID)
                .success(function (datam) {
                    debugger;
                    $scope.CustomerPaymentList = datam;

                    if ($scope.PaymentNumber) {
                        var list = $scope.CustomerPaymentList.filter(x => x.PaymentNumber == $scope.PaymentNumber)[0];
                        $scope.GetPaymentProcess(list);
                    }

                });
        }



        $scope.GetPaymentProcess = function (list) {
            debugger;
            $scope.PaymentNumber = list.PaymentNumber;
            var paymentRecId = list.PaymentNumber;
            $scope.showpaymentrecid = list.PaymentNumber;
            $(".closepayment").hide();
            $(".showpayment").show();
            $("#showpayment_" + list.PaymentNumber).hide();
            $("#closepayment_" + list.PaymentNumber).show();
            if ($scope.temppaymentRecId != paymentRecId) {
                $http.get($rootScope.APiUrl + "/api/GetSettledPaymentList?PaymentNumber=" + paymentRecId)
                    .success(function (datam) {
                        debugger;
                        $scope.SettledPaymentList = datam;
                        $scope.temppaymentRecId = paymentRecId;
                        $scope.showpaymentTransaction = true;


                    });
            }
            else {
                $scope.showpaymentTransaction = true;
            }
        };

        $scope.CloseGetPaymentProcess = function (list) {

            $(".closepayment").hide();
            $(".showpayment").show();
            $scope.showpaymentTransaction = false;
        };

      

    }]);