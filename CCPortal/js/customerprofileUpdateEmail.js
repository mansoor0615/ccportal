﻿/// <reference path="../templates/CustomerProfileMobileTemplate.html" />
/// <reference path="../templates/CustomerProfileMobileTemplate.html" />
/// <reference path="../templates/CustomerProfileMobileTemplate.html" />



app.directive('customeremailupdatehistory', function () {
    var index = -1;

    return {
        restrict: 'E',
        scope: {
            EmUD_customerid: '=customerid',
            EmUD_externalfn: '=externalfn',
            EmUD_title: '=title',



        },
        controller: ['$scope', '$rootScope', '$http', '$routeParams', '$filter',
 function ($scope, $rootScope, $http, $routeParams, $filter) {







     if (!$scope.EmUD_title) {
         $scope.EmUD_title = "Contact Info"
     }


     if (!$scope.EmUD_mode || $scope.EmUD_mode == "Edit") {
         $scope.$watch("EmUD_customerid", function (val) {
             if (val) {
                 $scope.EmUD_loadEmailHistory();
             }
         });
     }
     else {


     }








     $scope.EmUD_CurrentSelectedHistory = null;
     $scope.EmUD_loadEmailHistory = function () {
         debugger;
         $http.get($rootScope.APiUrl + "/api/GetCustomerEmailHistory?CustomerId=" + $scope.EmUD_customerid)
    .success(function (emp) {
        debugger;
        $scope.EmUD_CustomerContactHistory = emp.CustomerContactHistory;
        $scope.EmUD_CustomerContactDetails = emp.CustomerContactDetails;

    });
     };



     $scope.EmUD_showemailhistorypopup = function (id) {
         $scope.EmUD_CreateEmail = {};
         if ($scope.EmUD_CustomerContactHistory.length == 0) {
             if ($scope.EmUD_CustomerContactDetails && $scope.EmUD_CustomerContactDetails.Email) {
                 $scope.EmUD_CreateEmail.Email = $scope.EmUD_CustomerContactDetails.Email;
             }

         }
         $("#tempshowemailupdatehistorypopup").show();
     };

     $scope.EmUD_closeemailhistorypopup = function (id) {
        $("#tempshowemailupdatehistorypopup").hide();
    };


     function isValidEmail(email) {
         var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
         return regex.test(email);
     }

     $scope.EmUD_AddOrUpdateEmail = function (id) {
         debugger;
         if ($scope.EmUD_CreateEmail != null && $scope.EmUD_CreateEmail.Email != null && $scope.EmUD_CreateEmail.Email != "") {
             if (!isValidEmail($scope.EmUD_CreateEmail.Email)) {
                 $scope.EmUD_CreateEmail = {};
                 $rootScope.SwalMsgWithType("alert!", "Invaild Email Id", "error", 3000);
                 return;
             }
             $http.get($rootScope.APiUrl + "/api/AddCustomerEmailHistory?CustomerId=" + $scope.EmUD_customerid + "&Email=" + $scope.EmUD_CreateEmail.Email)
                 .success(function (emp) {
                     debugger;
                     if (emp.status) {
                         $scope.EmUD_loadEmailHistory();
                         $scope.EmUD_CurrentSelectedHistory = emp.record;
                         $scope.EmUD_sendverifyCode();

                     }
                     else {
                         swal(emp.msg)
                     }
                 });
         }
         else {
             $rootScope.SwalMsgWithType("alert!", "Please Enter Email Id", "error", 3000);
             return;
         }
     };


     $scope.EmUD_sendverifyCode = function (id) {
         debugger;
         $scope.EmUD_VerifyEmail = {};
         if (!$scope.EmUD_CurrentSelectedHistory) {
             swal("Contact Details Not Found");
             return;
         }
         $http.get($rootScope.APiUrl + "/api/SendEmailVerifyCodeByHistory?Id=" + $scope.EmUD_CurrentSelectedHistory.RecId)
             .success(function (emp) {
                 debugger;
                 if (emp.status) {
                     $("#emailupdateverifypopup").show();
                 }
                 else {
                     swal(emp.msg)
                 }
             });
     };


     $scope.EmUD_showemailverifypopup = function (obj) {
         debugger;
         $scope.EmUD_VerifyEmail = {};
         $scope.EmUD_CurrentSelectedHistory = obj;
         if (!$scope.EmUD_CurrentSelectedHistory) {
             swal("Contact Details Not Found");
             return;
         }
         $("#emailupdateverifypopup").show();
     };


     $scope.EmUD_CheckVerifyCode = function (id) {
         debugger;
         if (!$scope.EmUD_VerifyEmail) {
             swal("Verify code Not Found");
             return;
         }
         if (!$scope.EmUD_CurrentSelectedHistory) {
             swal("Contact Details Not Found");
             return;
         }

         $http.get($rootScope.APiUrl + "/api/CheckVerifyCodeByEmailContactHistory?Id=" + $scope.EmUD_CurrentSelectedHistory.RecId + "&AccessCode=" + $scope.EmUD_VerifyEmail.VerifyCode)
 .success(function (emp) {
     debugger;
     if (emp.status) {
         swal("Email Verified SucessFully");
         $("#emailupdateverifypopup").hide();
         $("#tempshowemailupdatehistorypopup").hide();



         $scope.EmUD_loadEmailHistory();
     }
     else {
         swal(emp.msg)
     }


 });
     };



     $scope.EmUD_resendverifycode = function (obj) {
         debugger;
         $scope.EmUD_CurrentSelectedHistory = obj;
         if (!$scope.EmUD_CurrentSelectedHistory) {
             swal("Contact Details Not Found");
             return;
         }
         $scope.EmUD_sendverifyCode();
     };




 }],
        link: function (scope, element, attrs) {
            scope.currentclass = attrs.class;




        },

        templateUrl: '/templates/CustomerProfileEmailTemplate.html'

    };
});






