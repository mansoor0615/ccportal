﻿app.controller('NotFoundCtrl', ['$scope', '$rootScope', '$http','$location',
    function ($scope, $rootScope, $http, $location) {

        var UserName = window.localStorage.getItem("UserName");
        if (UserName) {
            $rootScope.UserName = UserName;

            var CCPUser = window.localStorage.getItem("CCPUser");
            $rootScope.CCPUser = JSON.parse(CCPUser);
        }
        else {
            window.localStorage.clear();
            $rootScope.UserName = null;
            $location.path("/signin");
        }

     
        $scope.showCustomerList = false;
        $scope.clickbody = function ($event) {
            if (!$($event.target).is('#txtCustomerFilter')) {
                $scope.showCustomerList = false;
            }
        };
        $scope.getSearchCustomer = function (event) {
            debugger;
            var CustomerFilter = $scope.CustomerFilter;
            $http.get($rootScope.APiUrl + "/api/GetCallCenterbyCustomerAcutoComplete?term=" + CustomerFilter)
                .success(function (datam) {
                    $scope.customerlist = datam;
                    $scope.showCustomerList = true;

                });
        };

        $scope.selectCustomer = function (list) {
            $rootScope.SerachCustomerID = list.CustomerID;
            var url = "#/dashboard/" + list.CustomerID;
            window.open(url);
        };

     


        
    }]);