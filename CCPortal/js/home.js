﻿app.controller('HomeCtrl', ['$scope', '$rootScope', '$http','$location',
    function ($scope, $rootScope, $http, $location) {

        debugger;

        var UserName = window.localStorage.getItem("UserName");
        if (UserName) {
            $rootScope.UserName = UserName;

            var CCPUser = window.localStorage.getItem("CCPUser");
            $rootScope.CCPUser = JSON.parse(CCPUser);
        }
        else {
            window.localStorage.clear();
            $rootScope.UserName = null;
            $location.path("/signin");
        }

        GetCustomerTicketDetails();

        $scope.showCustomerList = false;
        $scope.clickbody = function ($event) {
            if (!$($event.target).is('#txtCustomerFilter')) {
                $scope.showCustomerList = false;
            }
        };
        $scope.getSearchCustomer = function (event) {
            debugger;
            var CustomerFilter = $scope.CustomerFilter;
            $http.get($rootScope.APiUrl + "/api/GetCustomerAcutoComplete?term=" + CustomerFilter)
                .success(function (datam) {
                    $scope.customerlist = datam;
                    $scope.showCustomerList = true;

                });
        };

        $scope.selectCustomer = function (list) {
            $rootScope.SerachCustomerID = list.CustomerID;
            var url = "#/dashboard/" + list.CustomerID;
            window.open(url);
        };

     
        function GetCustomerTicketDetails() {

                $http.get($rootScope.APiUrl + "/api/GetEnquiryStatusCount")
                    .success(function (emp) {
                        debugger;
                        $scope.TicketDatails = emp;

                        $scope.AllTicket = $scope.TicketDatails.filter(x => x.StatusName == "All")[0];
                        $scope.OpenTicket = $scope.TicketDatails.filter(x => x.StatusName == "Open")[0];
                        $scope.CloseTicket = $scope.TicketDatails.filter(x => x.StatusName == "Closed")[0];
                        $scope.OnHoldTicket = $scope.TicketDatails.filter(x => x.StatusName == "On Hold")[0];
                        $scope.ResolvedTicket = $scope.TicketDatails.filter(x => x.StatusName == "Resolved")[0];
                        $scope.ReopenTicket = $scope.TicketDatails.filter(x => x.StatusName == "Reopen")[0];
                        $scope.CanceledTicket = $scope.TicketDatails.filter(x => x.StatusName == "Canceled")[0];

                        var total = $scope.AllTicket.Count;

                        if ($scope.OpenTicket.Count > 0)
                            $scope.OpenTicketPer = (($scope.OpenTicket.Count / total) * 100).toFixed(2);

                        if ($scope.CloseTicket.Count > 0)
                            $scope.CloseTicketPer = (($scope.CloseTicket.Count / total) * 100).toFixed(2);

                        if ($scope.OnHoldTicket.Count > 0)
                            $scope.OnHoldTicketPer = (($scope.OnHoldTicket.Count / total) * 100).toFixed(2);

                        if ($scope.ResolvedTicket.Count > 0)
                            $scope.ResolvedTicketPer = (($scope.ResolvedTicket.Count / total) * 100).toFixed(2);

                        if ($scope.ReopenTicket.Count > 0)
                            $scope.ReopenTicketPer = (($scope.ReopenTicket.Count / total) * 100).toFixed(2);

                        if ($scope.CanceledTicket.Count > 0)
                            $scope.CanceledTicketPer = (($scope.CanceledTicket.Count / total) * 100).toFixed(2);

                    });
            

        }

        
    }]);