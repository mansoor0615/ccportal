﻿app.controller('CustomerServiceEnquiryListCtrl', ['$scope', '$rootScope', '$http', '$location', '$routeParams',
    function ($scope, $rootScope, $http, $location, $routeParams) {

        debugger;
        
        var UserName = window.localStorage.getItem("UserName");
        if (UserName) {
            $rootScope.UserName = UserName;
        }
        else {
            window.localStorage.clear();
            $rootScope.UserName = null;
            $location.path("/signin");
        }

        $rootScope.SerachCustomerID = $routeParams.CustomerId;

        $scope.pagination = {
            CustomerServicenumPerPage: "5"
        }


        function GetCCustomerDetailsByCustomerId() {
            var CustomerId = $rootScope.SerachCustomerID;
            if (CustomerId.length > 0) {

                $http.get($rootScope.APiUrl + "/api/GetDetailsByIndiviualCustomerPortal?CustomerID=" + CustomerId + "&PortalType=1")
                    .success(function (emp) {
                        debugger;
                        $scope.user = emp;
                        date = new Date($scope.user.DOBGeorgian);
                        $scope.userAge = _calculateAge(date);

                    });

            }
        }
        function _calculateAge(birthday) { // birthday is a date
            var ageDifMs = Date.now() - birthday.getTime();
            var ageDate = new Date(ageDifMs); // miliseconds from epoch
            return Math.abs(ageDate.getUTCFullYear() - 1970);
        }

        GetCCustomerDetailsByCustomerId();
        GetCustomerServiceEnquiry();
        $scope.RefreshGetCustomerServiceEnquiry = GetCustomerServiceEnquiry();

        function GetCustomerServiceEnquiry() {
            debugger;
            $http.get($rootScope.APiUrl + "/api/GetCustomerServiceEnquiryByCustomerId?CustomerId=" + $rootScope.SerachCustomerID)
                .success(function (data) {
                    $scope.CustomerServiceInquiryList = data;
                    $scope.CustomerServicecurrentPage = 1;
                    $scope.pagination.CustomerServicenumPerPage = "5";
                    $scope.CustomerServicetotallist = Math.ceil($scope.CustomerServiceInquiryList.length / parseInt($scope.pagination.CustomerServicenumPerPage));
                });
        }


        $scope.CreatecustEnq = function () {
            debugger;
            $scope.IsEdit = false;
            $scope.IsCreate = true;

            if (!$scope.CompanyBranchList || !$scope.CompanyBranchList.length < 0)
                GetCompanyBranches();

            $scope.customerservice = {
                CustomerId: $rootScope.SerachCustomerID,
                Description: null,
                Comments: null,
                Nationality: null,
                Branch: null,
                CategoryType: null,
                Category: null,
                SubCategory: null,
                UserId: $rootScope.UserName
            };
            $scope.editableForm1.$cancel();
            $("#createcustomerenqpopup").show();
            $scope.editableForm1.$show();
        };

        $scope.hidecustomerenq = function () {
            $("#createcustomerenqpopup").hide();
        };
        //GetCompanyBranches();
        //GetNationality();

        //function GetNationality() {
        //    debugger;

        //    $http.get($rootScope.APiUrl + "/api/Nationality")
        //        .success(function (data) {
        //            debugger;
        //            $scope.Nationality = data;
        //        });
        //}

        GetLookType();
        function GetLookType() {
            debugger;

            $http.get($rootScope.APiUrl + "/api/GetLookUpTypeId?Id=" + 25)
                .success(function (data) {
                    debugger;
                    $scope.CategoryTypeDrop = data;
                });
        }

        $scope.ChangeCategorytype = function (val) {
            debugger;
            $scope.SubCategory = [];

                   if (val) {
                $http.get($rootScope.APiUrl + "/api/GetLookupTypeByValue?LookUpTypeId=" + 26 + "&LookUpValueId=" + val)
                    .success(function (data) {
                        debugger;
                        $scope.Category = data;
                    });
            }
        }

        $scope.ChangeCategory = function (val) {
            debugger;
            if (val) {
                var temp = "";
                $scope.Category.filter(function (d) {
                    if ((d.Id == val && d.Description == "Nationality") || (d.Id == val && d.Description == "Price")
                        || (d.Id == val && d.Description == "Locations") || (d.Id == val && d.Description == "Iqama transfer")
                        || (d.Id == val && d.Description == "Escape report")) {
                        temp = (d.Description);
                        return true;
                    }
                });

                if (temp == "Nationality") {
                    $http.get($rootScope.APiUrl + "/api/Nationality")
                        .success(function (data) {
                            debugger;
                            $scope.SubCategory = [];
                            for (const [key, value] of data.entries(data)) {
                                $scope.SubCategory.push({ Id: value.Id, Description: value.Desc });
                            }
                        });
                }
                else if (temp == "Price") {
                    $http.get($rootScope.APiUrl + "/api/GetPackageDropList")
                        .success(function (data) {
                            debugger;
                            $scope.SubCategory = data;
                        });
                }

                else if (temp == "Iqama transfer" || temp == "Escape report") {
                    $http.get($rootScope.APiUrl + "/api/GetAvailableNationality")
                        .success(function (data) {
                            debugger;
                            $scope.SubCategory = data;
                        });
                }
                else if (temp == "Locations") {
                    $http.get($rootScope.APiUrl + "/api/GetAllCompanyBranchesForDrop")
                        .success(function (data) {
                            debugger;
                            $scope.SubCategory = [];
                            $scope.SubCategory.push({Id: "0", Description: "Inquiry" },{ Id: "-1", Description: "Availability" });
                            for (const [key, value] of data.entries(data)) {
                                $scope.SubCategory.push({ Id: value.Id, Description: value.Desc });
                            }
                        });
                }
                else {
                    $http.get($rootScope.APiUrl + "/api/GetLookupTypeByValue?LookUpTypeId=" + 27 + "&LookUpValueId=" + val)
                        .success(function (data) {
                            debugger;
                            $scope.SubCategory = data;
                        });
                }

            }
        
        }
      
        function GetCompanyBranches() {
            debugger;

            $http.get($rootScope.APiUrl + "/api/GetAllCompanyBranchesForMob")
                .success(function (data) {
                    debugger;
                    $scope.CompanyBranchList = data;
                });
        }

        $scope.saveCustEnquiryList = function (data, id) {
            debugger;
            if ($scope.customerservice && ($scope.customerservice.CategoryType && $scope.customerservice.Category && $scope.customerservice.SubCategory)) {
                $scope.editableForm1.$show();
                if ($scope.IsCreate == true) {

                    var response = $http({
                        method: "post",
                        url: $rootScope.APiUrl + "/api/CreateCustomerServiceEnquiry",
                        params: {
                            CustomerServiceEnquiry: JSON.stringify($scope.customerservice),
                        }
                    });
                    response.then(function (emp) {
                        debugger;
                        if (emp.data.status == true) {
                            swal("", emp.data.msg, "success");
                            $("#createcustomerenqpopup").hide();
                            GetCustomerServiceEnquiry();
                        }
                        else {
                            swal(emp.data.msg);
                        }
                    });

                }
                else {
                    angular.extend(data, { id: id });
                    var response = $http({
                        method: "post",
                        url: $rootScope.APiUrl + "/api/UpdateCustomerServiceEnquiry",
                        params: {
                            CustomerServiceEnquiry: JSON.stringify($scope.customerservice)
                        }
                    });
                    response.then(function (emp) {
                        debugger;
                        if (emp.data.status == true) {
                            swal("", emp.data.msg, "success");
                            $("#createcustomerenqpopup").hide();
                            GetCustomerServiceEnquiry();
                        }
                        else {
                            swal(emp.data.msg);
                        }
                    });
                }
            }
            else {
                return swal("please fill mandatory fields");
            }
        };


        $scope.editCustomerServiceEnquiry = function (recid) {
            debugger;
            if (recid) {
                $scope.editableForm1.$cancel();
                $http.get($rootScope.APiUrl + "/api/GetCustomerServiceEnquiryByRecId?RecId=" + recid)
                    .success(function (data) {
                        $scope.customerservice = data;
                        $scope.IsEdit = true;
                        $scope.IsCreate = false;

                        if (!$scope.CompanyBranchList || !$scope.CompanyBranchList.length < 0)
                            GetCompanyBranches();

                        $scope.editableForm1.$cancel();
                        $("#createcustomerenqpopup").show();
                        $scope.editableForm1.$show();
                    });
            }
        };

        $scope.deleteCustomerServiceEnquiry = function (ids) {
            debugger;
            checkIds = ids;
            swal({
                title: "Are You Sure to Delete?", text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Delete",
                closeOnConfirm: true
            }, function () {
                $http.get($rootScope.APiUrl + "/api/DeleteCustomerServiceEnquiry?Ids=" + checkIds)
          .success(function (data) {
              swal("", data, "success");
              GetCustomerServiceEnquiry();
          });
            });
        };


        //************* Pagination *****************

        $scope.paginationPageSizes = [5, 10, 15, 25, 50],
        $scope.CustomerServicepaginate = function (value) {
            var begin, end, index;
            begin = ($scope.CustomerServicecurrentPage - 1) * parseInt($scope.pagination.CustomerServicenumPerPage);
            end = begin + parseInt($scope.pagination.CustomerServicenumPerPage);
            index = $scope.CustomerServiceInquiryList.indexOf(value);
            return (begin <= index && index < end);
        };
        $scope.selectCustomerServicepagination = function (value) {
            debugger;
            $scope.CustomerServicecurrentPage = value;
        };
        $scope.changeCustomerServicepagesize = function (value) {
            debugger;
            $scope.pagination.CustomerServicenumPerPage = value;
            $scope.CustomerServicecurrentPage = 1;
            $scope.CustomerServicetotallist = Math.ceil($scope.CustomerServiceInquiryList.length / parseInt($scope.pagination.CustomerServicenumPerPage));

        };
        //************** Pagination ***********
    }]);