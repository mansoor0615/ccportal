﻿app.controller('TaskCtrl', ['$scope', '$rootScope', '$http', '$location', '$routeParams',
    function ($scope, $rootScope, $http, $location, $routeParams) {

        debugger;
        $scope.pagination = {
            TicketnumPerPage: "5"
        }
        var UserName = window.localStorage.getItem("UserName");
        if (UserName) {
            $rootScope.UserName = UserName;
        }
        else {
            window.localStorage.clear();
            $rootScope.UserName = null;
            $location.path("/signin");
        }

        $rootScope.SerachCustomerID = $routeParams.CustomerId;
        $scope.filterstatus = parseInt($routeParams.status);

    
        GetCustomerTaskList();
      

        $scope.GetCustomerTaskList = function () {
            GetCustomerTaskList();
        };

        function GetCustomerTaskList() {
            debugger;
            var rootapi = $rootScope.APiUrl + "/api/GetTaskForCustomer?CustomerId=" + $rootScope.SerachCustomerID;
          
            $http.get(rootapi)
               .success(function (data) {
                   debugger;
                   $scope.CustomerTaskList = data;
                   $scope.TicketcurrentPage = 1;
                   $scope.pagination.TicketnumPerPage = "5";
                   $scope.Tickettotallist = Math.ceil($scope.CustomerTaskList.length / parseInt($scope.pagination.TicketnumPerPage));
               });

        }

        $scope.getTaskDetails = function (item) {
            $scope.TaskId = item.RecId;

        };

        $scope.CreateTask = function () {
            debugger;

            if (!$scope.CustomerContract || $scope.CustomerContract.length < 1)
                $scope.GetCustomerContract();

            $scope.ddlAssignedGroup = null;
            $scope.ddlAssignedTo = null;
            $scope.TaskContract = null;
            $("#file-6").val("");
            $("#txtSubject").val("");
            $("#textArea").val("");
            $scope.TargetDate = null;

            $("#createpopup").removeClass("hide");
            $("#createpopup").show();
        }
        $scope.hideTicketpop = function () {
            $("#createpopup").hide();
        }

        $scope.Ticketupdate = function (enquiry) {
            $scope.Status = enquiry.StatusName;
            $scope.Subject = enquiry.Subject;
            $scope.Email = enquiry.Email;
            $scope.CustomerName = enquiry.CustomerName;
            $scope.Contact = enquiry.ContactNo;
            $scope.Desp = enquiry.Description;
            $scope.EnqId = enquiry.EnquiryId;
            $("#StatusSelect").val(enquiry.StatusId);
            $scope.StatusSelect = enquiry.StatusId;
            $("#Ticketupdate").show();

        }
        $scope.closeTicketupdate = function () {
            $("#Ticketupdate").hide();
        }

        $('.panel .nav-tabs').on('click', 'a', function (e) {
            var tab = $(this).parent(),
                tabIndex = tab.index(),
                tabPanel = $(this).closest('.panel'),
                tabPane = tabPanel.find('.tab-pane').eq(tabIndex);
            tabPanel.find('.active').removeClass('active');
            tab.addClass('active');
            tabPane.addClass('active');
        });

        $scope.ddlGroupDrop = [];
        $scope.loadGroupDrop = function (e) {
            $http.get($rootScope.APiUrl + "/api/GroupDrop")
                .success(function (data) {
                    $scope.ddlGroupDrop = data;
                });
        };

        $scope.ClearFilter = function () {
            debugger;
            $("#createpopup").addClass("hide");
            $scope.TaskContract = null;
         
        }

        $scope.fileNameChanged = function () {
            debugger;
            var fileChange = document.getElementById('file-6');
            $scope.FileName = fileChange.files.item(0).name;
            $scope.$apply(function () {
                if ($scope.FileName != null) {
                    $scope.ChoosenFileName = $scope.FileName;
                }
            });
        }

        $scope.CustomerContract = [];
        $scope.GetCustomerContract = function () {
            $http.get($rootScope.APiUrl + "/api/GetTicketCustomerDetails?CustomerId=" + $routeParams.CustomerId)
                .success(function (data) {
                    $scope.CustomerContract = data.Result2;
                });
        }

        $scope.Assigntogroup = [];
        $scope.GetAssigntogroup = function () {
            $http.get($rootScope.APiUrl + "/api/AssigntogroupList")
                .success(function (data) {
                    $scope.Assigntogroup = data.Data;
                });
        }
        $scope.GetAssigntogroup();

      
        $scope.AssignedTo = [];
        $scope.GetAssignedTo = function () {
            $http.get($rootScope.APiUrl + "/api/assigntoList")
                .success(function (data) {
                    $scope.AssignedTo = data.Data;
                });
        }
        $scope.GetAssignedTo();

           

        $("#crtbtn").click(function () {
            debugger;
            if (($("#textArea").val()).length && ($("#txtSubject").val()).length != 0) {
                debugger;
                //$('#crtbtn').attr('disabled', 'disabled');
                var data = new FormData();
               
                var fileInput = document.getElementById('file-6');
                for (i = 0; i < fileInput.files.length; i++) {
                    data.append(fileInput.files[i].name, fileInput.files[i]);
                }
               
               

                var Title = $("#txtSubject").val();
                var Description = $("#textArea").val();
                var AssignedTo = $scope.ddlAssignedTo;
              
                var ContractNumber = $scope.TaskContract;
                var CustomerId = $rootScope.SerachCustomerID;
                var TargetDate = $scope.TargetDate;
               
               
                data.append("Title", Title);
                data.append("Description", Description);
                data.append("AssignedTo", AssignedTo);
                data.append("TargetDate", TargetDate);
                data.append("ContractNumber", ContractNumber);
                data.append("CustomerId", CustomerId);


                $.ajax({

                    type: "POST",
                    url: $rootScope.APiUrl + '/Tasks/GetCreatePop',
                    traditional: true,
                    data: data,
                    contentType: false,
                    processData: false,
                    success: successFunc,
                    error: errorFunc
                });
            }
            else {
              
                if (($("#textArea").val()).length == 0) {
                    $("#textArea").next().show();
                }
                if (($("#txtSubject").val()).length == 0) {
                    $("#txtSubject").next().show();
                }

            }
            function successFunc(data, status) {
                swal(data);
                $("#createpopup").addClass("hide");
                GetCustomerTaskList();
            }
            function errorFunc() {
                swal('error');
                $("#createpopup").addClass("hide");
                GetCustomerTaskList();
            }
        });


        $scope.saveupdate = function (EnqId) {
            debugger;
            var f = new FormData();
            var data = new FormData();
            EnquiryId = parseInt(EnqId);
            comments = $scope.comments;
            status = $scope.StatusSelect;
            var checkIds = "";
            //var checkIds = $(".replyTy").filter(".active").data("stat");
            var ReplyType = "2";

            if (checkIds == "Internal") {
                var ReplyType = "1";
            }
            else {
                var ReplyType = "2";
            }

            var fileInput = document.getElementById('fileUploadQ');
            for (i = 0; i < fileInput.files.length; i++) {
                data.append(fileInput.files[i].name, fileInput.files[i]);
            }
            $.ajax({

                type: "POST",
                url: $rootScope.APiUrl + "/api/EnqList1?EnquiryId=" + JSON.stringify(EnquiryId) + "&status=" + JSON.stringify(status) + "&comments=" + JSON.stringify(comments) + "&ReplyType=" + ReplyType + "&UserId=" + $rootScope.UserName,
                traditional: true,

                //enctype: 'multipart/form-data',
                data: data,
                contentType: false,
                processData: false,
                success: successFunc,
                error: errorFunc
            });
        }
        function successFunc(data, status) {
            alert(data);
            $("#Ticketupdate").hide();
            $scope.comments = "";
            $scope.StatusSelecta = { "selected": null };
            $("#fileUploadQ").val("");


        }
        function errorFunc() {
            alert('error');
        }

        $scope.changestatus = function (status) {
            debugger;
            GetCustomerTaskList();
        }

        //************* Pagination *****************

        $scope.paginationPageSizes = [5, 10, 15, 25, 50],
        $scope.Ticketpaginate = function (value) {
            var begin, end, index;
            begin = ($scope.TicketcurrentPage - 1) * parseInt($scope.pagination.TicketnumPerPage);
            end = begin + parseInt($scope.pagination.TicketnumPerPage);
            index = $scope.CustomerTaskList.indexOf(value);
            return (begin <= index && index < end);
        };
        $scope.selectTicketpagination = function (value) {
            debugger;
            $scope.TicketcurrentPage = value;
        };
        $scope.changeTicketpagesize = function (value) {
            debugger;
            $scope.pagination.TicketnumPerPage = value;
            $scope.TicketcurrentPage = 1;
            $scope.Tickettotallist = Math.ceil($scope.CustomerTaskList.length / parseInt($scope.pagination.TicketnumPerPage));

        };
        //************** Pagination ***********
    }]);