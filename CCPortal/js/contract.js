﻿app.controller('ContractCtrl', ['$scope', '$rootScope', '$http', '$routeParams',  
    function ($scope, $rootScope, $http, $routeParams) {

       

        $rootScope.SerachCustomerID = $routeParams.CustomerId;
        $scope.ContractNumber = $routeParams.ContractNumber;

        if ($scope.ContractNumber) {
            GetContractDetailsByContractNumber();
        }
        $scope.pagination = {
            ContractListnumPerPage: "10",
            BContractListnumPerPage: "10"
        }
        $scope.filter = {
            ContractStatus: null,
        }
        $scope.StagesDrop = [];

        $scope.StagesDrop = [
            { id: "0", NameEn: "ShowAll", NameAr: "" },
            { id: "1", NameEn: "Active", NameAr: "" },
            { id: "2", NameEn: "InActive", NameAr: "" },

        ];

        GetCustomerContractList();
        GetCCustomerDetailsByCustomerId();
        $scope.GetCustomerContractList = function () {
            GetCustomerContractList();
        };
                                                                                                       
        function GetCCustomerDetailsByCustomerId() {
            var CustomerId = $rootScope.SerachCustomerID;
            if (CustomerId.length > 0) {
                $http.get($rootScope.APiUrl + "/api/GetDetailsByIndiviualCustomerPortal?CustomerID=" + CustomerId + "&PortalType=1")
                    .success(function (emp) {
                        debugger;
                        $scope.customer = emp;
                    });

            }
        }

        function GetCustomerContractList() {
            debugger;

            $http.get($rootScope.APiUrl + "/api/GetCustomerContractLists?CustomerId=" + $rootScope.SerachCustomerID)
                .success(function (data) {
                    debugger;
                    data.forEach(function addDates(row, index) {
                        debugger;
                        var currentTime = new Date();
                        currentTime.setDate(currentTime.getDate() + row.TotalRemainingDays);
                        row.ExpectedEndDate = currentTime;
                    });

                    $scope.CustomerContractList = data;
                    $scope.ContractLists = data;           
                    $scope.filter.ContractStatus = "0";
                    $scope.getFilterbyStatusInd();

                });
        }

        $scope.getFilterbyStatusInd = function () {
            debugger;
            var ContractStatus = $scope.filter.ContractStatus;
            if (ContractStatus < 1) {
                $scope.CustomerContractList = $scope.ContractLists;
            }

            else if (ContractStatus == "1") {
                $scope.CustomerContractList = [];
                var contract = $scope.ContractLists.filter(function (d) {
                    if (d.ContractStatus == "50" || d.ContractStatus == "80" || d.ContractStatus == "90" || d.ContractStatus == "30"
                        || d.ContractStatus == "11" || d.ContractStatus == "13") {
                        return (d);
                    }
                });
                $scope.CustomerContractList = contract;
            }
            else if (ContractStatus == "2") {
                $scope.CustomerContractList = [];
                var contract = $scope.ContractLists.filter(function (d) {
                    if (d.ContractStatus == "60" || d.ContractStatus == "20" || d.ContractStatus == "100" || d.ContractStatus == "120") {
                        return (d);
                    }
                });
                $scope.CustomerContractList = contract;
            }
            else {
                if ($routeParams.StageId == "30" || $routeParams.StageId == "90" || $routeParams.StageId == "80") {
                    $scope.filter.ContractStatus = "1";
                }
                $scope.CustomerContractList = [];
                var contract = $scope.ContractLists.filter(function (d) {
                    if (d.ContractStatus == ContractStatus) {
                        return (d);
                    }
                });
                $scope.CustomerContractList = contract;
            }
            $scope.ContractListcurrentPage = 1;
            $scope.pagination.ContractListnumPerPage = "10";
            $scope.ContractListtotallist = Math.ceil($scope.CustomerContractList.length / parseInt($scope.pagination.ContractListnumPerPage));

        };

        $scope.getContractDetailList = function (item) {
            $scope.ContractNumber = item.ContractNumber;

            GetContractDetailsByContractNumber();

            $('html, body').animate({
                scrollTop: $("#selected_contract").offset().top
            }, 1000);

        };

        $scope.showcontractlist = false;
        $scope.clickbody = function ($event) {
            if (!$($event.target).is('#txtContractNumber')) {
                $scope.showcontractlist = false;
            }
        };

        $scope.getcontract = function (e) {
            debugger;
            var contractnumber = $scope.ContractNumber;
            if (e.keyCode == 13) {
                GetContractDetailsByContractNumber();
            }
            else {
                if (contractnumber.length > 0) {
                    $http.post($rootScope.APiUrl + "/IndividualSector/GetContractAcutoComplete?term=" + contractnumber)
                        .success(function (datam) {
                            debugger;
                            $scope.contractlist = datam;
                            $scope.showcontractlist = true;

                        });
                }
                else {
                    $scope.showcontractlist = false;
                }
            }
        };
        $scope.selectcontract = function (list) {
            $scope.ContractNumber = list.ContractNumber;
            $scope.showcontractlist = false;
            GetContractDetailsByContractNumber();
        };
        $scope.getContractdetails = function () {
            GetContractDetailsByContractNumber();
        };
        function GetContractDetailsByContractNumber() {
            var contractnumber = $scope.ContractNumber;
            if (contractnumber.length > 0) {
                $http.post($rootScope.APiUrl + "/api/GetContractDetailsByContractNumber?Id=" + contractnumber)
                    .success(function (datam) {
                        debugger;
                        if (datam.ContractStatus == 30 || datam.ContractStatus == 40 || datam.ContractStatus == 50 || datam.ContractStatus == 70 || datam.ContractStatus == 80 || datam.ContractStatus == 90) {
                            var currentTime = new Date();
                            currentTime.setDate(currentTime.getDate() + datam.RemainingDays);
                            datam.ExpectedEndDate = currentTime;
                        }

                        $scope.ContractDetails = datam;
                        $scope.getContractCustomerRequest(); 

                        //getContractPaymentList();
                        //getContractAdvancePaymentList();
                        //getContractPaymentList();
                        //GetCustomerInvoiceByCustomerNumber();
                        //GetContractCalcuation();
                       
                    });
            }
        }

        $scope.getContractCustomerRequest = function (){
            $http.post($rootScope.APiUrl +"/api/GetContractRequestDetailsList?ContractNumber=" + $scope.ContractDetails.ContractNumber)
                .success(function (datam) {
                    debugger;
                    $scope.RequestDetailsList = datam;
                });
        };


        function GetContractCalcuation() {

            $http.post($rootScope.APiUrl + "/api/GetContractCalcuationDetails?ContractNumber=" + $scope.ContractDetails.ContractNumber)
                .success(function (datam) {
                    debugger;
                    $scope.ContractActionDetails = datam.CalcuationDetails;
                    $scope.CancelFee = datam.CancelFee;
                    $scope.TerminationFee = datam.TerminationFee;
                    $scope.ExtendPenalityFee = datam.ExtendPenalityFee;
                    $scope.EndPenaltyFee = datam.EndPenaltyFee;
                    $scope.ExchangeFee = datam.ExchangeFee;
                    $scope.PackageChangeFee = datam.PackageChangeFee;

                    var UnInvoicedDays = $scope.ContractActionDetails.UsedDays - $scope.ContractActionDetails.InvoicedDays;
                    var costperday = ($scope.ContractActionDetails.PackageAmount / $scope.ContractActionDetails.PackageDays).toFixed(2);
                    $scope.ContractCostperday = costperday;
                    var UnInvoicedAmount = UnInvoicedDays * costperday;
                    $scope.UnInvoicedAmounts = (UnInvoicedDays * costperday).toFixed(2);

                    $scope.OtherPayment = ($scope.ContractActionDetails.PaymentAmount - $scope.ContractActionDetails.AdvanceAmount).toFixed(2);
                    $scope.TotalBalances = ($scope.ContractActionDetails.PaymentAmount - ($scope.ContractActionDetails.InvoiceAmount + UnInvoicedAmount)).toFixed(2);
                });
        }

        $scope.getContractPaymentList = function () {
            getContractPaymentList();
        };

        function getContractPaymentList() {
            debugger;
            $http.get($rootScope.APiUrl + "/api/GetCustomerPaymentListForContract?ContractNumber=" + $scope.ContractDetails.ContractNumber)
                .success(function (datam) {
                    debugger;
                    $scope.ContractPaymentList = datam;
                   
                });
        }

        $scope.GetSalaryProcess = function (list) {
            debugger;
            var SalaryRecId = list.PaymentNumber;
            $scope.showSalaryrecid = list.PaymentNumber;
            $(".closesalary").hide();
            $(".showsalary").show();
            $("#showsalary_" + list.PaymentNumber).hide();
            $("#closesalary_" + list.PaymentNumber).show();
            if ($scope.tempSalaryRecId != SalaryRecId) {
                $http.get($rootScope.APiUrl + "/api/GetSettledPaymentList?PaymentNumber=" + SalaryRecId)
                    .success(function (datam) {
                        debugger;
                        $scope.SettledPaymentList = datam;
                        $scope.tempSalaryRecId = SalaryRecId;
                        $scope.showsalaryTransaction = true;


                    });
            }
            else {
                $scope.showsalaryTransaction = true;
            }
        };

        $scope.CloseGetSalaryProcess = function (list) {

            $(".closesalary").hide();
            $(".showsalary").show();
            $scope.showsalaryTransaction = false;
        };

        $scope.getContractAdvancePaymentList = function () {
            getContractAdvancePaymentList();
        };
        function getContractAdvancePaymentList() {

            debugger;
            $http.get($rootScope.APiUrl +"/api/getContractAdvancePaymentList?ContractNumber=" + $scope.ContractDetails.ContractNumber)
                .success(function (datam) {
                    debugger;
                    $scope.ContractAdvancePaymentList = datam;
                });
        }


        $scope.GetAdvanceReturnProcess = function (list) {
            debugger;
            var AdvanceRecId = list.AdvanceNumber;
            $scope.showAdvanceReturnrecid = list.AdvanceNumber;
            $(".closeAdvance").hide();
            $(".showAdvance").show();
            $("#showAdvance_" + list.AdvanceNumber).hide();
            $("#closeAdvance_" + list.AdvanceNumber).show();
            if ($scope.tempAdvanceRecId != AdvanceRecId) {
                $http.get($rootScope.APiUrl +"/api/GetAdvanceSettlement?AdvanceNumber=" + AdvanceRecId)  
                    .success(function (datam) {
                        debugger;

                        $scope.SettledPaymentList1 = datam;
                        $scope.tempAdvanceRecId = AdvanceRecId;
                        $scope.showAdvanceReturn = true;

                    });

                $http.get($rootScope.APiUrl +"/api/GetAdvanceInvoiceSettlement?AdvanceNumber=" + AdvanceRecId)
                    .success(function (datam) {
                        debugger;

                        $scope.AdvanceInvoiceSettleList = datam;
                        $scope.tempAdvanceRecId = AdvanceRecId;
                        $scope.showAdvanceReturn = true;

                    });
            }
            else {
                $scope.showAdvanceReturn = true;
            }
        };

        $scope.CloseGetAdvanceReturnProcess = function (list) {

            $(".closeAdvance").hide();
            $(".showAdvance").show();
            $scope.showAdvanceReturn = false;
        };

        $scope.GetCustomerInvoiceByCustomerNumber = function () {
            GetCustomerInvoiceByCustomerNumber();
        }
        function GetCustomerInvoiceByCustomerNumber() {

            $http.get($rootScope.APiUrl + "/api/GetCustomerInvoiceByCustomerNumber?CustomerId=" + $scope.ContractDetails.CustomerID + '&ContractNumber=' + $scope.ContractDetails.ContractNumber)
                .success(function (data) {
                    debugger;
                    $scope.CustInvoice = data;

                    //$scope.CheckSadadAccountStatus();
                  
                });
        }


        $scope.CheckSadadAccountStatus = function () {

            $http.get($rootScope.APiUrl + "/api/checkContractSadadBillActivation?ContractNumber=" + $scope.ContractDetails.ContractNumber)
                .success(function (datam) {
                    debugger;
                    // alert(datam);
                    if (datam.status) {
                        $scope.SadadCurrentAccountStatus = datam.data;
                    }
                    else {
                        $scope.SadadCurrentAccountStatus = 4;
                    }
                });
        };

        $scope.GetContractTransactionByContractNumber = function () {
            GetContractTransactionByContractNumber();
        };

        function GetContractTransactionByContractNumber() {

            $http.get($rootScope.APiUrl + "/api/GetContractTransactionByContractNumber?ContractNumber=" + $scope.ContractDetails.ContractNumber)
                .success(function (data) {
                    debugger;
                    $scope.ContractTransaction = data;

                });
        }

        $scope.GetDiscountByCustomerId = function () {
            GetDiscountByCustomerId();
        };
        function GetDiscountByCustomerId() {

            $http.get($rootScope.APiUrl + "/api/GetDiscountByCustomerId?CustomerID=" + $scope.ContractDetails.CustomerID)
                .success(function (data) {
                    debugger;
                    $scope.Discount = data;
                });
        }

        GetBCustomerContractList();
        $scope.GetBCustomerContractList = function () {
            GetBCustomerContractList();
        };
        function GetBCustomerContractList() {
            debugger;

            $http.get($rootScope.APiUrl + "/api/Business/GetContractListbyCustomer?CustomerId=" + $rootScope.SerachCustomerID)
                .success(function (data) {
                    debugger;
                    $scope.BCustomerContractList = data;
                    $scope.BContractLists = data;
                    $scope.filter.ContractStatus = "0";
                    $scope.getFilterbyStatusBus();

                 
                });
        }


        $scope.getFilterbyStatusBus = function () {
            debugger;
            var ContractStatus = $scope.filter.ContractStatus;
            if (ContractStatus < 1) {
                $scope.BCustomerContractList = $scope.BContractLists;
            }

            else if (ContractStatus == "1") {
                $scope.BCustomerContractList = [];
                var contract = $scope.BContractLists.filter(function (d) {
                    if (d.ContractStatus == "50" || d.ContractStatus == "80" || d.ContractStatus == "90" || d.ContractStatus == "30"
                        || d.ContractStatus == "11" || d.ContractStatus == "13") {
                        return (d);
                    }
                });
                $scope.BCustomerContractList = contract;
            }
            else if (ContractStatus == "2") {
                $scope.BCustomerContractList = [];
                var contract = $scope.BContractLists.filter(function (d) {
                    if (d.ContractStatus == "60" || d.ContractStatus == "20" || d.ContractStatus == "100" || d.ContractStatus == "120") {
                        return (d);
                    }
                });
                $scope.BCustomerContractList = contract;
            }
            else {
                if ($routeParams.StageId == "30" || $routeParams.StageId == "90" || $routeParams.StageId == "80") {
                    $scope.filter.ContractStatus = "1";
                }
                $scope.BCustomerContractList = [];
                var contract = $scope.BContractLists.filter(function (d) {
                    if (d.ContractStatus == ContractStatus) {
                        return (d);
                    }
                });
                $scope.BCustomerContractList = contract;
            }
            $scope.BContractListcurrentPage = 1;
            $scope.pagination.BContractListnumPerPage = "10";
            $scope.BContractListtotallist = Math.ceil($scope.BCustomerContractList.length / parseInt($scope.pagination.BContractListnumPerPage));

        };


        //************* Pagination ContractList *****************

        $scope.paginationPageSizes = [5, 10, 15, 25, 50],
            $scope.ContractListpaginate = function (value) {
                var begin, end, index;
                begin = ($scope.ContractListcurrentPage - 1) * parseInt($scope.pagination.ContractListnumPerPage);
                end = begin + parseInt($scope.pagination.ContractListnumPerPage);
                index = $scope.CustomerContractList.indexOf(value);
                return (begin <= index && index < end);
            };
        $scope.selectContractListpagination = function (value) {
            debugger;
            $scope.ContractListcurrentPage = value;
        };
        $scope.changeContractListpagesize = function (value) {
            debugger;
            $scope.pagination.ContractListnumPerPage = value;
            $scope.ContractListcurrentPage = 1;
            $scope.ContractListtotallist = Math.ceil($scope.CustomerContractList.length / parseInt($scope.pagination.ContractListnumPerPage));

        };
        //************** Pagination ***********


        //************* Pagination BContractList *****************

        $scope.paginationPageSizes = [5, 10, 15, 25, 50],
            $scope.BContractListpaginate = function (value) {
                var begin, end, index;
                begin = ($scope.BContractListcurrentPage - 1) * parseInt($scope.pagination.BContractListnumPerPage);
                end = begin + parseInt($scope.pagination.BContractListnumPerPage);
                index = $scope.BCustomerContractList.indexOf(value);
                return (begin <= index && index < end);
            };
        $scope.selectBContractListpagination = function (value) {
            debugger;
            $scope.ContractListcurrentPage = value;
        };
        $scope.changeBContractListpagesize = function (value) {
            debugger;
            $scope.pagination.BContractListnumPerPage = value;
            $scope.BContractListcurrentPage = 1;
            $scope.BContractListtotallist = Math.ceil($scope.BCustomerContractList.length / parseInt($scope.pagination.BContractListnumPerPage));

        };
        //************** Pagination ***********

    }]);