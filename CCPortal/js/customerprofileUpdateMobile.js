﻿


app.directive('customermobileupdate', function () {
    var index = -1;

    return {
        restrict: 'E',
        scope: {
            customerid: '=customerid',
            externalfn: '=externalfn',
            title: '=title',
         
          
           
        },
        controller: ['$scope', '$rootScope', '$http', '$routeParams', '$filter',
 function ($scope, $rootScope, $http, $routeParams, $filter) {
         

     if (!$scope.title)
     {
         $scope.title="Contact Info"
     }

   
     if (!$scope.mode || $scope.mode=="Edit") {
         $scope.$watch("customerid", function (val) {
             if (val) {
                 $scope.loadMobileHistory();
             }
         });
     }
     else {

    
     }


  

   



     $scope.CurrentSelectedHistory = null;
     $scope.loadMobileHistory = function () {
         debugger;
         $http.get($rootScope.APiUrl + "/api/GetCustomerMobileHistory?CustomerId=" + $scope.customerid)
    .success(function (emp) {
        debugger;
        $scope.CustomerContactHistory = emp.CustomerContactHistory;
        $scope.CustomerContactDetails = emp.CustomerContactDetails;
      
    });
     };

   

     $scope.showhistorypopup = function (id) {
         $("#temphistorypopup").show();
         $scope.CreateMob = {};

     };
     $scope.closehistorypopup = function (id) {
        $("#temphistorypopup").hide();

    };

     $scope.externalfn.mobileUpdatefn = function (id) {
         if (!$scope.customerid) {
             if (!id) {
                 swal("Customer Not Found");
                 return;
             }
             $scope.customerid = id;
         }

         $("#temphistorypopup").show();
         $scope.CreateMob = {};

     };

     $scope.AddOrUpdateMobileNumber = function (id) {


         var MobileNumber = $scope.CreateMob.MobileNumber;

         if (!MobileNumber)
         {
             swal({
                 title: ' alert!',
                 type: "error",
                 text: 'Type Currect Mobile Number',
                 timer: 3000
             });
             return;
         }
      
         var variable = MobileNumber.substring(0, 2);
         if (variable != '05' || MobileNumber.length != 10) {
             $scope.CreateMob = {};
             swal({
                 title: ' alert!',
                 type: "error",
                 text: 'Type Currect Mobile Number',
                 timer: 3000
             });
             return;
         }
        

         $http.get($rootScope.APiUrl + "/api/AddCustomerMobileNumberHistory?CustomerId=" + $scope.customerid + "&MobileNumber=" + MobileNumber)
 .success(function (emp) {
     debugger;
     if (emp.status) {
         $scope.loadMobileHistory();
         $scope.CurrentSelectedHistory = emp.record;
         $scope.sendverifyCode();
       
     }
     else {
         swal(emp.msg)
     }


 });
     };


     $scope.sendverifyCode = function (id) {
         $scope.VerifyMob = {};
         if (!$scope.CurrentSelectedHistory)
         {
             swal("Contact Details Not Found");
             return;
         }

         $http.get($rootScope.APiUrl + "/api/SendMobileVerifyCodeByHistory?Id=" + $scope.CurrentSelectedHistory.RecId)
 .success(function (emp) {
     debugger;
     if (emp.status)
     {
        
         $("#tempverifypopup").show();
     }
     else {
         swal(emp.msg)
     }
    

 });
     };


     $scope.showverifypopup = function (obj) {
         $scope.VerifyMob = {};
         $scope.CurrentSelectedHistory = obj;
         if (!$scope.CurrentSelectedHistory) {
             swal("Contact Details Not Found");
             return;
         }
         $("#tempverifypopup").show();
     };

     $scope.closeverifypopup = function (obj) {
        $("#tempverifypopup").hide();
    };


     $scope.CheckVerifyCode = function (id) {

         if (!$scope.VerifyMob) {
             swal("Verify code Not Found");
             return;
         }
         if (!$scope.CurrentSelectedHistory) {
             swal("Contact Details Not Found");
             return;
         }

         $http.get($rootScope.APiUrl + "/api/CheckVerifyCodeByContactHistory?Id=" + $scope.CurrentSelectedHistory.RecId + "&AccessCode=" + $scope.VerifyMob.VerifyCode)
 .success(function (emp) {
     debugger;
     if (emp.status) {
         swal("Mobile Number Verified SucessFully");
         $("#tempverifypopup").hide();
         $("#temphistorypopup").hide();

         $scope.externalfn.refreshfn();

         $scope.loadMobileHistory();
     }
     else {
         swal(emp.msg)
     }


 });
     };

    
   
     $scope.resendverifycode = function (obj) {
         $scope.CurrentSelectedHistory = obj;
         if (!$scope.CurrentSelectedHistory) {
             swal("Contact Details Not Found");
             return;
         }
         $scope.sendverifyCode();
     };

   


        }],
        link: function (scope, element, attrs) {
            scope.currentclass = attrs.class;
           


          
        },
     
        templateUrl: '/templates/CustomerProfileMobileTemplate.html'
    };
});






