﻿app.controller('TicketDetailsCtrl', ['$scope', '$rootScope', '$http', '$routeParams', '$filter',
    function ($scope, $rootScope, $http, $routeParams, $filter) {

        debugger;

        var UserName = window.localStorage.getItem("UserName");
        if (UserName) {
            $rootScope.UserName = UserName;
        }
        else {
            window.localStorage.clear();
            $rootScope.UserName = null;
            $location.path("/signin");
        }

        $rootScope.SerachCustomerID = $routeParams.CustomerId;
        function GetCCustomerDetailsByCustomerId() {
            var CustomerId = $rootScope.SerachCustomerID;
            if (CustomerId.length > 0) {

                $http.get($rootScope.APiUrl + "/api/GetDetailsByIndiviualCustomerPortal?CustomerID=" + CustomerId + "&PortalType=1")
                    .success(function (emp) {
                        debugger;
                        $scope.user = emp;
                        date = new Date($scope.user.DOBGeorgian);
                        $scope.userAge = _calculateAge(date);

                    });

            }
        }
        function _calculateAge(birthday) { // birthday is a date
            var ageDifMs = Date.now() - birthday.getTime();
            var ageDate = new Date(ageDifMs); // miliseconds from epoch
            return Math.abs(ageDate.getUTCFullYear() - 1970);
        }

        GetCCustomerDetailsByCustomerId();


        var TicketId = $routeParams.TicketId;
        loadfunction();
        function loadfunction() {
            $http.get($rootScope.APiUrl + "/api/GetTicketDetails?enquiryid=" + TicketId)
                .success(function (data) {
                    debugger;
                    var emp = data.Data;
                    $scope.Enqlists = emp;
                    getcomments();
                    if (emp.StatusId.length != 0) {
                        $scope.StatusSelecta = emp.StatusId;
                    }
                    else {
                        $scope.StatusSelecta = 1;
                    }

                })
                .error(function (err) {
                    alert(JSON.stringify(err));
                });
        }



        $('.panel .nav-tabs').on('click', 'a', function (e) {
            var tab = $(this).parent(),
                tabIndex = tab.index(),
                tabPanel = $(this).closest('.panel'),
                tabPane = tabPanel.find('.tab-pane').eq(tabIndex);
            tabPanel.find('.active').removeClass('active');
            tab.addClass('active');
            tabPane.addClass('active');
        });



        function getcomments() {

            $http.get($rootScope.APiUrl + "/api/GetCommentsDetailsForCustomer?enquiryid=" + TicketId)
                .success(function (emp) {
                    debugger;
                    $scope.commentsList = emp.Data;
                })
                .error(function (err) {
                    alert(JSON.stringify(err));
                });

        }

        $scope.getcomments = function () {
            getcomments();
        };

        $scope.showCustomerSatisfied = false;
        $scope.changeStatusSelected = function () {
            debugger;
            var status = $scope.StatusSelecta;
            if (status ==2)
                $scope.showCustomerSatisfied = true;
            else
                $scope.showCustomerSatisfied = false;
        };



        $scope.statuslist = [];
        $scope.GetStatus = function () {
            $http.get($rootScope.APiUrl + "/api/GetStatusList")
                .success(function (emp) {
                    $scope.statuslist = emp.Data;
                });
        }
        $scope.GetStatus();

        $scope.saveupdate = function (EnqId) {
            debugger;
            var f = new FormData();
            var data = new FormData();
            EnquiryId = parseInt(EnqId);
            comments = $scope.comments;
            status = $scope.StatusSelecta;
            CustomerSatisfied = $("#ddlCustomerSatisfied").val()

            var checkIds = "";
            //var checkIds = $(".replyTy").filter(".active").data("stat");
            var ReplyType = "2";

            if (checkIds == "Internal") {
                var ReplyType = "1";
            }
            else {
                var ReplyType = "2";
            }

            var fileInput = document.getElementById('fileUploadQD');
            for (i = 0; i < fileInput.files.length; i++) {
                data.append(fileInput.files[i].name, fileInput.files[i]);
            }
            $.ajax({

                type: "POST",
                url: $rootScope.APiUrl + "/api/EnqList1?EnquiryId=" + JSON.stringify(EnquiryId) + "&status=" + JSON.stringify(status) + "&comments=" + JSON.stringify(comments) + "&ReplyType=" + ReplyType + "&UserId=" + $rootScope.UserName + "&CustomerSatisfied=" + CustomerSatisfied,
                traditional: true,

                //enctype: 'multipart/form-data',
                data: data,
                contentType: false,
                processData: false,
                success: successFunc,
                error: errorFunc
            });
        }
        function successFunc(data, status) {
            alert(data);
            $("#Ticketupdate").hide();
            $scope.comments = "";
            loadfunction();
            $scope.StatusSelecta = { "selected": null };
            $("#fileUploadQD").val("");


        }
        function errorFunc() {
            alert('error');
        }


    }]);