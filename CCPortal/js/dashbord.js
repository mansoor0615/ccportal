﻿app.controller('DashbordCtrl', ['$scope', '$rootScope', '$http', '$location', '$routeParams',
    function ($scope, $rootScope, $http, $location, $routeParams) {

        debugger;

        var UserName = window.localStorage.getItem("UserName");
        if (UserName) {
            $rootScope.UserName = UserName;
        }
        else {
            window.localStorage.clear();
            $rootScope.UserName = null;
            $location.path("/signin");
        }

        $rootScope.SerachCustomerID = $routeParams.CustomerId;

        GetCCustomerDetailsByCustomerId();
        GetCustomerTicketDetails();
        GetCustomerBalance();
        function GetCCustomerDetailsByCustomerId() {
            var CustomerId = $rootScope.SerachCustomerID;
            if (CustomerId.length > 0) {

                $http.get($rootScope.APiUrl + "/api/GetDetailsByIndiviualCustomerPortal?CustomerID=" + CustomerId + "&PortalType=1")
                    .success(function (emp) {
                        debugger;
                        $scope.user = emp;
                        date = new Date($scope.user.DOBGeorgian);
                        $scope.userAge = _calculateAge(date);

                    });

            }
        }
        function _calculateAge(birthday) { // birthday is a date
            var ageDifMs = Date.now() - birthday.getTime();
            var ageDate = new Date(ageDifMs); // miliseconds from epoch
            return Math.abs(ageDate.getUTCFullYear() - 1970);
        }


        function GetCustomerTicketDetails() {

            var CustomerId = $rootScope.SerachCustomerID;
            if (CustomerId.length > 0) {

                $http.get($rootScope.APiUrl + "/api/GetEnquiryStatusCount?CustomerId=" + CustomerId)
                    .success(function (emp) {
                        debugger;
                        $scope.TicketDatails = emp;

                        $scope.AllTicket = $scope.TicketDatails.filter(x => x.StatusName == "All")[0];
                        $scope.OpenTicket = $scope.TicketDatails.filter(x => x.StatusName == "Open")[0];
                        $scope.CloseTicket = $scope.TicketDatails.filter(x => x.StatusName == "Closed")[0];
                        $scope.OnHoldTicket = $scope.TicketDatails.filter(x => x.StatusName == "On Hold")[0];
                        $scope.ResolvedTicket = $scope.TicketDatails.filter(x => x.StatusName == "Resolved")[0];
                        $scope.ReopenTicket = $scope.TicketDatails.filter(x => x.StatusName == "Reopen")[0];
                        $scope.CanceledTicket = $scope.TicketDatails.filter(x => x.StatusName == "Canceled")[0];

                        var total = $scope.AllTicket.Count;

                        if ($scope.OpenTicket.Count > 0)
                            $scope.OpenTicketPer = (($scope.OpenTicket.Count / total) * 100).toFixed(2);

                        if ($scope.CloseTicket.Count > 0)
                            $scope.CloseTicketPer = (($scope.CloseTicket.Count / total) * 100).toFixed(2);

                        if ($scope.OnHoldTicket.Count > 0)
                            $scope.OnHoldTicketPer = (($scope.OnHoldTicket.Count / total) * 100).toFixed(2);

                        if ($scope.ResolvedTicket.Count > 0)
                            $scope.ResolvedTicketPer = (($scope.ResolvedTicket.Count / total) * 100).toFixed(2);

                        if ($scope.ReopenTicket.Count > 0)
                            $scope.ReopenTicketPer = (($scope.ReopenTicket.Count / total) * 100).toFixed(2);

                        if ($scope.CanceledTicket.Count > 0)
                            $scope.CanceledTicketPer = (($scope.CanceledTicket.Count / total) * 100).toFixed(2);

                    });
            }

        }

        function GetCustomerBalance() {
            var CustomerId = $rootScope.SerachCustomerID;
            if (CustomerId.length > 0) {

                $http.get($rootScope.APiUrl + "/api/GetCustomerBalance?CustomerId=" + CustomerId)
                    .success(function (emp) {
                        debugger;
                        $scope.PaidAmount = emp.Payment > 0 ? emp.Payment.toFixed(2) : emp.Payment;
                        $scope.TotalAmount = emp.Invoice > 0 ? emp.Invoice.toFixed(2) : emp.Invoice;
                        $scope.PendingAmount = (emp.Payment - emp.Invoice).toFixed(2);
                    });
            }
        }

        function hijidate1(date) {
            var data = new FormData();
            data.append("date", date);
            $.ajax({
                type: "POST",
                url: "/Account/ConvertDate",
                traditional: true,
                data: data,
                contentType: false,
                processData: false,
                success: function (data) {

                    $scope.uesrHijriDOB = data;

                }
            });
        }


        GetCustomerContractList();
        $scope.GetCustomerContractList = function () {
            GetCustomerContractList();
        }
        function GetCustomerContractList() {
            debugger;

            $http.get($rootScope.APiUrl + "/api/GetCustomerContractLists?CustomerId=" + $rootScope.SerachCustomerID)
                .success(function (data) {
                    debugger;
                    data.forEach(function addDates(row, index) {
                        debugger;
                        var currentTime = new Date();
                        currentTime.setDate(currentTime.getDate() + row.TotalRemainingDays);
                        row.ExpectedEndDate = currentTime;
                    });

                    $scope.CustomerContractList = data;

                });
        }

        getCustomerPaymentList();
        $scope.getCustomerPaymentList = function () {
            getCustomerPaymentList();
        };

        function getCustomerPaymentList() {
            debugger;
            $http.get($rootScope.APiUrl + "/api/GetCustomerPaymentList?CustomerId=" + $rootScope.SerachCustomerID)
                .success(function (datam) {
                    debugger;
                    $scope.CustomerPaymentList = datam;

                });
        }
        GetCustomerTicketList();
        $scope.GetCustomerTicketList = function () {
            GetCustomerTicketList();
        };

        function GetCustomerTicketList() {
            debugger;
            $http.get($rootScope.APiUrl + "/api/GetMyTicket?CustomerId=" + $rootScope.SerachCustomerID)
                .success(function (data) {
                    debugger;
                    $scope.CustomerTicketList = data.Data;
                });

        }


        GetCustomerServiceEnquiry();
        $scope.RefreshGetCustomerServiceEnquiry = GetCustomerServiceEnquiry();

        function GetCustomerServiceEnquiry() {
            debugger;
            $http.get($rootScope.APiUrl + "/api/GetCustomerServiceEnquiryByCustomerId?CustomerId=" + $rootScope.SerachCustomerID)
                .success(function (data) {
                    $scope.CustomerServiceInquiryList = data;
                });
        }

        GetBCustomerContractList();
        $scope.GetBCustomerContractList = function () {
            GetBCustomerContractList();
        };
        function GetBCustomerContractList() {
            debugger;

            $http.get($rootScope.APiUrl + "/api/Business/GetContractListbyCustomer?CustomerId=" + $rootScope.SerachCustomerID)
                .success(function (data) {
                    debugger;
                    $scope.BCustomerContractList = data;
                });
        }


        // ****************** Charts***********************
        colorsList = ["#F08080", "#20B2AA", "#222b6f", "#ff0000", "#e808cd", "#53a93f", "#8064a1", "#ff0734", "#db3b3b", "#0e33ea", "#c0504e", "#fb8334", ]

        $scope.GetAccountSatisfactionChartList = function () {
            GetAccountSatisfactionChartList();
        }
        function GetAccountSatisfactionChartList() {
            var dataPoint = [];
            for (i = 0; i < 5; i++) {
                var obj = { label: "Product " + [i], y: Math.floor(Math.random() * 100), legendText: "Product " + [i], color: this.colorsList[i] };
                dataPoint.push(obj);
            }
            AccountSatisfactionChart(dataPoint);
        };
        GetAccountSatisfactionChartList();

        function AccountSatisfactionChart(dataPoint) {
            debugger;

            this.OnboardChart = new CanvasJS.Chart("AccountSatisfactionChart", {
                animationEnabled: true,
                exportEnabled: true,
                colorSet: ["#fb8334", "#53a93f", "#8064a1", "#ff0734", "#db3b3b"],
                title: {
                    text: ""
                },
                startAngle: 90,
                data: [{
                    type: "column",
                    color: "LightSeaGreen",
                    indexLabelPlacement: "outside",
                    showInLegend: false,
                    legendText: "Ticket Status",
                    indexLabel: "{label} - {y}",
                    dataPoints: dataPoint
                }]
            });

            this.OnboardChart.render();

        }


        $scope.GetTransactionSatisfactionChartList = function () {
            GetTransactionSatisfactionChartList();
        }
        function GetTransactionSatisfactionChartList() {
            var dataPoint = [];
            for (i = 0; i < 5; i++) {
                var obj = { label: "Product " + [i], y: Math.floor(Math.random() * 100), legendText: "Product " + [i], color: this.colorsList[i+5] };
                dataPoint.push(obj);
            }
            TransactionSatisfactionChart(dataPoint);
        };
        GetTransactionSatisfactionChartList();

        function TransactionSatisfactionChart(dataPoint) {
            debugger;

            this.OnboardChart = new CanvasJS.Chart("TransactionSatisfactionChart", {
                animationEnabled: true,
                exportEnabled: true,
                colorSet: ["#fb8334", "#53a93f", "#8064a1", "#ff0734", "#db3b3b"],
                title: {
                    text: ""
                },
                startAngle: 90,
                data: [{
                    type: "column",
                   // color: "LightSeaGreen",
                    indexLabelPlacement: "outside",
                    showInLegend: false,
                    legendText: "Ticket Status",
                    indexLabel: "{label} - {y}",
                    dataPoints: dataPoint
                }]
            });

            this.OnboardChart.render();

        }



        // ****************** /Charts***********************


    }]);